//	Package plugin provides essential features for skeleton program plugins.
//	These features include connecting to the appropriate file descriptors to
//	communicate messages.
package plugin

import (
	"bufio"
	"fmt"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
	"os"
	"skeleton/API/skeleton"
	"strings"
)

//
type Plugin struct {
	//---------------------------Externals----------------------//
	Name       string
	Path       string
	RpcAddress string
	RpcPort    string
	QuitChan   chan int

	//---------------------------Internals----------------------//
	listener net.Listener
	skelet   *skeleton.Skeleton
}

//	Sets the internals for communication with caller process
//		skel is the skeleton object used to communicate with main skeleton program
//		parser is the function called when a new message is received on os.StdIn
//		obj are the objects to be RPC-exposed by the plugin
func (p *Plugin) Init(skel *skeleton.Skeleton,
	parser func(string) error,
	obj ...interface{}) {
	p.skelet = skel
	// Set path and name of plugin
	strs := strings.Split(os.Args[0], "/")
	p.Path = strings.Join(strs[0:len(strs)-1], string(os.PathSeparator))
	p.Name = strs[len(strs)-1]
	//	Set current plugin RPC server and register obj objects
	var err error
	p.RpcAddress, p.RpcPort, err = skel.GetRpcCoords(p.Name)
	if err != nil {
		fmt.Println(skel.LogPrefix(), "Error getting RPC coords:", err.Error())
	} else if obj[0] != nil {
		fmt.Println(skel.LogPrefix(), "Starting", p.Name, "RPC service on", p.RpcAddress+":"+p.RpcPort, obj)
		server := rpc.NewServer()
		server.HandleHTTP(rpc.DefaultRPCPath, rpc.DefaultDebugPath)
		for _, v := range obj {
			server.Register(v)
			// rpc.Register(v)
		}
		// rpc.HandleHTTP()
		listener, e := net.Listen("tcp", fmt.Sprintf(":%s", p.RpcPort))
		if e != nil {
			fmt.Println(skel.LogPrefix(), "listen error on", p.Name, ":", e)
		}
		go func(l net.Listener) {
			for {
				if conn, err := l.Accept(); err != nil {
					fmt.Println(skel.LogPrefix(), p.Name, ": Error serving:", err)
				} else {
					go server.ServeCodec(jsonrpc.NewServerCodec(conn))
				}
			}
		}(listener)
		// go http.Serve(listener, nil)
	}
	p.QuitChan = make(chan int)
	go p.listenOnStdIn(parser)
}

func (p *Plugin) listenOnStdIn(parse func(string) error) {
	fmt.Println(p.skelet.LogPrefix(), p.Name, "listenOnStdIn")

	r := bufio.NewReader(os.Stdin)
	for {
		data, _, err := r.ReadLine()
		if err != nil {
			fmt.Println("Error reading from plugin ", p.Name, "os.Stdin :", err)
			p.Stop()
			break
		} else {
			fmt.Println(p.skelet.LogPrefix(), p.Name, "listenOnStdIn message:", string(data))
			if e := parse(string(data)); e != nil {
				fmt.Println(e)
			}
		}
	}
}

// Close channels and file descriptors, usually before top program
func (p *Plugin) Stop() {
	fmt.Println(p.skelet.LogPrefix(), "Stopping", p.Name)
	p.listener.Close()
}
