import QtQuick 2.0
import QtQuick.Controls 1.3


MenuItem{
    text: qsTr("Palette")
    property var topwindow
    onTriggered: {
        var comp= Qt.createQmlObject("import GoExtensions 1.0; Person{id:person;}",
                                     topwindow,null)
        var goobj= Qt.createQmlObject("import GoExtensions 1.0; GoObj{id:goobj;}",
                                     topwindow,null)
        comp.write("Adding a palette Tab",19)
        goobj.write("About to call goobj.callRPC(\"palette.Cat\", \"une\", \" phrase\")")

        var response=goobj.callrpc("palette","Palette.Cat", JSON.stringify(["une","deux"]))
        goobj.write("Just after goobj.callRPC"+response)
        console.log(response)
    }
}


