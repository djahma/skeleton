package main

import (
	"errors"
	"fmt"
	"strings"
)

func Parse(str string) error {
	fmt.Println(skel.LogPrefix(), "Received StdIn message:", str)

	strs := strings.Split(str, " ")
	if len(strs) <= 1 {
		e := fmt.Sprintln(skel.LogPrefix(),
			"Parsing error in, not enough elements in given string",
			strs)
		return errors.New(e)
	}
	if strs[0] == "Get" {
		switch strs[1] {
		case "pluginRpcAddress":
		default:
			fmt.Println(skel.LogPrefix(), "Unrecognised Get request", strs)
		}
	} else if strs[0] == "Push" {
		switch strs[1] {
		case "quit":
			fmt.Println(skel.LogPrefix(), "Received Quit signal in palette")
			plug.QuitChan <- 1
		case "log":
			fmt.Println(skel.LogPrefix(), strings.Join(strs[2:], " "))
		}
	} else {
		e := fmt.Sprintln("Parsing error in palette, no match for string sequence:",
			strs)
		return errors.New(e)
	}
	return nil
}
