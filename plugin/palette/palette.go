package main

import (
	"encoding/json"
	"fmt"
	"os"
	mui "skeleton/plugin/mainUI/API"
	"strings"

	// "skeleton/mssg"
	"skeleton/API/skeleton"
	"skeleton/plugin"
)

var (
	plug *plugin.Plugin
	skel *skeleton.Skeleton
)

func main() {
	skel = &skeleton.Skeleton{PluginName: "palette"}
	if err := skel.InitRPC(os.Args[1], os.Args[2]); err != nil {
		fmt.Println(err)
		return
	}
	plug = &plugin.Plugin{}
	palette := new(Palette)
	plug.Init(skel, Parse, palette)
	defer func() {
		skel.Close()
		plug.Stop()
	}()
	mainui := &mui.MainUI{}
	mainui.Initialise(plug, skel)
	defer mainui.Close()
	skel.Ok()
	result, err := mainui.Add(7, 5)
	if err != nil {
		fmt.Println(skel.LogPrefix(), err)
	}
	fmt.Println(skel.LogPrefix(), "sending a message from palette stdout. Add rpc=", result, plug.Path)
	_ = mainui.AddMenuItem([]string{"View", "New Tab"}, true, plug.Path+"/qml/menu_view.qml")
	<-plug.QuitChan
	fmt.Println("After plug.QuitChan in palette")

}

type Palette string

type RPCArgs struct {
	Strs   []string
	Runes  []rune
	Ints   []int64
	Floats []float64
	Bools  []bool
}

//	Concatenate strings.
//		args is a stringified json array of strings: ["","",""]
func (p *Palette) Cat(args *string, reply *string) error {
	fmt.Println(skel.LogPrefix(), "In palette Cat func", *args)
	var Strs []string
	json.Unmarshal([]byte(*args), &Strs)
	fmt.Println(skel.LogPrefix(), "In palette Cat func", Strs)
	s := strings.Join(Strs, "")
	*reply = s
	return nil
}

type Args struct {
	X, Y int
}
