package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"skeleton/API/skeleton"
	"skeleton/plugin"

	"gopkg.in/qml.v1"
)

var (
	plug      *plugin.Plugin
	skel      *skeleton.Skeleton
	qmlEngine *qml.Engine
	qmlCtxt   *qml.Context
	qmlWindow *qml.Window
)

func main() {
	defer func() {
		if x := recover(); x != nil {
			fmt.Println(skel.LogPrefix(), "run time panic: %s", x)
			skel.Quit()
		}
	}()
	//	Initialise Skeleton connection
	// skel = skeleton.NewSkeleton("mainUI", os.Args[1], os.Args[2])
	// skel = &skeleton.Skeleton{PluginName: "mainUI"}
	var err error
	skel, err = skeleton.NewSkeleton("mainUI", os.Args[1], os.Args[2])
	if err != nil {
		fmt.Println(skel.LogPrefix(), err)
		return
	}
	//	Initialise mainUI RPC through the plugin object
	plug = &plugin.Plugin{}
	mui := new(MainUI)
	plug.Init(skel, Parse, mui)
	defer func() {
		skel.Close()
		plug.Stop()
	}()

	err = skel.Println("UN MESSAGE DEPUIS mainUI sur SKELETON RPC")
	if err != nil {
		fmt.Println(skel.LogPrefix(), "error on rpc:", err)
	}

	qml.Run(run)
}

func run() error {
	pwd, _ := filepath.Abs(filepath.Dir(os.Args[0]))
	qmlDir := pwd + string(os.PathSeparator) + "qml"
	fmt.Println(skel.LogPrefix(), "qmlDir="+qmlDir)
	qml.SetLogger(log.New(os.Stdout, skel.LogPrefix(), 0))
	qmlEngine = qml.NewEngine()
	defer qmlEngine.Destroy()
	qmlCtxt = qmlEngine.Context()
	defer qmlCtxt.Destroy()

	qml.RegisterTypes("GoExtensions", 1, 0, []qml.TypeSpec{
		{Init: func(p *Person, obj qml.Object) { p.Name = "none" }},
		{Init: func(g *GoObj, obj qml.Object) { g.sk = skel }},
	})

	component, err := qmlEngine.LoadFile(qmlDir + string(os.PathSeparator) + "skeletonApp.qml")
	if err != nil {
		fmt.Println(skel.LogPrefix(), "on vient de paniquer:", err.Error())
		skel.Quit()
		panic(err)
	}
	qmlWindow = component.CreateWindow(nil)
	defer qmlWindow.Destroy()
	// fmt.Println(ob.String("objectName"))
	// ob.Set("title", "qml.RunMain")
	// window.Set("title", "un autre titre")
	qmlWindow.On("closing", func() { skel.Quit() })
	//window.Set("height", 400)
	//window.Set("width", 300)

	qmlWindow.Set("x", 10)
	qmlWindow.Set("y", 10)
	qmlWindow.Show()
	qmlWindow.Wait()
	return nil
}

type Person struct {
	Name string
}

func (p *Person) Write(str string, i int) {
	i += 1
	fmt.Println(skel.LogPrefix(), "un message depuis depuis qml event", str, i)

}
