package mainUI

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/rpc"
	"net/rpc/jsonrpc"
	"skeleton/API/skeleton"
	"skeleton/plugin"
	"time"
)

type MainUI struct {
	plug    *plugin.Plugin
	timeout time.Duration
	skel    *skeleton.Skeleton
	client  *rpc.Client
}

func (m *MainUI) Initialise(p *plugin.Plugin, s *skeleton.Skeleton) {
	m.plug = p
	m.skel = s
	addr, port, err := m.skel.GetRpcCoords("mainUI")
	// rpcAddress := m.skel.GetPluginRpcAddress("mainUI", p.Name)
	client, err := jsonrpc.Dial("tcp", addr+":"+port)
	// client, err := jsonrpc.Dial("tcp", rpcAddress)

	// client, err := rpc.DialHTTP("tcp", rpcAddress)
	if err != nil {
		fmt.Println("dialing:", err)
		return
	}
	m.client = client
}
func (m *MainUI) Close() {
	m.client.Close()
}

type Add struct {
	A int
	B int
	C int
}

func (m *MainUI) Add(a int, b int) (int, error) {
	type Args struct {
		X, Y int
	}
	args := &Args{a, b}
	var reply int
	err := m.client.Call("MainUI.Add", args, &reply)
	if err != nil {
		fmt.Println(m.skel.LogPrefix(), "MainUI.Add rpc error:", err)
		return -1, err
	}
	return reply, nil
}

type MenuArgs struct {
	MenuPath []string
	IsPath   bool
	ObjPath  string
}

//	Add a menu item to top application menu
//		mipath is an array of string matching the menu hierarchy
//		isPath is true if following arg is a path to a qml file, or false if it is a qml string
//		qmlPath is either a path, or a string containing a qml object.
func (m *MainUI) AddMenuItem(mipath []string, isPath bool, qmlPath string) error {
	b, _ := json.Marshal(&MenuArgs{mipath, isPath, qmlPath})
	var err string
	if e := m.client.Call("MainUI.AddMenuItem", string(b), &err); e != nil {
		fmt.Println(m.skel.LogPrefix(), e)
		return e
	} else {
		if err != "" {
			fmt.Println(m.skel.LogPrefix(), err)
			return errors.New(err)
		}
	}
	return nil
}

func (m *MainUI) AddSkTab(isPath bool, qmlPath string) error {
	fmt.Println("qmlPath=", qmlPath)
	b, _ := json.Marshal(&MenuArgs{IsPath: isPath, ObjPath: qmlPath})
	var err string
	if e := m.client.Call("MainUI.AddSkTab", string(b), &err); e != nil {
		fmt.Println(m.skel.LogPrefix(), e)
		return e
	} else {
		if err != "" {
			fmt.Println(m.skel.LogPrefix(), err)
			return errors.New(err)
		}
	}
	return nil
}

func (m *MainUI) RegisterPreferenceTab(pluginName string, isPath bool, qmlPath string) error {
	b, _ := json.Marshal(&MenuArgs{[]string{pluginName}, isPath, qmlPath})
	var err string
	if e := m.client.Call("MainUI.RegisterPreferenceTab", string(b), &err); e != nil {
		fmt.Println(m.skel.LogPrefix(), e)
		return e
	} else {
		if err != "" {
			fmt.Println(m.skel.LogPrefix(), err)
			return errors.New(err)
		}
	}
	return nil
}
