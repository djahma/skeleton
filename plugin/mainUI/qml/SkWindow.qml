import QtQuick 2.0
import QtQuick.Window 2.1
import "../Themes"

Window{
    id:extwin
    objectName: "subWindow"
    property string type: "SkWindow"
    width:300
    height: 200
//    minimumHeight: 300
//    minimumWidth: 200
    color: Theme.window.backgroundActive
    modality: Qt.NonModal
    title: "External Window"
    property alias dock: rootdock
    property var mainWindow

    /*
      SkDock holds the Rows/Columns containing the SkTabDocks
      */
    SkDock{
        id: rootdock
        topwindow: extwin
    }

    Component.onCompleted: {
    }

    onClosing: {
        mainWindow.removeWindow(extwin)
    }

    //  Move an SkTab or SkTabDock accross windows
    function moveTabOrTabDock(obj, point){
        var point2, found
        var newWindow, foundTabDock
        var retVal=null
//        if(obj.objectName==="tab"){
        if(obj.type && obj.type==="SkTab"){
            point2 = extwin.windowToScreenPoint(point)
            found = extwin.mainWindow.windowAtPoint(point2)
            //  No window is found so we create an extwin to accomodate the tab
            if(found===null){
                console.log("SkWindow.moveTabOrTabdock: No window found under point", point2.x,point2.y)
                newWindow = extwin.mainWindow.createWindowAt(point2)
                obj.tabdock.removeTab(obj.index)
                newWindow.dock.addTab(obj)
                newWindow.visible=true
            }
            else{
                //  If we reach this line, a window has been found
                console.log("SkWindow.moveTabOrTabdock: Found a window under point", point2.x, point2.y)
                if(found!==extwin){
                    obj.tabdock.removeTab(obj.index)
                    foundTabDock = found.dock.tabdockAtPoint(point2)
                    if(foundTabDock!==null){foundTabDock.addTab(obj)}
                    else {found.dock.addTab(obj)}
                }
            }
        }
//        else if(obj.objectName==="tabdock"){
        else if(obj.type && obj.type==="SkTabDock"){
            point2 = extwin.windowToScreenPoint(point)
            found = extwin.mainWindow.windowAtPoint(point2)
            if(found===null){
                console.log("SkWindow.moveTabOrTabdock: tabdock section, No window found under point", point2.x,point2.y)
                 newWindow = extwin.mainWindow.createWindowAt(point2)
                newWindow.height=obj.height+24  //  +24 for the window frame
                newWindow.width=obj.width+2 //+2 for the window border
                dock.removeTabDock(obj)
                newWindow.dock.addTabDock(obj)
                newWindow.visible = true
                retVal=newWindow
            }
            else{
                console.log("SkWindow.moveTabOrTabdock: tabdock section, Found a window under point", point2.x, point2.y)
                if(found!==extwin){
                    retVal=found
                    dock.removeTabDock(obj)
                    foundTabDock = found.dock.tabdockAtPoint(point2)
                    if(foundTabDock!==null){
                        foundTabDock.dock.moveTabDockToDirection(obj, foundTabDock, "left")
                    }
                    else {found.dock.addTabDock(obj) }
                }
            }
        }
        dock.cleanEmptyTabDocks()

        if(dock.__docks.count===0){
            console.log("Closing window now")
            extwin.mainWindow.removeWindow(extwin)
//            extwin.close()
        }
        return retVal
    }

//    //  Create an SkWindow at given point in screen coordinates
//    function createWindowAt(point){
//        console.log("SkWindow.createWindowAt(", point.x, ",", point.y,")")
//        var comp=Qt.createComponent("SkWindow.qml")
//        var rootWindow=mainWindow
////        for(var i=0; i<extwin.__windows.count; i++){
////            if(extwin.__windows.get(i).window.objectName==="rootApp")rootWindow=extwin.__windows.get(i).window
////        }
//        var window=comp.createObject(rootWindow)
//        window.x=point.x; window.y=point.y;
//        rootWindow.registerWindow(window)
//        //  We register all currently known windows to the newly created window
//        for(i=0; i<rootWindow.__windows.count; i++){
//            window.registerWindow(rootWindow.__windows.get(i).window)
//        }
//        return window
//    }

//    //  Adds win SkWindow to the __windows list if it is not already registered
//    function registerWindow(win){
//        for(var i=0; i<__windows.count; i++){
//            if(win===__windows.get(i).window)return
//        }
//        __windows.append({window:win})
//    }

//    //  Removes SkWindow from the __windows list
//    function removeWindow(win){
//        for(var i=0; i<__windows.count; i++){
//            if(win===__windows.get(i).window){
//                win.close()
//                __windows.remove(i)
//            }
//        }
//    }

    //  Map a point in extwin coordinates to Screen coordinates
    function windowToScreenPoint(point){
        var screenPoint=Qt.point(point.x, point.y)
        screenPoint.x += extwin.x
        screenPoint.y += extwin.y
        return screenPoint
    }
//    //  Map a point in screen coordinates to extwin coordinates
//    function screenToWindowPoint(point){
//        var winPoint = Qt.point(point.x, point.y)
//        winPoint.x -= extwin.x
//        winPoint.y -= extwin.y
//        return winPoint
//    }

//    //  return the window at Point point in screen coordinates
//    //  or null if none is found
//    function windowAtPoint(point){
//        var withinX, withinY
//        var currentWin
//        for(var i=0; i<__windows.count; i++){
//            withinX=false; withinY=false
//            currentWin = __windows.get(i).window
//            if( (point.x > currentWin.x) && (point.x < (currentWin.x + currentWin.width)) )
//                withinX = true
//            if( (point.y > currentWin.y) && (point.y < (currentWin.y + currentWin.height)) )
//                withinY = true
//            if(withinX && withinY) return currentWin
//        }
//        return null
//    }

}
