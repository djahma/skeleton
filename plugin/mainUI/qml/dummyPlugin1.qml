import QtQuick 2.0
import QtQuick.Controls 1.1

SkTab{
    title: "title1"
    Flickable{
        anchors.fill: parent
        contentHeight: contentItem.childrenRect.height
        contentWidth:  contentItem.childrenRect.width
        Item{
            width:childrenRect.width
            height: childrenRect.height
            Button{
                id: premboutton
                text: "un boutton pour voir..."
                onClicked: {
                    console.log("itemUnderPoint:", topdock.itemUnderPoint())
                }
            }
            Button{
                text:"un deuxieme long boutton"
                anchors.left: premboutton.right
            }}
    }
}
