import QtQuick 2.0
import QtQuick.Window 2.0
import "../Themes"

Rectangle {
    id:tabhead;
    objectName: "tabhead"
    property string type: "SkTabHead"
    clip: true
    height: titletext.contentHeight+10;
    width: Math.min(
               titletext.contentWidth+titletext.contentHeight+(titletext.anchors.leftMargin*2),
                    Theme.control.tab.widthMax);
    radius: Theme.control.button.radius
    border.width: Theme.border.width.thin
    border.color: Theme.border.color.dark
    gradient: Gradient {
        GradientStop { position: 0.0; color: tabhead.isSelected ? Theme.control.color.selected:Theme.window.backgroundActive; }
        GradientStop { position: 0.3276; color: tabhead.isSelected ? Theme.control.color.backgroundActive:Theme.window.backgroundActive;}
//        GradientStop { position: 1; color: tabhead.isSelected ? Theme.control.color.backgroundActive:Theme.window.backgroundActive; }
    }
    // Bottom-Up hierarchy is: TABHEAD.tabbar.tabdock.dock.topwindow
    property var tabbar
    property string title: "dummy title";
    property bool isSelected: true;
    property int index
    property alias dragMouseArea: titlearea

    Text{
        id:titletext;
        text:title;
        elide: Text.ElideRight;
        color: tabhead.isSelected? Theme.text.color.selected: Theme.text.color.active;
        verticalAlignment: Text.AlignBottom
        anchors.verticalCenter: tabhead.verticalCenter
        anchors.left: tabhead.left
        anchors.leftMargin: 3
        width:{
            if(contentWidth+contentHeight+(anchors.leftMargin*2)>Theme.control.tab.widthMax){
                Theme.control.tab.widthMax-contentHeight-(anchors.leftMargin*2)
            }
        }
        MouseArea{
            id: titlearea
            anchors.fill: parent
            drag.target:tabhead
            drag.axis: Drag.XandYAxis
            onClicked: {
                tabhead.tabbar.tabdock.dock.topwindow.title=title
                tabbar.tabdock.currentIndex=index
            }
            onPositionChanged: {
                if(tabhead.Drag.target && tabhead.Drag.active){
                    if(tabhead.Drag.target.droppart==="tabbar"){
                        if(tabhead.Drag.target.parent!==tabhead && tabhead.tabbar.tabdock===tabhead.Drag.target.parent.tabbar.tabdock)
                        if(tabhead.Drag.target.parent.index-tabhead.index<0)
                            tabhead.Drag.target.parent.x=(tabhead.x/(tabhead.Drag.target.parent.index-tabhead.index))
                        else tabhead.Drag.target.parent.x=-(tabhead.x/(tabhead.Drag.target.parent.index-tabhead.index))
                    }
                }
            }
            onReleased: {
                if(tabhead.Drag.target && tabhead.Drag.active){ //  we're hovering a target
                    if(tabhead.Drag.target.droppart==="tabbar"){    //  target is the tabhead area
                        if(tabhead.Drag.target.parent!==tabhead
                                && tabhead.tabbar.tabdock===tabhead.Drag.target.parent.tabbar.tabdock){
                            //  within same tabdock we swap tabheads
                            tabbar.tabdock.currentIndex=tabhead.Drag.target.index
                            tabbar.tabdock.moveTab(tabhead.index,tabhead.Drag.target.index)
                        }
                        else{tabhead.x=0; tabhead.y=0}  //  Hovering a different tabdock: do nothing
                    }
                    else if(tabhead.Drag.target.droppart==="tabbody"){  //  Hovering content area
                        tabbar.tabdock.dock.moveTabToDirection(tabbar.tabdock.__tabs.get(index).tab, tabhead.Drag.target.dock,tabhead.Drag.target.direction)
                    }
                    else{   //  Hovering anything else, we put tabhead back where it was
                         tabhead.x=0; tabhead.y=0
//                        tabbar.tabdock.updateTabDisplay(tabbar.tabdock.currentIndex)
                    }
                }
                else {  //  Drag is dropped outside current window
                    var tw=tabhead.tabbar.tabdock.dock.topwindow
                    var obj=titlearea.mapToItem(null,titlearea.mouseX, titlearea.mouseY)
                    tabhead.Drag.cancel()
                    //  This test is because preference window is a std Window, not an SkWindow
                    if (typeof tw["moveTabOrTabDock"] === "function") {
                        tw.moveTabOrTabDock(tabbar.tabdock.__tabs.get(index).tab, obj)
                    }
                    tabbar.tabdock.updateTabDisplay(tabbar.tabdock.currentIndex)
                }
            }
        }
    }

    Image {
        id:closetabimg;
        source: "../Themes/default/images/window-close.svg"
        anchors.left:titletext.right;
        anchors.verticalCenter: tabhead.verticalCenter
        anchors.rightMargin: 1;
        anchors.leftMargin: 2;
        height: titletext.height;
        width: height;
        fillMode: Image.PreserveAspectFit;
        visible:{
            var tw = tabhead.tabbar.tabdock.dock.topwindow
            //  This test is because preference window is a std Window, not an SkWindow
            //  In preference window, tabs mustn't be closable
            if (typeof tw["moveTabOrTabDock"] === "function") {
                (tabheadarea.containsMouse || closeMouse.containsMouse)? true:false;
            }
            else false
        }
        MouseArea {
            id: closeMouse
            hoverEnabled: true
            anchors.fill: parent;
            z:2;
            onClicked:{ tabbar.tabdock.removeTab(index) }
        }
    }
    //  when hovered, the close button appears in the tab
    MouseArea{
        id:tabheadarea;
        anchors.fill: parent;
        hoverEnabled: true;
        z:-1;
    }
    //  Relates to Rectangle{id:tabhead}
    //  Drag stuff
    Drag.active: titlearea.drag.active
    Drag.keys:["tabheaddrag"]
    Drag.hotSpot.x:titlearea.mouseX
    Drag.hotSpot.y: titlearea.mouseY
    Drag.onActiveChanged:{
        console.log(index,Drag.active, Drag.keys.toString())
    }

    DropArea{
        id:tabdroparea
        keys:["tabheaddrag"]
        anchors.fill: parent
        property int index : tabhead.index
        property string droppart: "tabbar"

        Binding{
            target:tabdroparea
            property: "visible"
            value: titlearea.drag.active? false:true
        }
        states:[
            State{
                when:tabhead.x!==0
                PropertyChanges {
                    target: tabdroparea
                    width: parent.width
                    height: parent.height
                    anchors.fill: undefined
                    x:-tabhead.x
                }
            }
        ]
    }

    states:[
        State{
            when: titlearea.drag.active
            PropertyChanges {
                target: tabhead
                opacity: 0.618
                border.color: Theme.border.color.focus
            }
        },
        State{
            when: index===tabbar.tabdock.currentIndex
            PropertyChanges{
                target: tabhead
                isSelected: true
            }
        },
        State{
            when: index!==tabbar.tabdock.currentIndex
            PropertyChanges{
                target: tabhead
                isSelected:false
            }
        }
    ]
   Behavior on x {
        PropertyAnimation { properties: "x"; easing.type: Easing.InOutQuad }
    }
   Behavior on y {
        PropertyAnimation { properties: "y"; easing.type: Easing.InOutQuad }
    }
}
