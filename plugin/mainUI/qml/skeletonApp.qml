import QtQuick 2.1
import QtQuick.Controls 1.0
import QtQuick.Window 2.0
import QtQuick.Layouts 1.1
import "../Themes"
import GoExtensions 1.0

ApplicationWindow {
    id: rootApp
    objectName: "rootApp"

    title: qsTr("WinDock")
    width: 618
    height: 382
    property ListModel __windows: ListModel{}
    property alias dock: rootdock
    property var preferenceWindow
    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("Exit")
                onTriggered: {
//                    Qt.quit();
                    goobj.quit()
                }
            }
            MenuItem {
                text: qsTr("Test")
                onTriggered: {
                    person.write("Un message bidon", 5)
                    console.log(typeof(rootApp))
                }
            }
        }
        Menu{
            title:qsTr("Edit")
            MenuSeparator{}
            MenuItem{
                text: qsTr("Preferences...")
                onTriggered: {

                    preferenceWindow.visible=true
                }
            }
        }
        Menu{
            title:qsTr("View")
            Menu{
                title: qsTr("New Tab")
            }
        }
        Menu{
            title: qsTr("Help")
        }
    }

    SkDock{
        id: rootdock
        objectName: "topdock"
        topwindow: rootApp
    }
    Person {
        id: person
    }
    GoObj{
        id: goobj
    }

    statusBar: Rectangle{
        width: parent.width
        height: childrenRect.height
        color: Theme.window.backgroundActive

        RowLayout {
//            anchors.fill: parent
            Label { text: "Read Only" }
        }
    }

    Component.onCompleted: {
        __windows.append({window:rootApp})
        var comp=Qt.createComponent("./preferences/preferences.qml")
        preferenceWindow=comp.createObject(rootApp)
        createDummyPlugins()
        goobj.okToSkeleton()
    }
    //A quick function to build a dummy environment for testing purpose
    function createDummyPlugins(){
        console.log("createDummyPlugin()")
        var comp=Qt.createComponent("./SkTabDock.qml")
        if(comp.status===Component.Ready){
            console.log("createDummyPlugin(): SkTabDock Component is ready")
            var dock1=comp.createObject(rootApp)
            var dock2=comp.createObject(rootApp)
            console.log("createDummyPlugin():naming it dock1")
            console.log("createDummyPlugin(): parenting it to rootdock")
            console.log("createDummyPlugin(): rootdock.addTabDock(obj)")
            rootdock.addTabDock(dock1)
            rootdock.addTabDock(dock2)
        }
        else if(comp.status===Component.Error){
            console.log(comp.errorString())
        }

        comp=Qt.createComponent("./dummyPlugin1.qml")
        var comp2=Qt.createComponent("./dummyPlugin2.qml")
        var comp3=Qt.createComponent("./dummyPluginBasic.qml")
        var comp4=Qt.createComponent("./dummyPaletteActive.qml")
        var comp5=Qt.createComponent("./dummyPaletteInActive.qml")
        var comp6=Qt.createComponent("./dummyPaletteDisabled.qml")

        if(comp.status===Component.Ready){
            var obj=comp.createObject(rootdock)
            dock1.addTab(obj)
            obj=comp.createObject(rootdock)
            dock2.addTab(obj)
            obj=comp.createObject(rootdock)
            dock1.addTab(obj)
            obj=comp2.createObject(rootdock)
            dock2.addTab(obj)
            obj=comp2.createObject(rootdock)
            dock2.addTab(obj)
            obj=comp3.createObject(rootdock)
            dock2.addTab(obj)
            obj=comp4.createObject(rootdock)
            dock1.addTab(obj)
            obj=comp5.createObject(rootdock)
            dock1.addTab(obj)
            obj=comp6.createObject(rootdock)
            dock1.addTab(obj)
        }
    }

    /*  Move an SkTab or SkTabDock accross windows
      point is in window coordinates
      */
    function moveTabOrTabDock(obj, point){
        console.log("windock.moveTabOrTabdock(", point.x,point.y,")")
        var point2, found
        var newWindow, foundTabDock
        var retVal=null
//        if(obj.objectName==="tab"){
        if(obj.type && obj.type==="SkTab"){
             point2 = rootApp.windowToScreenPoint(point)
            found = rootApp.windowAtPoint(point2)
            //  No window is found so we create an extwin to accomodate the tab
            if(found===null){
                console.log("windock.moveTabOrTabdock: No window found under point", point2.x,point2.y)
                newWindow = rootApp.createWindowAt(point2)
                obj.tabdock.removeTab(obj.index)
                newWindow.height=obj.tabdock.height
                newWindow.width=obj.tabdock.width
                newWindow.dock.addTab(obj)
                newWindow.visible=true
                return
            }
            else{
                //  If we reach this line, a window has been found
                console.log("windock.moveTabOrTabdock: Found a window under point", point2.x, point2.y)
                if(found!==rootApp){
                    console.log("windock.moveTabOrTabdock, found is different from rootApp:", found.type)
                    obj.tabdock.removeTab(obj.index)
                    foundTabDock = found.dock.tabdockAtPoint(point2)
                    if(foundTabDock!==null){
                        console.log("windock.moveTabOrTabdock, foundTabDock.type=",foundTabDock.type)
                        foundTabDock.addTab(obj)}
                    else {
                        console.log("windock.moveTabOrTabdock, foundTabDock is null")
                        found.dock.addTab(obj)}
                }
            }
        }
//        else if(obj.objectName==="tabdock"){
        else if(obj.type && obj.type==="SkTabDock"){
            point2 = rootApp.windowToScreenPoint(point)
            found = rootApp.windowAtPoint(point2)
            var tempTab
            if(found===null){
                console.log("windock.moveTabOrTabdock: No window found under point", point2.x,point2.y)
                 newWindow = rootApp.createWindowAt(point2)
                newWindow.height=obj.height+24  //  +24 for the window frame
                newWindow.width=obj.width+2 //+2 for the window border
                dock.removeTabDock(obj)
                newWindow.dock.addTabDock(obj)
                newWindow.visible=true
                retVal=newWindow
            }
            else{
                console.log("windock.moveTabOrTabdock: Found a window under point", point2.x, point2.y)
                if(found!==rootApp){
                    retVal=found
                    dock.removeTabDock(obj)
                    foundTabDock = found.dock.tabdockAtPoint(point2)
                    if(foundTabDock!==null){
                        foundTabDock.dock.moveTabDockToDirection(obj, foundTabDock, "left")
                    }
                    else{ found.dock.addTabDock(obj)}
                }
            }
        }
        dock.cleanEmptyTabDocks()
        return retVal
    }

    //  Create an SkWindow at given point in screen coordinates
    function createWindowAt(point){
        console.log("SkWindow.createWindowAt(", point.x, ",", point.y,")")
        var comp=Qt.createComponent("./SkWindow.qml")
        var window=comp.createObject(rootApp)
        window.mainWindow=rootApp
        window.x=point.x; window.y=point.y;
        rootApp.registerWindow(window)
        return window
    }

    //  Adds win SkWindow to the __windows list
    function registerWindow(win){
        for(var i=0; i<__windows.count; i++){
            if(win===__windows.get(i).window)return
        }
        //  Reaching here means win is not an existing window
        __windows.append({window:win})
    }

    //  Removes SkWindow from the __windows list
    function removeWindow(win){
        for(var i=0; i<__windows.count; i++){
            if(win===__windows.get(i).window){
                win.close()
                __windows.remove(i)
            }
        }
    }

    //  Map a point in rootApp coordinates to Screen coordinates
    function windowToScreenPoint(point){
        var screenPoint=Qt.point(point.x, point.y)
        screenPoint.x += rootApp.x
        screenPoint.y += rootApp.y
        return screenPoint
    }
    //  Map a point in screen coordinates to rootApp coordinates
    function screenToWindowPoint(point){
        var winPoint = Qt.point(point.x, point.y)
        winPoint.x -= rootApp.x
        winPoint.y -= rootApp.y
        return winPoint
    }

    //  return the window at Point point in screen coordinates
    //  or null if none is found
    function windowAtPoint(point){
        var withinX, withinY
        var currentWin
        console.log("windock.windowAtpoint __windows.count=",__windows.count)
        for(var i=0; i<__windows.count; i++){
            withinX=false; withinY=false
            currentWin = __windows.get(i).window
            console.log("windock.windowAtpoint(",point.x,point.y,") i=",i," x=",currentWin.x, "y=",currentWin.y,"width=",currentWin.width,"height=",currentWin.height)
            if( (point.x > currentWin.x) && (point.x < (currentWin.x + currentWin.width)) )
                withinX = true
            if( (point.y > currentWin.y) && (point.y < (currentWin.y + currentWin.height)) )
                withinY = true
            if(withinX && withinY) return currentWin
        }
        return null
    }

    onContentItemChanged: {
        console.log("CONTENTITEM CHANGED:", contentItem.height, contentItem.width)
    }
    contentItem.onHeightChanged:{
        console.log("CONTENTITEM HEIGHT CHANGED:", contentItem.height, contentItem.width)
    }
    contentItem.onWidthChanged:{
        console.log("CONTENTITEM WIDTH CHANGED:", contentItem.height, contentItem.width)
    }
}
