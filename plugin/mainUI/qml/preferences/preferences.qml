import QtQuick 2.0
import QtQuick.Window 2.0
import QtQuick.Controls 1.2
import "../../Themes"
import "../../qml"

Window{
    id:prefwin
    title: "Preferences"
    modality:Qt.WindowModal
    color: Theme.window.backgroundActive
    property alias tabview:tabviewer
    minimumWidth: 610
    minimumHeight: 377
    SkDock{
        id: tabviewer
        topwindow: prefwin
        Component.onCompleted: {
            var comp=Qt.createComponent("../SkTabDock.qml")
            var dock1=comp.createObject(prefwin)
            tabviewer.addTabDock(dock1)
            var comp2=Qt.createComponent("../dummyPluginBasic.qml")
            var newTab=comp2.createObject(prefwin)
            newTab.title="Active Palette"
            dock1.addTab(newTab)
        }
    }

    function registerNewTab(plugName, isPath, path){
        console.log("registerNewTab(", plugName,",", isPath,",", path,")")
        if(isPath){
            var comp=Qt.createComponent(path)
            var tabobj=comp.createObject(prefwin)
            tabobj.title=plugName
            tabviewer.addTab(tabobj)
//            var tabobj =tabviewer.addTab(plugName,comp)
//            tabobj.parent=tabviewer
        }
        else{

        }
    }
}

