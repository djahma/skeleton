import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3

SpinBox{
    style: SpinBoxStyle{
        background: TextField {}
        decrementControl: Button{
            iconName: "down"
            checked: true
            checkable: true
            implicitWidth: 20
            anchors.fill: parent
            anchors.bottomMargin: 1
        }
        incrementControl: Button{
            iconName: "up"
            checked: true
            checkable: true
            implicitWidth: 20
            anchors.fill: parent
            anchors.topMargin: 1
        }
    }

}
