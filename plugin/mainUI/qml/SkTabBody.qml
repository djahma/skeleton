import QtQuick 2.0
import "../Themes"
import QtQuick.Controls 1.1

Rectangle {
    id: bodyRoot
    property ListModel __bodies: ListModel {}
    property alias dragLayer: bodyDropArea
    property var tabdock
    clip: true
    signal update(int index)

    onUpdate: {
        console.log("SkTabBody.onUpdate(",index,") in",tabdock.objectName,", length=", __bodies.count)
        bodyRoot.children=""
        dragLayer.parent=bodyRoot
        for(var i=0;i<__bodies.count;i++){
            __bodies.get(i).item.visible=false
            if(i==index){
                __bodies.get(i).item.visible=true
            }
            __bodies.get(i).item.parent=bodyRoot
        }

    }

    color:Theme.control.color.backgroundActive;

    Rectangle {
        id: bodyDropArea
        opacity: 0
        anchors.fill: parent
        color: Theme.control.color.backgroundDarkActive
        property var keys: ['tabheaddrag','tabdockdrag']
        //  Drop holes are dead areas which should be painted as active areas
        DropArea{
            id: drophole1
            anchors.top:parent.top;anchors.bottom: dropLeftMost.top
            anchors.left:parent.left;anchors.right:dropUpMost.left
        }
        DropArea{
            id: drophole2
            anchors.top:parent.top;anchors.bottom: dropRightMost.top
            anchors.left:dropUpMost.right;anchors.right:parent.right
        }
        DropArea{
            id: drophole3
            anchors.top:dropUpMost.bottom;anchors.bottom: dropLeft.top
            anchors.left:dropLeftMost.right;anchors.right:dropUp.left
        }
        DropArea{
            id: drophole4
            anchors.top:dropUpMost.bottom;anchors.bottom: dropRight.top
            anchors.left:dropUp.right;anchors.right:dropRightMost.left
        }
        DropArea{
            id: drophole5
            anchors.top:dropLeft.bottom;anchors.bottom: dropDownMost.top
            anchors.left:dropLeftMost.right;anchors.right:dropDown.left
        }
        DropArea{
            id: drophole6
            anchors.top:dropRight.bottom;anchors.bottom: dropDownMost.top
            anchors.left:dropDown.right;anchors.right:dropRightMost.left
        }
        DropArea{
            id: drophole7
            anchors.top:dropLeftMost.bottom;anchors.bottom: parent.bottom
            anchors.left:parent.left;anchors.right:dropDownMost.left
        }
        DropArea{
            id: drophole8
            anchors.top:dropRightMost.bottom;anchors.bottom: parent.bottom
            anchors.left:dropDownMost.right;anchors.right:parent.right
        }
        //  Active areas to drop SkTabHead or SkTabDock
        DropArea {
            id: dropIn
            property string direction: qsTr("in")
            property string droppart: "tabbody"
            property alias dock: bodyRoot.tabdock
            keys: bodyDropArea.keys
            width: parent.width * 0.382
            height: parent.height * 0.292
            x: parent.width * 0.309
            y: parent.height * 0.354
            Image {
                id: bodyDrop_in
                opacity: 0.4
                source: "../Themes/default/images/tab-new.svg"
                anchors.fill: dropIn
                fillMode: Image.PreserveAspectFit
            }
        }
        DropArea {
            id: dropUp
            property string direction: qsTr("up")
            property string droppart: "tabbody"
            property alias dock: bodyRoot.tabdock
            keys: bodyDropArea.keys
            width: parent.width * 0.382
            height: parent.height * 0.208
            x: parent.width * 0.309
            y: parent.height * 0.146
            Image {
                id: bodyDrop_up
                opacity: 0.4
                source: "../Themes/default/images/go-up.svg"
                anchors.fill: dropUp
                fillMode: Image.PreserveAspectFit
            }
        }
        DropArea {
            id: dropDown
            property string direction: qsTr("down")
            property string droppart: "tabbody"
            property alias dock: bodyRoot.tabdock
            keys: bodyDropArea.keys
            width: parent.width * 0.382
            height: parent.height * 0.208
            x: parent.width * 0.309
            y: parent.height * 0.646
            Image {
                id: bodyDrop_down
                opacity: 0.4
                source: "../Themes/default/images/go-up.svg"
                anchors.fill: dropDown
                rotation: 180
                fillMode: Image.PreserveAspectFit
            }
        }
        DropArea {
            id: dropLeft
            property string direction: qsTr("left")
            property string droppart: "tabbody"
            property alias dock: bodyRoot.tabdock
            keys: bodyDropArea.keys
            width: parent.width * 0.163
            height: parent.height * 0.292
            x: parent.width * 0.146
            y: parent.height * 0.354
            Image {
                id: bodyDrop_left
                opacity: 0.4
                source: "../Themes/default/images/go-up.svg"
                anchors.fill: dropLeft
                rotation: -90
                fillMode: Image.PreserveAspectFit
            }
        }
        DropArea {
            id: dropRight
            property string direction: qsTr("right")
            property string droppart: "tabbody"
            property alias dock: bodyRoot.tabdock
            keys: bodyDropArea.keys
            width: parent.width * 0.163
            height: parent.height * 0.292
            x: parent.width * 0.691
            y: parent.height * 0.354
            Image {
                id: bodyDrop_right
                opacity: 0.4
                source: "../Themes/default/images/go-up.svg"
                anchors.fill: dropRight
                rotation: 90
                fillMode: Image.PreserveAspectFit
            }
        }
        DropArea {
            id: dropUpMost
            property string direction: qsTr("upmost")
            property string droppart: "tabbody"
            property alias dock: bodyRoot.tabdock
            keys: bodyDropArea.keys
            width: parent.width * 0.708
            height: parent.height * 0.146
            x: parent.width * 0.146
            y: 0
            Rectangle {
                id: bodyDrop_upmost
                opacity: 0.4
                color: "transparent"
                anchors.fill: dropUpMost
                Image {
                    source: "../Themes/default/images/go-up.svg"
                    x: parent.width * 1 / 4 - width / 2
                    opacity: parent.opacity
                    fillMode: Image.PreserveAspectFit
                    height: parent.height
                }
                Image {
                    source: "../Themes/default/images/go-up.svg"
                    x: parent.width * 2 / 4 - width / 2
                    opacity: parent.opacity
                    fillMode: Image.PreserveAspectFit
                    height: parent.height
                }
                Image {
                    source: "../Themes/default/images/go-up.svg"
                    x: parent.width * 3 / 4 - width / 2
                    opacity: parent.opacity
                    fillMode: Image.PreserveAspectFit
                    height: parent.height
                }
            }
        }
        DropArea {
            id: dropDownMost
            property string direction: qsTr("downmost")
            property string droppart: "tabbody"
            property alias dock: bodyRoot.tabdock
            keys: bodyDropArea.keys
            width: parent.width * 0.708
            height: parent.height * 0.146
            x: parent.width * 0.146
            y: parent.height * 0.854
            Rectangle {
                id: bodyDrop_downmost
                opacity: 0.4
                color: "transparent"
                anchors.fill: dropDownMost
                Image {
                    source: "../Themes/default/images/go-up.svg"
                    rotation: 180
                    x: parent.width * 1 / 4 - width / 2
                    opacity: parent.opacity
                    fillMode: Image.PreserveAspectFit
                    height: parent.height
                }
                Image {
                    source: "../Themes/default/images/go-up.svg"
                    rotation: 180
                    x: parent.width * 2 / 4 - width / 2
                    opacity: parent.opacity
                    fillMode: Image.PreserveAspectFit
                    height: parent.height
                }
                Image {
                    source: "../Themes/default/images/go-up.svg"
                    rotation: 180
                    x: parent.width * 3 / 4 - width / 2
                    opacity: parent.opacity
                    fillMode: Image.PreserveAspectFit
                    height: parent.height
                }
            }
        }
        DropArea {
            id: dropRightMost
            property string direction: qsTr("rightmost")
            property string droppart: "tabbody"
            property alias dock: bodyRoot.tabdock
            keys: bodyDropArea.keys
            width: parent.width * 0.146
            height: parent.height * 0.708
            x: parent.width * 0.854
            y: parent.height * 0.146
            Rectangle {
                id: bodyDrop_rightmost
                opacity: 0.4
                color: "transparent"
                anchors.fill: dropRightMost
                Image {
                    source: "../Themes/default/images/go-up.svg"
                    rotation: 90
                    y: parent.height * 1 / 4 - height / 2
                    opacity: parent.opacity
                    fillMode: Image.PreserveAspectFit
                    width: parent.width
                }
                Image {
                    source: "../Themes/default/images/go-up.svg"
                    rotation: 90
                    y: parent.height * 2 / 4 - height / 2
                    opacity: parent.opacity
                    fillMode: Image.PreserveAspectFit
                    width: parent.width
                }
                Image {
                    source: "../Themes/default/images/go-up.svg"
                    rotation: 90
                    y: parent.height * 3 / 4 - height / 2
                    opacity: parent.opacity
                    fillMode: Image.PreserveAspectFit
                    width: parent.width
                }
            }
        }
        DropArea {
            id: dropLeftMost
            property string direction: qsTr("leftmost")
            property string droppart: "tabbody"
            property alias dock: bodyRoot.tabdock
            keys: bodyDropArea.keys
            width: parent.width * 0.146
            height: parent.height * 0.708
            x: 0
            y: parent.height * 0.146
            Rectangle {
                id: bodyDrop_leftmost
                opacity: 0.4
                color: "transparent"
                anchors.fill: dropLeftMost
                Image {
                    source: "../Themes/default/images/go-up.svg"
                    rotation: -90
                    y: parent.height * 1 / 4 - height / 2
                    opacity: parent.opacity
                    fillMode: Image.PreserveAspectFit
                    width: parent.width
                }
                Image {
                    source: "../Themes/default/images/go-up.svg"
                    rotation: -90
                    y: parent.height * 2 / 4 - height / 2
                    opacity: parent.opacity
                    fillMode: Image.PreserveAspectFit
                    width: parent.width
                }
                Image {
                    source: "../Themes/default/images/go-up.svg"
                    rotation: -90
                    y: parent.height * 3 / 4 - height / 2
                    opacity: parent.opacity
                    fillMode: Image.PreserveAspectFit
                    width: parent.width
                }
            }
        }
    }

    states: [
        State {
            when: drophole1.containsDrag|drophole2.containsDrag|
                  drophole3.containsDrag|drophole4.containsDrag|
                  drophole5.containsDrag|drophole6.containsDrag|
                  drophole7.containsDrag|drophole8.containsDrag
            PropertyChanges {
                target: bodyDropArea
                opacity: 1
                z:2
            }
        },
        State {
            when: dropIn.containsDrag
            PropertyChanges {
                target: bodyDropArea
                opacity: 1; z:2
            }
            PropertyChanges {
                target: bodyDrop_in
                opacity: 1
            }
        },
        State {
            when: dropUp.containsDrag
            PropertyChanges {
                target: bodyDropArea
                opacity: 1; z:2
            }
            PropertyChanges {
                target: bodyDrop_up
                opacity: 1
            }
        },
        State {
            when: dropDown.containsDrag
            PropertyChanges {
                target: bodyDropArea
                opacity: 1; z:2
            }
            PropertyChanges {
                target: bodyDrop_down
                opacity: 1
            }
        },
        State {
            when: dropLeft.containsDrag
            PropertyChanges {
                target: bodyDropArea
                opacity: 1; z:2
            }
            PropertyChanges {
                target: bodyDrop_left
                opacity: 1
            }
        },
        State {
            when: dropRight.containsDrag
            PropertyChanges {
                target: bodyDropArea
                opacity: 1; z:2
            }
            PropertyChanges {
                target: bodyDrop_right
                opacity: 1
            }
        },
        State {
            when: dropUpMost.containsDrag
            PropertyChanges {
                target: bodyDropArea
                opacity: 1; z:2
            }
            PropertyChanges {
                target: bodyDrop_upmost
                opacity: 1
            }
        },
        State {
            when: dropDownMost.containsDrag
            PropertyChanges {
                target: bodyDropArea
                opacity: 1; z:2
            }
            PropertyChanges {
                target: bodyDrop_downmost
                opacity: 1
            }
        },
        State {
            when: dropRightMost.containsDrag
            PropertyChanges {
                target: bodyDropArea
                opacity: 1; z:2
            }
            PropertyChanges {
                target: bodyDrop_rightmost
                opacity: 1
            }
        },
        State {
            when: dropLeftMost.containsDrag
            PropertyChanges {
                target: bodyDropArea
                opacity: 1; z:2
            }
            PropertyChanges {
                target: bodyDrop_leftmost
                opacity: 1
            }
        }
    ]
}
