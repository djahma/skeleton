import QtQuick 2.0
import "../Themes"

Rectangle{
    id:bar
    objectName: "tabbar"
    property string type: "SkTabBar"
    // Bottom-Up hierarchy is: tabhead.TABBAR.tabdock.dock.topwindow
    property var tabdock
    property alias dragtabdock:mousemovetab
    height: Theme.control.tab.heightInactive+(Theme.control.button.radius*2)
    anchors.left: parent.left
    anchors.leftMargin: Theme.border.width.normal
    anchors.right: parent.right
    anchors.rightMargin: Theme.border.width.normal
    anchors.top: parent.top
    anchors.topMargin: Theme.border.width.normal
    color: Theme.window.backgroundActive
    property ListModel __heads: ListModel{}

    signal update(int index)
    /*
      Update signal refreshes the list of tabs and the way they are displayed in rowbar
      Pulls its tab headers from __heads list
      */
    onUpdate: {
        //  We force rowbar to redraw the heads from __heads model and thus, fill contentItem in.
         rowbar.forceLayout()
        console.log("SkTabBar.onUpdate(",index,") , length=", __heads.count)
        var flick=rowbar.contentItem
        for(var i=0;i<flick.children.length;i++){
            var item=flick.children[i]
//            if(item.objectName!=="wrapper")continue
            if(!item.type || item.type!=="wrapper")continue
//            console.log("item geometry x y height width", item.x, item.y, item.height, item.width)
            if(item.children.length===0)item.parent=null
            else{
                item.children[0].y=0
                item.children[0].x=0
            }
            rowbar.currentIndex=index
        }
    }
    /*returns index of SkTabHead under point Point
      point is in SkTabBar coordinates
      */
    function itemAt(point){
        console.log(point.x,point.y)
        console.log(rowbar.indexAt(point.x,point.y))
        return rowbar.indexAt(point.x,point.y)
    }
    /*
      The Item to scroll tabs on the right
      */
    Image {
        id: leftimg
        source: "../Themes/default/images/stock_right.png"
        rotation: 180
        anchors.left: bar.left
        anchors.top:bar.top
        anchors.bottom: bar.bottom
        anchors.bottomMargin: -Theme.control.button.radius

        width: 15
        fillMode: Image.PreserveAspectFit
        MouseArea{
            id: mouseleft
            anchors.fill: parent
            //acceptedButtons: Qt.LeftButton
            onPressed: { bar.tabdock.currentIndex=Math.max(0,bar.tabdock.currentIndex-1)}
        }
    }
    /*
      The Item where tabs are actually displayedItem
      Listview inherits from Flickable
      */
    ListView {
        id:rowbar
        implicitWidth: bar.width-50
        orientation: Qt.Horizontal
        anchors.left:leftimg.right
        anchors.top:bar.top
        anchors.bottom: bar.bottom
        clip: true
//        Rectangle{
//            objectName: "blueborder"
//            anchors.fill: parent
//            color: "transparent"
//            border.width: 1
//            border.color: "darkgray"
//        }

        layoutDirection: Qt.LeftToRight
        model: bar.__heads
        /* Couldn't find a way to display SkTabHeads directly in the ListView
          So we reparent these SkTabHeads to an Item in the delegate property
          */
        delegate: Rectangle{
            id: wrapper
            objectName: "wrapper"
            property string type: "wrapper"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: -Theme.control.button.radius
            width: Math.min(model.item.width, Theme.control.tab.widthMax)
            height: model.item.height
            gradient: Gradient {
                GradientStop { position: 0.0; color:  "transparent"}
                GradientStop { position: 0.76; color: Theme.control.color.backgroundActive }
            }
            Binding { target: model.item; property: "parent"; value: wrapper }
            Binding { target: model.item; property: "visible"; value: true }
            Binding { target: model.item; property: "tabbar"; value: bar }
        }
    }
    /*
      The Item to scroll tabs on the right
      */
    Image {
        id: rightimg
        source: "../Themes/default/images/stock_right.png"
        anchors.left: rowbar.right
        width: 15
        anchors.top:bar.top
        anchors.bottom: bar.bottom
        fillMode: Image.PreserveAspectFit
        MouseArea{
            id: mouseright
            anchors.fill: parent
            onPressed: { bar.tabdock.currentIndex=Math.min(bar.tabdock.count-1,bar.tabdock.currentIndex+1)}
        }
    }
    /*
      The Item to drag the whole parent SkTabDock
      */
    Image {
        id: movetabimg
        source: "../Themes/default/images/view-fullscreen.png"
        anchors.left: rightimg.right
        anchors.right: bar.right
        anchors.top:bar.top
        anchors.bottom: bar.bottom
        fillMode: Image.PreserveAspectFit
        MouseArea{
            id: mousemovetab
            anchors.fill: parent
            drag.target: tabdock
            drag.axis: Drag.XAndYAxis
            onPressed: {
                tabdock.setPreviousPosition()
                tabdock.unsetAnchors()
            }
            onReleased: {
                if(tabdock.Drag.target && tabdock.Drag.active){ // Hovering a target
                    console.log("Hovering a target")

//                    console.log("tabdock DRAG:", tabbar.tabdock.Drag.target.droppart, tabbar.tabdock.Drag.active)
                    if(tabdock.Drag.target.droppart==="tabbody"){   //  Hovering content area
                        tabdock.dock.moveTabDockToDirection(tabdock, tabdock.Drag.target.dock, tabdock.Drag.target.direction)
                    }
                    else{   //  Hovering anything else, tabdock placed back where it was
                        tabdock.setAnchors()
                        tabdock.placeAtPreviousPosition()
                    }
                }
                else{   //Drag dropped outside current window
                    var tw=tabdock.dock.topwindow
                    var obj=mousemovetab.mapToItem(null,mousemovetab.mouseX, mousemovetab.mouseY)
                    tabdock.Drag.cancel()
                    var retVal=null
                    //  This test is because preference window is a std Window, not an SkWindow
                    if (typeof tw["moveTabOrTabDock"] === "function") {
                        retVal=tw.moveTabOrTabDock(tabdock, obj)
                    }
                    if(retVal===null){
                        tabdock.setAnchors()
                        tabdock.placeAtPreviousPosition()
                    }
                }
            }   //---End of onReleased
        }
    }   //---End of movetabimg


    states: [
        /*
          States to hide/show arrows to present non-visible tabs
          */
        State {
            when: rowbar.currentIndex===0
            PropertyChanges {
                target: leftimg
                opacity:0.1
            }
        },
        State {
            when: rowbar.currentIndex===rowbar.count-1
            PropertyChanges {
                target: rightimg
                opacity:0.1
            }
        }
        //to be continued...
    ]
}
