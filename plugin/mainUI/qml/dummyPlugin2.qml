import QtQuick 2.0
import QtQuick.Controls 1.1

SkTab{
    title: "title 2"
    CheckBox{
        id: check
        checked: true
        text: "une check two"
    }

    Button{
        anchors.top: check.bottom
        anchors.bottom: parent.bottom
        text: "boutton two"
    }
}
