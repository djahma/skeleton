import QtQuick 2.0
import QtQuick.Controls 1.1
import "../Themes"

SkTab{
    title: "La Palette InActive avec un super long titre qui fera plus de 300 de Large je l'espère"

    SystemPalette { id: activeColors; colorGroup: SystemPalette.Inactive }
    Rectangle{
        height:50; width:150
        x:1; y:1
        color:activeColors.window
        border.width: 4;
        border.color: activeColors.windowText;
        Text {
            anchors.fill:parent
            anchors.margins: 4
            wrapMode: Text.WrapAnywhere
            text: qsTr("activeColors.window + activeColors.windowText")
        }
    }
    Rectangle{
        height:50; width:150
        x: 160; y:1
        color:activeColors.base
        border.width: 4;
        border.color: activeColors.alternateBase;
        Text {
            anchors.fill:parent
            anchors.margins: 4
            wrapMode: Text.WrapAnywhere
            text: qsTr("activeColors.Base + activeColors.alternateBase")
        }
    }
    Rectangle{
        height:50; width:150
        x:1; y:53
        color:activeColors.button
        border.width: 4;
        border.color: activeColors.buttonText;
        Text {
            anchors.fill:parent
            anchors.margins: 4
            wrapMode: Text.WrapAnywhere
            text: qsTr("activeColors.button + activeColors.buttonText")
        }
    }
    Rectangle{
        height:50; width:150
        x:160; y:53
        color:activeColors.highlight
        border.width: 4;
        border.color: activeColors.highlightedText;
        Text {
            anchors.fill:parent
            anchors.margins: 4
            wrapMode: Text.WrapAnywhere
            text: qsTr("activeColors.highlight + activeColors.highlightedText")
        }
    }
    Rectangle{
        height:50; width:150
        x:1; y:106
        color:activeColors.mid
        border.width: 4;
        border.color: activeColors.dark;
        Text {
            anchors.fill:parent
            anchors.margins: 4
            wrapMode: Text.WrapAnywhere
            text: qsTr("activeColors.mid + activeColors.dark")
        }
    }
    Rectangle{
        height:50; width:150
        x:160; y:106
        color:activeColors.midlight
        border.width: 4;
        border.color: activeColors.light;
        Text {
            anchors.fill:parent
            anchors.margins: 4
            wrapMode: Text.WrapAnywhere
            text: qsTr("activeColors.midlight + activeColors.light")
        }
    }
    Rectangle{
        height:50; width:150
        x:1; y:159
        color:activeColors.text
        border.width: 4;
        border.color: activeColors.shadow;
        Text {
            anchors.fill:parent
            anchors.margins: 4
            wrapMode: Text.WrapAnywhere
            text: qsTr("activeColors.text + activeColors.shadow")
        }
    }
}
