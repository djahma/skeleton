import QtQuick 2.0
import "../Themes"

Item{
    id:sktab;
    objectName: "tab"
    property string type: "SkTab"
    property string title: "this title";
    property alias head: head;
    property alias body: body;
    property int index
    property var tabdock
    SkTabHead {
        id: head
        title: sktab.title
    }
    Item{
        id: body
        objectName: "tabbody"
        property string type: "SkTabBody"
        anchors.fill: parent
//        width:childrenRect.width
//        height:childrenRect.height
    }

    function setTitle(ttle){
        head.title=ttle;
    }
    function getTitle(){
        return head.title;
    }
    Component.onCompleted: {
        for(var i=0;i<sktab.children.length;i++){
//            var objname=sktab.children[i].objectName
            console.log('child',i, 'found (out of ',sktab.children.length,':',sktab.children[i].objectName)
            if(!sktab.children[i].type || (sktab.children[i].type!=="SkTabHead" && sktab.children[i].type!=="SkTabBody")){
                console.log("reparenting one child: ",sktab.children[i].objectName,sktab.children[i].toString(),"title=",sktab.head.title)
                sktab.children[i].parent=body
                --i
            }
        }
    }
//    onParentChanged: {
//        head.parent=sktab
//    }
}
