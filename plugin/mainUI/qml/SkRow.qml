import QtQuick 2.0

Row{
    id: row
    objectName: "row"
    property string type: "SkRow"
    spacing: 2
    property bool visited: false
    property int minHeight: {
        var maxMinHeight=0
        for(var i=0; i<row.children.length; i++){
            maxMinHeight=Math.max(row.children[i].minHeight,maxMinHeight)
        }
        Math.max(100, maxMinHeight)
    }
    property int minWidth: {
        var totalMinWidth=0
        for(var i=0; i<row.children.length; i++){
            totalMinWidth+=row.children[i].minWidth
        }
        Math.max(100, totalMinWidth)
    }
    //internal: last known size
    property int _height: 300
    property int _width: 200

    // Adds the SkTab given by tabObj to a new SkTabDock and then to this SkRow
    function addTab(tabObj){
//        if(tabObj.objectName!=="tab")return null
        if(!tabObj.type || tabObj.type!=="SkTab")return null
        //we create an SkTabDock to accomodate the tab
        var topDock=row.parent
        //we look for the SkDock to add an SkTabDock
//        while(topDock.objectName!=="dock") topDock=topDock.parent
        while(topDock.type && topDock.type!=="SkDock") topDock=topDock.parent
        var comp=Qt.createComponent("SkTabDock.qml")
        var td=comp.createObject(topDock)
        td.addTab(tabObj) //we add the tab to the newly created tabdock
        topDock.addTabDock(td) //  Adding our new SkTabDock to top SkDock
        //finally adding SkTabDock to this row
        row.addTabDock(td)
        return tabObj
    }

    // Adds the SkTabDock to this SkRow as specified by tabdockObj
    function addTabDock(tabdockObj){
        console.log("SkRow.addTabDock")
//        if(tabdockObj.objectName==="tabdock"){
        if(tabdockObj.type && tabdockObj.type==="SkTabDock"){
            row.unsetAnchors(tabdockObj)
            tabdockObj.parent= row
            row.setAnchors(tabdockObj)
            row.childrenFitRowWidth()
        }
    }
    // Adds the SkColumn to this SkRow. If colObj is null, a new empty SkColumn is created.
    function addColumn(colObj){
        //First create and parent SkRow to this column
        if(colObj===null) colObj=row.createColumn()
//        else if(colObj.objectName==="col") colObj.parent=row
        else if(colObj.type && colObj.type==="SkColumn") colObj.parent=row
        else return null
        row.setAnchors(colObj)  // Then set its anchors
        row.childrenFitRowWidth()
        return colObj
    }

    /*
      Inserts a SkTab or a SkTabDock or a SkColumn to this SkRow, at the correct position index
      if passed an SkTab parameter, creates a new SkTabDock for it first
      */
    function insertAt( tabORtabdockORcol, position){
        //  check that position is in appropriate range or return immediately
        if(position<0 || position>row.children.length) return null
        //  If given an SkTab we create an SkTabDock for it first
//        if(tabORtabdockORcol.objectName==="tab"){
        if(tabORtabdockORcol.type && tabORtabdockORcol.type==="SkTab"){
            //  we want to handle an SkTabDock in the rest of the function, and not a tab
            if(row.addTab(tabORtabdockORcol)!==null)tabORtabdockORcol=tabORtabdockORcol.tabdock
            else return null
        }
        //  We empty the row by inserting all its children into a convenient Rectangle{}
        var newObj=Qt.createQmlObject('import QtQuick 2.0; Rectangle {}', row)
        var index=0
//        while(row.children[index].objectName==="col" || row.children[index].objectName==="tabdock" || row.children[index].objectName==="dock1" || row.children[index].objectName==="dock2"){
        while(row.children[index].type && (row.children[index].type==="SkColumn" ||
                                           row.children[index].type==="SkTabDock")){
//            console.log("SkRow.insertAt(",tabORtabdockORcol.objectName,position, ")", tabORtabdockORcol.height,tabORtabdockORcol.width)
            if(row.children[index]===tabORtabdockORcol)index++  //index++ because we want tabORtabdockORcol to be the last added element in newObj
            else row.children[index].parent=newObj
        }
        tabORtabdockORcol.parent=newObj
        //  Now we can reinsert the children in the appropriate order
        var i=0, j=0
        while(newObj.children.length>0){
            if(i===position) {
//                var objName=newObj.children[newObj.children.length-1].objectName
                if(!newObj.children[newObj.children.length-1].type) break
//                var objName=newObj.children[newObj.children.length-1].objectName
                var objType=newObj.children[newObj.children.length-1].type
//                if(objName==="col")row.addColumn(newObj.children[newObj.children.length-1])
                if(objType==="SkColumn")row.addColumn(newObj.children[newObj.children.length-1])
                else row.addTabDock(newObj.children[newObj.children.length-1])
            }
            else {
//                if(newObj.children[0].objectName==="col") row.addColumn(newObj.children[0])
                if(newObj.children[0].type && newObj.children[0].type==="SkColumn") row.addColumn(newObj.children[0])
                else row.addTabDock(newObj.children[0])
            }
            i++
        }
        newObj.parent=null
        return tabORtabdockORcol
    }

    //Returns a SkRow parented to this column
    function createColumn(){
        var comp=Qt.createComponent("SkColumn.qml")
        var newCol=comp.createObject(row)
        row.setAnchors(newCol)
        return newCol
    }

    /*
      Finds child position within this row
      or returns -1 in case of error
      */
    function childPosition(child){
        for(var i=0; i<row.children.length; i++){
            if(child===row.children[i]) return i
        }
        return -1
    }

    //  Resize functions to alter child height and width, by altering next child height and width
    function resizeWidth(child, deltaWidth){
//        if(row.parent.objectName==="row"){
        if(row.parent.type && row.parent.type==="SkRow"){
            var position=row.parent.childPosition(row)
            if(position<row.parent.children.length-1){
                var diffFront = row.parent.children[position+1].minWidth - (row.parent.children[position+1].width-deltaWidth)
                if(diffFront>0)deltaWidth-=diffFront
                var diffBack = row.minWidth-(row.width+deltaWidth)
                if(diffBack>0)deltaWidth+=diffBack
                row.parent.children[position+1].width-=deltaWidth
                child.width+=deltaWidth
            }
            else {
                row.parent.resizeWidth(row, deltaWidth)
            }
        }
//        else if(row.parent.objectName==="col"){
        else if(row.parent.type && row.parent.type==="SkColumn"){
            row.parent.resizeWidth(row, deltaWidth)
        }
    }

    function resizeHeight(child, deltaHeight){
//        if(row.parent.objectName==="row"){
        if(row.parent.type && row.parent.type==="SkRow"){
            row.parent.resizeHeight(row, deltaHeight)
        }
//        else if(row.parent.objectName==="col"){
        else if(row.parent.type && row.parent.type==="SkColumn"){
            var position=row.parent.childPosition(row)
            if(position<row.parent.children.length-1){
                var diffDown = row.parent.children[position+1].minHeight - (row.parent.children[position+1].height-deltaHeight)
                if(diffDown>0)deltaHeight-=diffDown
                var diffUp = row.minHeight-(row.height+deltaHeight)
                if(diffUp>0)deltaHeight+=diffUp
                row.parent.children[position+1].height-=deltaHeight
                row.height+=deltaHeight
            }
            else {
                row.parent.resizeHeight(row, deltaHeight)
            }
        }
    }

    /*
      Set/Unset anchors on row.children
      obj parameter must be a children of this SkRow
      */
    function setAnchors(obj){
        var isFromThisCol=false
        for(var index=0; index<row.children.length; index++){
            if(row.children[index]===obj)isFromThisCol=true
        }
        if(!isFromThisCol) return
        obj.anchors.top=row.top
        obj.anchors.bottom=row.bottom
        obj.anchors.left=undefined
        obj.anchors.right=undefined
        row.height=Math.max(row.height,obj._height)
    }
    function setAllAnchors(){
        for(var index=0; index<row.children.length; index++){
            row.setAnchors(row.children[index])
        }
    }
    function unsetAnchors(obj){
        obj._width=obj.width
        obj._height=obj.height
        obj.anchors.top=undefined
        obj.anchors.bottom=undefined
        obj.anchors.left=undefined
        obj.anchors.right=undefined
        obj.anchors.fill=undefined
    }
    function unsetAllAnchors(){
        for(var index=0; index<row.children.length; index++){
            row.unsetAnchors(row.children[index])
        }
    }
    //  Resize row's children to make their total width fit row's width
    function childrenFitRowWidth(){
        if(row.children.length===0)return
        var combinedWidth=0
        for(var i=0; i<row.children.length;i++){
            combinedWidth+=row.children[i].width
        }
        var diff=0
        for(i=0; i<row.children.length;i++){
            row.children[i].width=(row.children[i].width/combinedWidth)*row.width
            if(row.children[i].width - row.children[i].minWidth <0)diff+=row.children[i].width - row.children[i].minWidth
            row.children[i].width=Math.max(row.children[i].width,row.children[i].minWidth)
        }
        //  Spread unallocated width to other children
        if(diff<0){
            var diffBack=0
            for(i=row.children.length-1; i>=0; i--){
                if(diff>=0)continue
                diffBack=row.children[i].width - row.children[i].minWidth
                if(diffBack>0){
                    row.children[i].width -= Math.min(diffBack, -diff)
                    diff+=Math.min(diffBack, -diff)
                }
            }
        }
        //  update internal children size
        for(i=0; i<row.children.length; i++){
//            if(row.children[i].objectName!=="col" && row.children[i].objectName!=="tabdock")continue
            if(!row.children[i].type || (row.children[i].type!=="SkColumn" && row.children[i].type!=="SkTabDock"))continue
            row.children[i]._width=row.children[i].width
            row.children[i]._height=row.children[i].height
//            if(row.children[i].objectName==="col")row.children[i].childrenFitColHeight()
            if(row.children[i].type && row.children[i].type==="SkColumn")row.children[i].childrenFitColHeight()
        }
    }

    onChildrenChanged: {
        row.childrenFitRowWidth()
    }

    onWidthChanged: {
        row.childrenFitRowWidth()
    }
}
