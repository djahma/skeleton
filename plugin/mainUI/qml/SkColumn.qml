import QtQuick 2.0

Column{
    id: col
    objectName: "col"
    property string type: "SkColumn"
    spacing: 2
    property bool visited : false
    property int minHeight: {
        var totalMinHeight=0
        for(var i=0; i<col.children.length; i++){
            totalMinHeight+=col.children[i].minHeight
        }
        Math.max(100, totalMinHeight)
    }
    property int minWidth: {
        var maxMinWidth=0
        for(var i=0; i<col.children.length; i++){
            maxMinWidth=Math.max(col.children[i].minWidth,maxMinWidth)
        }
        Math.max(100, maxMinWidth)
    }
    //internal: last known size
    property int _height: 300
    property int _width: 200

    // Adds the SkTab given by tabObj to a new SkTabDock and then to this SkColumn
    function addTab(tabObj){
        if(tabObj.type && tabObj.type!=="SkTab")return null
//        if(tabObj.objectName!=="tab")return null
        //we create an SkTabDock to accomodate the tab
        var topDock=col.parent
        //we look for the SkDock to add an SkTabDock
        while(topDock.type!=="SkDock") topDock=topDock.parent
//        while(topDock.objectName!=="dock") topDock=topDock.parent
        var comp=Qt.createComponent("SkTabDock.qml")
        var td=comp.createObject(topDock)
        td.addTab(tabObj) //we add the tab to the newly created tabdock
        topDock.addTabDock(td) //  Adding our new SkTabDock to top SkDock
        //finally adding SkTabDock to this column
        col.addTabDock(td)
        return tabObj
    }

    // Adds the SkTabDock to this SkCol as specified by tabdockObj
    function addTabDock(tabdockObj){
        console.log("SkColumn.addTabDock")
        if(tabdockObj.type && tabdockObj.type==="SkTabDock"){
//        if(tabdockObj.objectName==="tabdock"){
            col.unsetAnchors(tabdockObj)
            tabdockObj.parent= col
            col.setAnchors(tabdockObj)
            col.childrenFitColHeight()
        }
    }
    // Adds the SkRow to this SkCol. If rowObj is null, a new empty SkRow is created.
    function addRow(rowObj){
        //First create and parent SkRow to this column
        if(rowObj===null) rowObj=col.createRow()
        else if(rowObj.type==="SkRow") rowObj.parent=col
//        else if(rowObj.objectName==="row") rowObj.parent=col
        else return null
        col.setAnchors(rowObj)  // Then set its anchors
        col.childrenFitColHeight()
        return rowObj
    }

    /*
      Inserts a SkTab or a SkTabDock or a SkRow to this SkColumn, at the correct position index
      if passed an SkTab parameter, creates a new SkTabDock for it first
      */
    function insertAt(tabORtabdockORrow, position){
        //  check that position is in appropriate range or return immediately
        if(position<0 || position>col.children.length+1) return null
        //  If given an SkTab we create an SkTabDock for it first
        if(tabORtabdockORrow.type && tabORtabdockORrow.type ==="SkTab"){
//        if(tabORtabdockORrow.objectName==="tab"){
            //  we want to handle an SkTabDock in the rest of the function, and not a tab
            if(col.addTab(tabORtabdockORrow)!==null)tabORtabdockORrow=tabORtabdockORrow.tabdock
            else return null
        }
        //  We empty the col by inserting all its children into a convenient Rectangle{}
        var newObj=Qt.createQmlObject('import QtQuick 2.0; Rectangle {}', col)
        var index=0
        while(col.children[index].type && (col.children[index].type==="SkRow" ||
                                           col.children[index].type==="SkTabDock")){
//        while(col.children[index].type && (col.children[index].objectName==="row" ||
//                                           col.children[index].objectName==="tabdock" ||
//                                           col.children[index].objectName==="dock1" ||
//                                           col.children[index].objectName==="dock2")){
//            console.log("SkColumn.insertAt(",tabORtabdockORrow.objectName,position, ")", tabORtabdockORrow.height,tabORtabdockORrow.width)
            if(col.children[index]===tabORtabdockORrow)index++  //index++ because we skip the item we are inserting at this stage, to be sure it is the last element added to newObj
            else col.children[index].parent=newObj
        }
        tabORtabdockORrow.parent=newObj
        //  Now we can reinsert the children in the appropriate order
        var i=0, j=0
        while(newObj.children.length>0){
            if(i===position) {
                if(!newObj.children[newObj.children.length-1].type)break
                var objType=newObj.children[newObj.children.length-1].type
//                var objName=newObj.children[newObj.children.length-1].objectName
                if(objType==="SkRow")col.addRow(newObj.children[newObj.children.length-1])
//                if(objName==="row")col.addRow(newObj.children[newObj.children.length-1])
                else col.addTabDock(newObj.children[newObj.children.length-1])
            }
            else {
                if(!newObj.children[0].type)break
//                if(newObj.children[0].objectName==="row") col.addRow(newObj.children[0])
                if(newObj.children[0].type==="SkRow") col.addRow(newObj.children[0])
                else col.addTabDock(newObj.children[0])
            }
            i++
        }
//        newObj.destroy()
        newObj.parent=null
        return tabORtabdockORrow
    }

    //Returns a SkRow parented to this column
    function createRow(){
        var comp=Qt.createComponent("SkRow.qml")
        var newRow=comp.createObject(col)
        col.setAnchors(newRow)
        return newRow
    }

    /*
      Finds child position within this column
      or returns -1 in case of error
      */
    function childPosition(child){
        for(var i=0; i<col.children.length; i++){
            if(child===col.children[i]) return i
        }
        return -1
    }

    //  Resize functions to alter child height and width, by altering next child height and width
    function resizeWidth(child, deltaWidth){
//        if(col.parent.objectName==="row"){
        if(col.parent.type && col.parent.type==="SkRow"){
            var position=col.parent.childPosition(col)
            if(position<col.parent.children.length-1){
                var diffFront = col.parent.children[position+1].minWidth - (col.parent.children[position+1].width-deltaWidth)
                if(diffFront>0)deltaWidth-=diffFront
                var diffBack = col.minWidth-(col.width+deltaWidth)
                if(diffBack>0)deltaWidth+=diffBack
                col.parent.children[position+1].width-=deltaWidth
                col.width+=deltaWidth
            }
            else {
                col.parent.resizeWidth(col, deltaWidth)
            }
        }
//        else if(col.parent.objectName==="col"){
        else if(col.parent.type && col.parent.type==="SkColumn"){
            col.parent.resizeWidth(col, deltaWidth)
        }
    }

    function resizeHeight(child, deltaHeight){
//        if(col.parent.objectName==="row"){
        if(col.parent.type && col.parent.type==="SkRow"){
            col.parent.resizeHeight(col, deltaHeight)
        }
//        else if(col.parent.objectName==="col"){
        else if(col.parent.type && col.parent.type==="SkColumn"){
            var position=col.parent.childPosition(col)
            if(position<col.parent.children.length-1){
                var diffDown = col.parent.children[position+1].minHeight - (col.parent.children[position+1].height-deltaHeight)
                if(diffDown>0)deltaHeight-=diffDown
                var diffUp = col.minHeight-(col.height+deltaHeight)
                if(diffUp>0)deltaHeight+=diffUp
                col.parent.children[position+1].height-=deltaHeight
                col.height+=deltaHeight
            }
            else {
                col.parent.resizeHeight(col, deltaHeight)
            }
        }
    }

    /*
      Set/Unset anchors on col.children
      obj parameter must be a children of this SkColumn
      */
    function setAnchors(obj){
        var isFromThisCol=false
        for(var index=0; index<col.children.length; index++){
            if(col.children[index]===obj)isFromThisCol=true
        }
        if(!isFromThisCol) return
        obj.anchors.top=undefined
        obj.anchors.bottom=undefined
        obj.anchors.left=col.left
        obj.anchors.right=col.right
        col.width=Math.max(col.width,obj._width)
    }
    function setAllAnchors(){
        for(var index=0; index<col.children.length; index++){
            col.setAnchors(col.children[index])
        }
    }
    function unsetAnchors(obj){
        obj._width=obj.width
        obj._height=obj.height
        obj.anchors.top=undefined
        obj.anchors.bottom=undefined
        obj.anchors.left=undefined
        obj.anchors.right=undefined
        obj.anchors.fill=undefined
    }
    function unsetAllAnchors(){
        for(var index=0; index<col.children.length; index++){
            col.unsetAnchors(col.children[index])
        }
    }
    //  Resize col's children to make their total height fit col's height
    function childrenFitColHeight(){
        console.log("SkColumn.childrenFitColHeight:", col.children.length,"children")
        if(col.children.length===0)return
        var combinedHeight=0
        for(var i=0; i<col.children.length;i++){
            combinedHeight+=col.children[i].height
        }
        var diff=0
        for(i=0; i<col.children.length;i++){
            col.children[i].height=(col.children[i].height/combinedHeight)*col.height
            if(col.children[i].height - col.children[i].minHeight <0) diff+=col.children[i].height - col.children[i].minHeight
            col.children[i].height=Math.max(col.children[i].height, col.children[i].minHeight)
        }
        if(diff<0){
            var diffBack=0
            for(i=col.children.length-1; i>=0; i--){
                if(diff>=0) continue
                diffBack=col.children[i].height - col.children[i].minHeight
                if(diffBack>0){
                    col.children[i].height -=Math.min(diffBack,-diff)
                    diff+=Math.min(diffBack,-diff)
                }
            }
        }
        //  update internal children size
        for(i=0; i<col.children.length;i++){
//            if(col.children[i].objectName!=="row" && col.children[i].objectName!=="tabdock")continue
            if(!col.children[i].type || (col.children[i].type!=="SkRow" && col.children[i].type!=="SkTabDock"))continue
            col.children[i]._width=col.children[i].width
            col.children[i]._height=col.children[i].height
            if(col.children[i].type && col.children[i].type==="SkRow")col.children[i].childrenFitRowWidth()
//            if(col.children[i].objectName==="row")col.children[i].childrenFitRowWidth()
        }
    }

    onChildrenChanged: {
        console.log("SkColumn.onChildrenChanged: #", col.children.length)
        if(col.children.length===0)return
        col.childrenFitColHeight()
    }

    onHeightChanged: {
        console.log("SkCol.onHeightChanged",col.children.length)
        col.childrenFitColHeight()
    }
}
