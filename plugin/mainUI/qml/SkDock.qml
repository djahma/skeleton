import QtQuick 2.0

Item{
    id: rootdock
    anchors.fill: parent
    anchors.margins: 2
    objectName: "dock"
    property string type: "SkDock"
    property ListModel __docks: ListModel{}
    //the window containing this dock
    // Bottom-Up hierarchy is: tabhead.tabbar.tabdock.DOCK.topwindow
    property var topwindow

    /*  Adds a SkTab to current SkDock within first encountered SkTabDock
      Creates a SkTabDock for it if none exists
      */
    function addTab(tab){
        console.log("SkDock.addTab")
//        if(tab.objectName!=="tab"){
        if(tab.type && tab.type!=="SkTab"){
            console.log("Err: SkDock.addTab(tab): tab is not a SkTab, aborting.")
            return
        }
        //  Looking for an existing tabdock to parent our tab to
        var firstColRow=rootdock.children[0]
        var foundTabDock=null
//        while(firstColRow.objectName==="col" || firstColRow.objectName==="row"){
        while(firstColRow.type && (firstColRow.type==="SkColumn" || firstColRow.type==="SkRow")){
            for(var i=0; i<firstColRow.children.length; i++){
//                if(firstColRow.children[i].objectName==="tabdock"){
                if(firstColRow.children[i].type && firstColRow.children[i].type==="SkTabDock"){
                    foundTabDock=firstColRow.children[i]
                    break
                }
            }
            if(foundTabDock!==null)break
            if(firstColRow.children.length===0){
                //  No more children
                break
            }
            else firstColRow=firstColRow.children[0]
        }
        //  reparent the tab or create a new tabdock
        if(foundTabDock!==null)foundTabDock.addTab(tab)
        else{
            var comp=Qt.createComponent("SkTabDock.qml")
            var tabdock=comp.createObject(rootdock)
            tabdock.addTab(tab)
            rootdock.addTabDock(tabdock)
        }
        topwindow.title=tab.head.title
    }

    /*  Adds a SkTabDock to current SkDock within first encountered container
      Creates a SkRow/SkColumn for it if none exists
      */
    function addTabDock(td){
        console.log("SkDock.addTabDock")
//        if(td.objectName==="tabdock"){
        if(td.type && td.type==="SkTabDock"){
            registerTabDock(td)
        }
        else {
            console.log("Err: SkDock.addTabDock(td): td is not a tabdock, aborting.")
            return null
        }
        var firstColRow=rootdock.children[0]
        console.log("SkDock.addTabDock: rootdock.children[0]=",rootdock.children[0].type, rootdock.children[0].x, td.x)

        var secondObj=rootdock.children[0]
        //  Dig in first Row/Col tree down to last one
//        while(firstColRow.objectName==="col" || firstColRow.objectName==="row"){
        while(firstColRow.type && (firstColRow.type==="SkColumn" || firstColRow.type==="SkRow")){
            secondObj=firstColRow
            if(firstColRow.children.length===0){
                //  No more children
                break
            }
            else firstColRow=firstColRow.children[0]
        }
        secondObj.addTabDock(td)
        if(td.count>0){
            topwindow.title=td.__tabs.get(td.currentIndex).tab.head.title
        }
        return td
    }

    //  Remove SkTabDock td from __docks registry and subcontainer
    function removeTabDock(td){
        for(var i=0; i<__docks.count; i++){
            if(__docks.get(i).tabdock===td){
                __docks.remove(i)
                td.parent=null
                break
            }
        }
    }

    /*  Moves movedTD SkTabDock and place it at direction of trgtTD SkTabDock
        Similar to moveTabToDirection function, but with SkTabDock object
        */
    function moveTabDockToDirection(movedTD, trgtTD, direction){
        console.log("SkDock.moveTabDockToDirection(",movedTD.type,trgtTD.type,direction,")")
        var targetColRow=trgtTD.parent   //target tabdock container
        var pos=targetColRow.childPosition(trgtTD) //current position of target tabdock
        var newColRow, newCol, newRow
        var tabsArr=movedTD.getTabsArray()
        movedTD.dock=trgtTD.dock
        rootdock.registerTabDock(movedTD)
        switch(direction){
        case "in":
            movedTD.removeTabs(tabsArr)
            trgtTD.addTabs(tabsArr)
            removeTabDock(movedTD)
            break;
        case "left":
            console.log("Moving TabDock left, poistion=", pos)
            moveTabLeft(trgtTD, movedTD, pos)
            break;
        case "right":
            console.log("Moving TabDock right, position=", pos)
            moveTabRight(trgtTD, movedTD, pos)
            break;
        case "up":
            console.log("Moving TabDock up")
            moveTabUp(trgtTD, movedTD, pos)
            break;
        case "down":
            console.log("Moving TabDock down")
            moveTabDown(trgtTD, movedTD, pos)
            break;
        case "leftmost":
            console.log("Moving TabDock leftmost")
//            if(targetColRow.parent.objectName==="row"){
            if(targetColRow.parent.type && targetColRow.parent.type==="SkRow"){
                moveTabLeft(targetColRow, movedTD, targetColRow.parent.childPosition(targetColRow))
            }
//            else if(targetColRow.parent.objectName==="col"){
            else if(targetColRow.parent.type && targetColRow.parent.type==="SkColumn"){
//                if(targetColRow.parent.parent.objectName==="row"){
                if(targetColRow.parent.parent.type &&
                        targetColRow.parent.parent.type==="SkRow"){
                    moveTabLeft(targetColRow.parent, movedTD, targetColRow.parent.parent.childPosition(targetColRow.parent))
                }
//                else if(targetColRow.parent.parent.objectName==="dock"){
                else if(targetColRow.parent.parent.type &&
                        targetColRow.parent.parent.type==="SkDock"){
                    newRow=rootdock.addRow()
                    moveTabLeft(targetColRow.parent, movedTD, 0)
                }
            }
//            else if(targetColRow.parent.objectName==="dock"){
            else if(targetColRow.parent.type &&
                    targetColRow.parent.type==="SkDock"){
//                if(targetColRow.objectName==="row") moveTabLeft(trgtTD, movedTD,0)
                if(targetColRow.type && targetColRow.type==="SkRow") moveTabLeft(trgtTD, movedTD,0)
                else{
                    newRow=rootdock.addRow()
                    moveTabLeft(targetColRow, movedTD,0)
                }
            }
            break;
        case "rightmost":
            console.log("Moving TabDock rightmost")
//            if(targetColRow.parent.objectName==="row"){
            if(targetColRow.parent.type && targetColRow.parent.type==="SkRow"){
                moveTabRight(targetColRow, movedTD, targetColRow.parent.childPosition(targetColRow))
            }
//            else if(targetColRow.parent.objectName==="col"){
            else if(targetColRow.parent.type && targetColRow.parent.type==="SkColumn"){
                if(targetColRow.parent.parent.type && targetColRow.parent.parent.type==="SkRow"){
                    moveTabRight(targetColRow.parent, movedTD, targetColRow.parent.parent.childPosition(targetColRow.parent))
                }
//                else if(targetColRow.parent.parent.objectName==="dock"){
                else if(targetColRow.parent.parent.type &&
                        targetColRow.parent.parent.type==="SkDock"){
                    newRow=rootdock.addRow()
                    moveTabRight(targetColRow.parent, movedTD, 0)
                }
            }
//            else if(targetColRow.parent.objectName==="dock"){
            else if(targetColRow.parent.type && targetColRow.parent.type==="SkDock"){
                if(targetColRow.type && targetColRow.type==="SkRow") moveTabRight(trgtTD, movedTD,targetColRow.children.length-1)
                else{
                    newRow=rootdock.addRow()
                    moveTabRight(targetColRow, movedTD,0)
                }
            }
            break;
        case "upmost":
            console.log("Moving TabDock upmost")
//            if(targetColRow.parent.objectName==="col"){
            if(targetColRow.parent.type && targetColRow.parent.type==="SkColumn"){
                moveTabUp(targetColRow, movedTD, targetColRow.parent.childPosition(targetColRow))
            }
//            else if(targetColRow.parent.objectName==="row"){
            else if(targetColRow.parent.type && targetColRow.parent.type==="SkRow"){
//                if(targetColRow.parent.parent.objectName==="col"){
                if(targetColRow.parent.parent.type && targetColRow.parent.parent.type==="SkColumn"){
                    moveTabUp(targetColRow.parent, movedTD, targetColRow.parent.parent.childPosition(targetColRow.parent))
                }
//                else if(targetColRow.parent.parent.objectName==="dock"){
                else if(targetColRow.parent.parent.type && targetColRow.parent.parent.type==="SkDock"){
                    newCol=rootdock.addColumn()
                    moveTabUp(targetColRow.parent, movedTD, 0)
                }
            }
//            else if(targetColRow.parent.objectName==="dock"){
            else if(targetColRow.parent.type &&
                    targetColRow.parent.type==="SkDock"){
//                if(targetColRow.objectName==="col") moveTabUp(trgtTD, movedTD, 0)
                if(targetColRow.type && targetColRow.type==="SkColumn") moveTabUp(trgtTD, movedTD, 0)
                else{
                    newCol=rootdock.addColumn()
                    moveTabUp(targetColRow, movedTD,0)
                }
            }
            break;
        case "downmost":
            console.log("Moving TabDock downmost")
//            if(targetColRow.parent.objectName==="col"){
            if(targetColRow.parent.type && targetColRow.parent.type==="SkColumn"){
                moveTabDown(targetColRow, movedTD, targetColRow.parent.childPosition(targetColRow))
            }
//            else if(targetColRow.parent.objectName==="row"){
            else if(targetColRow.parent.type && targetColRow.parent.type==="SkRow"){
//                if(targetColRow.parent.parent.objectName==="col"){
                if(targetColRow.parent.parent.type &&
                        targetColRow.parent.parent.type==="SkColumn"){
                    moveTabDown(targetColRow.parent, movedTD, targetColRow.parent.parent.childPosition(targetColRow.parent))
                }
//                else if(targetColRow.parent.parent.objectName==="dock"){
                else if(targetColRow.parent.parent.type && targetColRow.parent.parent.type==="SkDock"){
                    newCol=rootdock.addColumn()
                    moveTabDown(targetColRow.parent, movedTD, 0)
                }
            }
//            else if(targetColRow.parent.objectName==="dock"){
            else if(targetColRow.parent.type && targetColRow.parent.type==="SkDock"){
//                if(targetColRow.objectName==="col") moveTabDown(trgtTD, movedTD, targetColRow.children.length-1)
                if(targetColRow.type && targetColRow.type==="SkColumn") moveTabDown(trgtTD, movedTD, targetColRow.children.length-1)
                else{
                    newCol=rootdock.addColumn()
                    moveTabDown(targetColRow, movedTD,0)
                }
            }
            break;
        }

    }

    /*
      Removes tab from soure tabdock at tabIndex and parent it to target tabdock
      creating rows and columns as required by the direction
      */
    function moveTabToDirection(movedTab, trgtTabDock, direction){
        var srcTabDock=movedTab.tabdock  //the tab being moved around
        var targetColRow=trgtTabDock.parent   //target tabdock container
        var pos=targetColRow.childPosition(trgtTabDock) //current position of target tabdock
        if(pos===-1){
            console.log("Error: cannot find position of SkTabDock within SkRow/SkColumn")
            return
        }
        var newColRow, newCol, newRow
        //Removes the tab from the source tabdock
        if(srcTabDock===trgtTabDock && direction==="in")return
        srcTabDock.removeTab(movedTab.index)
        //parent the removed tab to the correct row/column/tabdock
        switch(direction){
        case "in":
            trgtTabDock.addTab(movedTab)
            break;
        case "left":
            console.log("Moving Tab left")
            moveTabLeft(trgtTabDock, movedTab, pos)
            break;
        case "right":
            console.log("Moving Tab right")
            moveTabRight(trgtTabDock, movedTab, pos)
            break;
        case "up":
            console.log("Moving Tab up")
            moveTabUp(trgtTabDock, movedTab, pos)
            break;
        case "down":
            console.log("Moving Tab down")
            moveTabDown(trgtTabDock, movedTab, pos)
            break;
        case "upmost":
            console.log("Moving Tab  upmost")
//            if(targetColRow.parent.objectName==="row"){
            if(targetColRow.parent.type && targetColRow.parent.type==="SkRow"){
//                if(targetColRow.parent.parent.objectName==="col"){
                if(targetColRow.parent.parent.type &&
                        targetColRow.parent.parent.type==="SkColumn"){
                    moveTabUp(targetColRow.parent, movedTab, targetColRow.parent.parent.childPosition(targetColRow.parent))
                }
//                else if(targetColRow.parent.parent.objectName==="dock"){
                else if(targetColRow.parent.parent.type &&
                        targetColRow.parent.parent.type==="SkDock"){
                    newCol=rootdock.addColumn()
                    moveTabUp(targetColRow.parent, movedTab, 0)
                }
            }
//            else if(targetColRow.parent.objectName==="col"){
            else if(targetColRow.parent.type && targetColRow.parent.type==="SkColumn"){
                moveTabUp(targetColRow, movedTab, targetColRow.parent.childPosition(targetColRow))
            }
//            else if(targetColRow.parent.objectName==="dock"){
            else if(targetColRow.parent.type && targetColRow.parent.type==="SkDock"){
                newCol=rootdock.addColumn()
                moveTabUp(targetColRow, movedTab,0)
            }
            break;
        case "downmost":
            console.log("Moving Tab  downmost")
//            if(targetColRow.parent.objectName==="row"){
            if(targetColRow.parent.type && targetColRow.parent.type==="SkRow"){
//                if(targetColRow.parent.parent.objectName==="col"){
                if(targetColRow.parent.parent.type && targetColRow.parent.parent.type==="SkColumn"){
                    moveTabDown(targetColRow.parent, movedTab, targetColRow.parent.parent.childPosition(targetColRow.parent))
                }
//                else if(targetColRow.parent.parent.objectName==="dock"){
                else if(targetColRow.parent.parent.type && targetColRow.parent.parent.type==="SkDock"){
                    newCol=rootdock.addColumn()
                    moveTabDown(targetColRow.parent, movedTab, 0)
                }
            }
//            else if(targetColRow.parent.objectName==="col"){
            else if(targetColRow.parent.type && targetColRow.parent.type==="SkColumn"){
                moveTabDown(targetColRow, movedTab, targetColRow.parent.childPosition(targetColRow))
            }
//            else if(targetColRow.parent.objectName==="dock"){
            else if(targetColRow.parent.type && targetColRow.parent.type==="SkDock"){
                newCol=rootdock.addColumn()
                moveTabDown(targetColRow, movedTab,0)
            }
            break;
        case "leftmost":
            console.log("Moving Tab  leftmost")
//            if(targetColRow.parent.objectName==="row"){
            if(targetColRow.parent.type && targetColRow.parent.type==="SkRow"){
                moveTabLeft(targetColRow, movedTab, targetColRow.parent.childPosition(targetColRow))
            }
//            else if(targetColRow.parent.objectName==="col"){
            else if(targetColRow.parent.type && targetColRow.parent.type==="SkColumn"){
//                if(targetColRow.parent.parent.objectName==="row"){
                if(targetColRow.parent.parent.type && targetColRow.parent.parent.type==="SkRow"){
                    moveTabLeft(targetColRow.parent, movedTab, targetColRow.parent.parent.childPosition(targetColRow.parent))
                }
//                else if(targetColRow.parent.parent.objectName==="dock"){
                else if(targetColRow.parent.parent.type && targetColRow.parent.parent.type==="SkDock"){
                    newRow=rootdock.addRow()
                    moveTabLeft(targetColRow.parent, movedTab, 0)
                }
            }
//            else if(targetColRow.parent.objectName==="dock"){
            else if(targetColRow.parent.type && targetColRow.parent.type==="SkDock"){
//                if(targetColRow.objectName==="row") moveTabLeft(trgtTabDock, movedTab,0)
                if(targetColRow.type && targetColRow.type==="SkRow") moveTabLeft(trgtTabDock, movedTab,0)
                else{
                    newRow=rootdock.addRow()
                    moveTabLeft(targetColRow, movedTab,0)
                }
            }
            break;
        case "rightmost":
            console.log("Moving Tab  rightmost")
//            if(targetColRow.parent.objectName==="row"){
            if(targetColRow.parent.type && targetColRow.parent.type==="SkRow"){
                moveTabRight(targetColRow, movedTab, targetColRow.parent.childPosition(targetColRow))
            }
//            else if(targetColRow.parent.objectName==="col"){
            else if(targetColRow.parent.type && targetColRow.parent.type==="SkColumn"){
//                if(targetColRow.parent.parent.objectName==="row"){
                if(targetColRow.parent.parent.type && targetColRow.parent.parent.type==="SkRow"){
                    moveTabRight(targetColRow.parent, movedTab, targetColRow.parent.parent.childPosition(targetColRow.parent))
                }
//                else if(targetColRow.parent.parent.objectName==="dock"){
                else if(targetColRow.parent.parent.type && targetColRow.parent.parent.type==="SkDock"){
                    newRow=rootdock.addRow()
                    moveTabRight(targetColRow.parent, movedTab, 0)
                }
            }
//            else if(targetColRow.parent.objectName==="dock"){
            else if(targetColRow.parent.type && targetColRow.parent.type==="SkDock"){
//                if(targetColRow.objectName==="row") moveTabRight(trgtTabDock, movedTab,targetColRow.children.length-1)
                if(targetColRow.type && targetColRow.type==="SkRow") moveTabRight(trgtTabDock, movedTab,targetColRow.children.length-1)
                else{
                    newRow=rootdock.addRow()
                    moveTabRight(targetColRow, movedTab,0)
                }
            }
            break;
        default:
            srcTabDock.addTab(movedTab) //reparenting tab where it comes from
            break;
        }
        if(srcTabDock.__tabs.count===0)rootdock.removeTabDock(srcTabDock)
        rootdock.cleanUselessColRows(rootdock.children[0])
        rootdock.cleanEmptyColRows(rootdock.children[0])
//        if(rootdock.children[0].objectName==="row")rootdock.children[0].childrenFitRowWidth()
//        else if(rootdock.children[0].objectName==="col")rootdock.children[0].childrenFitColHeight()
        if(rootdock.children[0].type && rootdock.children[0].type==="SkRow")rootdock.children[0].childrenFitRowWidth()
        else if(rootdock.children[0].type && rootdock.children[0].type==="SkColumn")rootdock.children[0].childrenFitColHeight()
    }

    /*
      Functions to move SkTab, SkTabDock, SkRow, SkColumn
      given the targetTabDock SkBody overlay
      */
    function moveTabLeft(trgtTabDock, movedTab, position){
        var targetColRow=trgtTabDock.parent
        var newColRow
//        if(targetColRow.objectName==="row"){
        if(targetColRow.type && targetColRow.type==="SkRow"){
//            if(movedTab.objectName==="tabdock" ){
            if(movedTab.type && movedTab.type==="SkTabDock" ){
                var pos2=targetColRow.childPosition(movedTab)
                if(pos2===-1)targetColRow.insertAt(movedTab,position)
                else if(pos2<position) {
                    console.log("pos2=", pos2, "position=", position)
                    targetColRow.insertAt(movedTab,position-1)
                }
                else targetColRow.insertAt(movedTab,position)
            }
            else targetColRow.insertAt(movedTab,position)
        }
        else {
            newColRow=targetColRow.createRow()
//            if(movedTab.objectName==="tabdock")
            if(movedTab.type && movedTab.type==="SkTabDock")
                newColRow.addTabDock(movedTab)
            else newColRow.addTab(movedTab)
            newColRow.addTabDock(trgtTabDock)
            targetColRow.insertAt(newColRow,position)
        }
    }
    function moveTabRight(trgtTabDock, movedTab, position){
        var targetColRow=trgtTabDock.parent
        var newColRow
//        if(targetColRow.objectName==="row"){
        if(targetColRow.type && targetColRow.type==="SkRow"){
//            if(movedTab.objectName==="tabdock"){
            if(movedTab.type && movedTab.type==="SkTabDock"){
                var pos2=targetColRow.childPosition(movedTab)
                if(pos2===-1)targetColRow.insertAt(movedTab,position+1)
                else if(pos2<position) targetColRow.insertAt(movedTab,position)
                else targetColRow.insertAt(movedTab,position+1)
            }
            else targetColRow.insertAt(movedTab,position+1)
        }
        else{
            newColRow=targetColRow.createRow()
            newColRow.addTabDock(trgtTabDock)
//            if(movedTab.objectName==="tabdock")
            if(movedTab.type && movedTab.type==="SkTabDock")
                newColRow.addTabDock(movedTab)
            else newColRow.addTab(movedTab)
            targetColRow.insertAt(newColRow,position)
        }
    }
    function moveTabUp(trgtTabDock, movedTab, position){
        var targetColRow=trgtTabDock.parent
        var newColRow
//        if(targetColRow.objectName==="col"){
        if(targetColRow.type && targetColRow.type==="SkColumn"){
//            if(movedTab.objectName==="tabdock"){
            if(movedTab.type && movedTab.type==="SkTabDock"){
                var pos2=targetColRow.childPosition(movedTab)
                if(pos2===-1)targetColRow.insertAt(movedTab,position)
                else if(pos2<position) {
                    targetColRow.insertAt(movedTab,position-1)
                }
                else targetColRow.insertAt(movedTab,position)
            }
            else targetColRow.insertAt(movedTab,position)
        }
        else {
            newColRow=targetColRow.createColumn()
//            if(movedTab.objectName==="tabdock")
            if(movedTab.type && movedTab.type==="SkTabDock")
                newColRow.addTabDock(movedTab)
            else newColRow.addTab(movedTab)
            newColRow.addTabDock(trgtTabDock)
            targetColRow.insertAt(newColRow,position)
        }
    }
    function moveTabDown(trgtTabDock, movedTab, position){
        var targetColRow=trgtTabDock.parent
        var newColRow
//        if(targetColRow.objectName==="col"){
        if(targetColRow.type && targetColRow.type==="SkColumn"){
//            if(movedTab.objectName==="tabdock"){
            if(movedTab.type && movedTab.type==="SkTabDock"){
                var pos2=targetColRow.childPosition(movedTab)
                if(pos2===-1)targetColRow.insertAt(movedTab,position+1)
                else if(pos2<position) targetColRow.insertAt(movedTab,position)
                else targetColRow.insertAt(movedTab,position+1)
            }
            else targetColRow.insertAt(movedTab,position+1)
        }
        else {
            newColRow=targetColRow.createColumn()
            newColRow.addTabDock(trgtTabDock)
//            if(movedTab.objectName==="tabdock")
            if(movedTab.type && movedTab.type==="SkTabDock")
                newColRow.addTabDock(movedTab)
            else newColRow.addTab(movedTab)
            targetColRow.insertAt(newColRow,position)
        }
    }

    function addRow(){
        console.log("SkDock.addRow() : creating SkRow component in SkDock")
        var rowObj=null
        var comp=Qt.createComponent("SkRow.qml")
        rowObj=comp.createObject(rootdock)
        rowObj.anchors.fill=rootdock
        //  There can be only one top container, so we wrap subitems into the row we just created
        if(rootdock.children.length>1) {
            rootdock.unsetChildAnchors(rootdock.children[0])
            rowObj.addColumn(rootdock.children[0])
        }
        return rowObj
    }

    function addColumn(){
        console.log("SkDock.addColumn() : creating SkColumn component in SkDock")
        var colObj=null
        var comp=Qt.createComponent("SkColumn.qml")
        if(comp.status===Component.Ready){
            colObj=comp.createObject(rootdock)
            colObj.anchors.fill=rootdock
        }
        //  There can be only one top container, so we wrap subitems into the column we just created
        if(rootdock.children.length>1) {
            rootdock.unsetChildAnchors(rootdock.children[0])
            colObj.addRow(rootdock.children[0])
        }
        return colObj
    }
    function unsetChildAnchors(child){
        child.anchors.left=undefined
        child.anchors.right=undefined
        child.anchors.top=undefined
        child.anchors.bottom=undefined
        child.anchors.fill=undefined
    }
    function setChildAnchors(child){
        child.anchors.fill=rootdock
    }
    //  Digs into the Column/Row hierarchy and deletes empty ones
    function cleanEmptyColRows(topLevelColRow){
        console.log("SkDock.cleanEmptyColRows(",topLevelColRow.type,") #children=",topLevelColRow.children.length )
        for(var i=0; i<topLevelColRow.children.length; i++){
//            if(topLevelColRow.children[i].objectName==="tabdock" && topLevelColRow.children[i].__tabs.count===0){
            if(     topLevelColRow.children[i].type &&
                    topLevelColRow.children[i].type==="SkTabDock" &&
                    topLevelColRow.children[i].__tabs.count===0){
                rootdock.removeTabDock(topLevelColRow.children[i])
            }
//            else if(topLevelColRow.children[i].objectName==="col" || topLevelColRow.children[i].objectName==="row"){
            else if(topLevelColRow.children[i].type && (topLevelColRow.children[i].type==="SkColumn" || topLevelColRow.children[i].type==="SkRow")){
                if(topLevelColRow.children[i].children.length===0) topLevelColRow.children[i].parent=null
                else {
                    rootdock.cleanEmptyColRows(topLevelColRow.children[i])
                    //we check again in case it just got emptied
                    if(topLevelColRow.children[i].children.length===0){
                        topLevelColRow.children[i].parent=null
                        --i
                    }
                }
            }   //----endof else if
        }   //----endof for
    }

    //  Digs into the Column/Row hierarchy and transfer children from unecessary ones
    function cleanUselessColRows(topLevelColRow){
        console.log("SkDock.cleanUselessColRows(",topLevelColRow.type,")")
//        var tlcrName=topLevelColRow.objectName
        if(!topLevelColRow.type)return
        var tlcrName=topLevelColRow.type
//        if(tlcrName!=="row" && tlcrName!=="col")return
        if(tlcrName!=="SkRow" && tlcrName!=="SkColumn")return
        for(var i=0; i<topLevelColRow.children.length; i++){
            if(topLevelColRow.children.length===1){
//                if((topLevelColRow.objectName==="row" && topLevelColRow.parent.objectName==="col" && topLevelColRow.children[0].objectName==="col")
                if((topLevelColRow.type==="SkRow" && topLevelColRow.parent.type==="SkColumn" && topLevelColRow.children[0].type==="SkColumn")
                        || (topLevelColRow.type==="SkColumn" && topLevelColRow.parent.type==="SkRow" && topLevelColRow.children[0].type==="SkRow")){
                    //transfer
                    var pos1=topLevelColRow.parent.childPosition(topLevelColRow)
                    for(var j=topLevelColRow.children[0].children.length-1; j>=0; j--){
                        topLevelColRow.parent.insertAt(topLevelColRow.children[0].children[j],pos1)
                    }
                }
            }
            else rootdock.cleanUselessColRows(topLevelColRow.children[i])
        }
    }

    //  Deletes empty tabdocks
    function cleanEmptyTabDocks(){
        var td
        for(var i=0; i<__docks.count;i++){
            td=__docks.get(i).tabdock
            if(td.__tabs.count===0)rootdock.removeTabDock(td)
        }
    }

    //  return the SkTabDock found at point in screen coordinates
    //  or null if none is found
    function tabdockAtPoint(point){
        console.log("SkDock.tabdockAtPoint(", point.x,point.y,")")
        var td, point2
        var withinX, withinY
        for(var i=0; i<__docks.count; i++){
            withinX=false; withinY=false
            td=__docks.get(i).tabdock
            console.log("SkDock.tabdockAtPoint tabdock(",i,") mapping to window td.x=",td.x,"td.y=",td.y)
            point2=td.mapToItem(null, td.x, td.y)
            //there's a problem with mapping to td.parent whereby point2 coordinates becomes twice what they truly are
            point2.x/=2;point2.y/=2
            console.log("SkDock.tabdockAtPoint tabdock(",i,") mapped to window is x=",point2.x,"y=",point2.y)
            point2=topwindow.windowToScreenPoint(point2)
            console.log("SkDock.tabdockAtPoint tabdock(",i,").type=",td.type,"screen coord.=", point2.x,point2.y,"tabdock size=", td.width,td.height, "top point=",point2.x+td.width, point2.y+td.height)
            if( (point.x > point2.x) && (point.x < (point2.x + td.width)) ){
                withinX = true
            }
            if( (point.y > point2.y) && (point.y < (point2.y + td.height)) ){
                withinY = true}
            if(withinX && withinY) return td
            else console.log("SkDock.tabdockAtPoint, nothing found in", i)
        }
        return null
    }

    //  Save tabDock to the __docks list if it's not already in it.
    function registerTabDock(tabDock){
        console.log("SkDock.registerTabDock: existing docks=", rootdock.__docks.count)
        for(var i=0; i<rootdock.__docks.count; i++){
            if(tabDock===__docks.get(i).tabdock) return
        }
        //  If we're here, tabDock is not registered so we should add it
        rootdock.__docks.append({tabdock:tabDock})
        tabDock.dock=rootdock
        return tabDock
    }

    //  Look into every SkTabDock to find SkTab to be selected by "title"
    function selectTabByTitle(searchTitle){
        var foundDock, foundTab
        for(var i=0;i<__docks.count;i++){
            var td = __docks.get(i).tabdock
            for(var j=0;j<td.__tabs.count;j++){
                var t=td.__tabs.get(j).tab
                if(t.title===searchTitle){
                    foundDock=i
                    foundTab=j
                }
                if(foundTab!==undefined)break
            }
            if(foundTab!==undefined)break
        }
        if(foundTab!==undefined){
            var tdock=__docks.get(foundDock).tabdock
            tdock.currentIndex=foundTab
            return tdock.__tabs.get(foundTab).tab
        }
        return
    }

    //  Adds a top SkRow as first container
    Component.onCompleted: {
        var success=rootdock.addRow()
        if(success===null)console.log("Err: SkDock.onCompleted failed at creating first top SkRow{}")
    }
}
