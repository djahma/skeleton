import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import "../Themes"

Rectangle{
    id: root
    objectName: "tabdock"
    property string type: "SkTabDock"
    implicitHeight: 300
    implicitWidth: 200
    property int minHeight: 100
    property int minWidth: 100
    //internal: last known size
    property int _height: 300
    property int _width: 200
    property int _prevX: 0
    property int _prevY: 0
    radius: Theme.control.button.radius
    color:"transparent"
    border.width: Theme.border.width.normal
    border.color: Theme.border.color.dark

    //Parent Docking area for sub SkTabDocks
    // Bottom-Up hierarchy is: tabhead.tabbar.TABDOCK.dock.topwindow
    property var dock
    //Holds the currently selected tab index
    property int currentIndex: 0
    //holds the quantity of tabs under this SkTabDock
    property int count: __tabs.count
    /*! \internal
    List to store all tabs currently parented by this SkTabDock
    */
    property ListModel __tabs: ListModel{}

    /*
      addTabs(Item tabs) adds only SkTab Items
      Takes an array of QtObject as input in 'tabs'
      */
    function addTabs(tabs){
        console.log("SkTabDock.addTabs(): tabs=",tabs.length)
        for(var i=0;i<tabs.length;i++){
//            if(tabs[i].objectName!=="tab")continue
            if(!tabs[i].type || tabs[i].type!=="SkTab")continue
            tabs[i].index=count
            tabs[i].tabdock=root
            tabs[i].parent=root
            tabs[i].head.parent=tabbar
            __tabs.append({tab:tabs[i]})
            currentIndex=tabs[i].index
            console.log("new __tab.count ", count)
        }
        updateTabDisplay()
    }
    //    Adds one SkTab object
    function addTab(sktab){
        console.log("SkTabDock.addTab()")
//        if(sktab.objectName!=="tab")return
        if(!sktab.type || sktab.type!=="SkTab")return
        sktab.index=count
        sktab.tabdock=root
        sktab.head.tabbar=tabbar
        sktab.head.parent=tabbar

        sktab.parent=root
        __tabs.append({tab:sktab})
        currentIndex=sktab.index
        updateTabDisplay()
    }
    //Removes the tab from SkTabDock at index
    function removeTab(index){
        console.log("SkTabDock.removeTab(",index,")")
        __tabs.remove(index)
        if(index===currentIndex)currentIndex=Math.max(0,--currentIndex)
        for(var i=index;i<count;i++){
            __tabs.get(i).tab.index=i
        }
        updateTabDisplay()
        if(__tabs.count===0) root.destroy()
    }

    //  Given an array of tabs parameter, removes these tabs from this tabdock
    function removeTabs(tabsArray){
        for(var i=0; i<tabsArray.length; i++){
            for(var j=0; j<__tabs.count; j++){
                if(tabsArray[i]===__tabs.get(j).tab){
                    removeTab(j)
                    break;
                }
            }
        }
    }
    //Swaps two tabs positions
    function moveTab(from, to){
        console.log("SkTabDock.moveTab(from=",from,", to=",to,")")
        currentIndex=to
        __tabs.move(from,to,1)
        __tabs.get(from).tab.index=to
        __tabs.get(to).tab.index=from
        for(var i=0;i<__tabs.count;i++){
            __tabs.get(i).tab.index=i
        }
        updateTabDisplay()
    }
    /*
    updateTabDisplay() refreshes the headers and bodies lists
    and call for a refresh of their display in their respective tabbar and tabbody elements
    */
    function updateTabDisplay(){
        tabbody.__bodies.clear()
        tabbar.__heads.clear()
        console.log("SkTabDock.updateTabDisplay: ",objectName, "#elements:", __tabs.count,"currentIndex=", currentIndex)
        if(__tabs.count===0) root.destroy()
        for(var i=0;i<__tabs.count;i++){
            var element = __tabs.get(i).tab
            tabbar.__heads.append({item:element.head})
            tabbar.__heads.get(i).item.index=i
            tabbody.__bodies.append({item:element.body})
        }
        if(currentIndex>=__tabs.count)currentIndex=__tabs.count-1
        else{
            tabbar.update(currentIndex)
            tabbody.update(currentIndex)
        }
    }

    function unsetAnchors(){
        root.anchors.top=undefined
        root.anchors.bottom=undefined
        root.anchors.left=undefined
        root.anchors.right=undefined
    }

    function setAnchors(){
//        if(root.parent.objectName=="row"){
        if(root.parent.type && root.parent.type==="SkRow"){
            root.anchors.top=root.parent.top
            root.anchors.bottom=root.parent.bottom
            root.anchors.left=undefined
            root.anchors.right=undefined
        }
//        else if(root.parent.objectName=="col"){
        else if(root.parent.type && root.parent.type==="SkColumn"){
            root.anchors.top=undefined
            root.anchors.bottom=undefined
            root.anchors.left=root.parent.left
            root.anchors.right=root.parent.right
        }
    }

    function setPreviousPosition(){
        root._prevX=root.x
        root._prevY=root.y
    }

    function placeAtPreviousPosition(){
//        if(root.parent.objectName=="row") root.x=root._prevX
//        else if(root.parent.objectName=="col") root.y=root._prevY
        if(root.parent.type && root.parent.type==="SkRow") root.x=root._prevX
        else if(root.parent.type && root.parent.type==="SkColumn") root.y=root._prevY
    }

    function getTabsArray(){
        var tabsArr=[]
        for(var i=0; i<__tabs.count; i++){
            tabsArr[i]=__tabs.get(i).tab
        }
        return tabsArr
    }

    onCurrentIndexChanged: {
        tabbar.update(currentIndex)
        tabbody.update(currentIndex)
        if(__tabs.count>0) dock.topwindow.title=__tabs.get(currentIndex).tab.head.title
        else dock.topwindow.title="External Window"
    }

    /*
      The Item to display tab headers
      */
    SkTabBar{
        id: tabbar
        tabdock: root
    }
    /*
      The Item to display tab content in a frame
      */
    SkTabBody{
        id: tabbody
        tabdock: root
        anchors.top:tabbar.bottom
        anchors.left:parent.left
        anchors.leftMargin: Theme.border.width.normal
        anchors.right:parent.right
        anchors.rightMargin: Theme.border.width.normal
        anchors.bottom: parent.bottom
        anchors.bottomMargin: Theme.border.width.normal
        //        border.color: "yellow"
        //        border.width: 2
    }
    /*
      Drag stuff
      */
    Drag.active:tabbar.dragtabdock.drag.active
    Drag.keys: ['tabdockdrag']
    Drag.hotSpot.x: root.mapFromItem(tabbar.dragtabdock, tabbar.dragtabdock.mouseX, tabbar.dragtabdock.mouseY).x
    Drag.hotSpot.y: root.mapFromItem(tabbar.dragtabdock, tabbar.dragtabdock.mouseX, tabbar.dragtabdock.mouseY).y

    /*
      Resizing tab dock area
      */
    MouseArea{
        id: resizeMouse
        hoverEnabled: true
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        width: 16
        height: 16
        drag.target: resizeImg
        preventStealing: true
        Image {
            id: resizeImg
            visible: resizeMouse.containsMouse ? true : false
            source: "../Themes/default/images/resize-notification.png"
            width: parent.width
            height: parent.height
            anchors.right: parent.right
            anchors.bottom: parent.bottom
        }
        onMouseXChanged: {
            if(resizeMouse.drag.active){
                var deltaWidth=resizeMouse.mouseX + resizeMouse.x + resizeMouse.width - root.width
//                if(root.parent.objectName==="row"){
                if(root.parent.type && root.parent.type==="SkRow"){
                    var position=root.parent.childPosition(root)
                    if(position<root.parent.children.length-1){
                        var diffFront = root.parent.children[position+1].minWidth - (root.parent.children[position+1].width-deltaWidth)
                        if(diffFront>0)deltaWidth-=diffFront
                        var diffBack = root.minWidth-(root.width+deltaWidth)
                        if(diffBack>0)deltaWidth+=diffBack
                        root.parent.children[position+1].width-=deltaWidth
                        root.width+=deltaWidth
                    }
                    else {
                        root.parent.resizeWidth(root, deltaWidth)
                    }
                }
//                else if(root.parent.objectName==="col"){
                else if(root.parent.type && root.parent.type==="SkColumn"){
                    root.parent.resizeWidth(root, deltaWidth)
                }
            }
        }
        onMouseYChanged: {
            if(resizeMouse.drag.active) {
                var deltaHeight=resizeMouse.mouseY + resizeMouse.y + resizeMouse.height - root.height
//                if(root.parent.objectName==="row"){
                if(root.parent.type && root.parent.type==="SkRow"){
                    root.parent.resizeHeight(root, deltaHeight)
                }
//                else if(root.parent.objectName==="col"){
                else if(root.parent.type && root.parent.type==="SkColumn"){
                    var position=root.parent.childPosition(root)
                    if(position<root.parent.children.length-1){
                        var diffDown = root.parent.children[position+1].minHeight - (root.parent.children[position+1].height-deltaHeight)
                        if(diffDown>0)deltaHeight-=diffDown
                        var diffUp = root.minHeight-(root.height+deltaHeight)
                        if(diffUp>0)deltaHeight+=diffUp
                        root.parent.children[position+1].height-=deltaHeight
                        root.height+=deltaHeight
                    }
                    else {
                        root.parent.resizeHeight(root, deltaHeight)
                    }
                }
            }
        }   //----End of onMouseYChanged:
    }   //----End of MouseArea
    states:[
        State{
            when: root.Drag.active
            PropertyChanges {
                target: root
                opacity: 0.618
                border.color: Theme.border.color.focus
            }
        }
    ]
    Behavior on x {
         PropertyAnimation { properties: "x"; easing.type: Easing.OutCubic }
     }
    Behavior on y {
         PropertyAnimation { properties: "y"; easing.type: Easing.OutCubic }
     }
}
