package main

import (
	"errors"
	"fmt"
	"strings"
)

//	Parse string messages received from StdIn
func Parse(str string) error {
	strs := strings.Split(str, " ")
	if len(strs) <= 1 {
		e := fmt.Sprintln("Parsing error in mainUI, not enough elements in given string",
			strs)
		return errors.New(e)
	}
	//	Get is a request for information from mainUI
	if strs[0] == "Get" {
		switch strs[1] {
		case "pluginRpcAddress":
		default:
			fmt.Println(skel.LogPrefix(), "Unrecognised Get request", strs)
		}
		//	Push is for information sent to mainUI that do not require an answer.
	} else if strs[0] == "Push" {
		switch strs[1] {
		case "quit":
			fmt.Println(skel.LogPrefix(), "Received Quit signal")
		case "log":
			fmt.Println(skel.LogPrefix(), strings.Join(strs[2:], " "))
		}
	} else {
		e := fmt.Sprintln("Parsing error in mainUI, no match for string sequence:",
			strs)
		return errors.New(e)
	}
	return nil
}
