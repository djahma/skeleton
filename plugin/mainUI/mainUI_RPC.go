package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"gopkg.in/qml.v1"
)

//	Type MainUI provides the services available to other plugins via RPC.
type MainUI int

type Args struct {
	X, Y int
}

type Reply int

func (m *MainUI) Add(args *Args, reply *Reply) error {
	*reply = Reply(args.X + args.Y)
	return nil
}
func (m *MainUI) Write(str string) {
	fmt.Println(skel.LogPrefix(), "un message depuis depuis qml event", str)
}

type MenuArgs struct {
	MenuPath []string
	IsPath   bool
	ObjPath  string
}

//	Adds a qml MenuItem{} at the specified position
//		args is a json object of the form:
//		{MenuPath":["File", "Open Recent", "..."],	//under which Menu{} to place MenuItem{}
//			"IsPath": bool,					//true if ObjPath is an actual path, fasle if a string
//			"ObjPath":"/path/to/qml/for/MenuItem/object"}
func (m *MainUI) AddMenuItem(args *string, err *string) error {
	defer func() {
		if x := recover(); x != nil {
			fmt.Println(skel.LogPrefix(), "run time panic: ", x)
		}
	}()
	mArgs := &MenuArgs{}
	if e := json.Unmarshal([]byte(*args), mArgs); e != nil {
		fmt.Println(skel.LogPrefix(), "Error unmarshalling in AddMenuItem", e)
	}
	//	We first look for the correct to put the MenuItem
	menu := qmlWindow.Object("menuBar").List("menus")
	var targetMenu qml.Object
	var menus []qml.Object
	menu.Convert(&menus)
	targetMenu = menus[0]
	for _, val := range mArgs.MenuPath {
		found := false
		for i, v := range menus {
			fmt.Println("item #", i, "type=", v.Property("type"))
			//	type==2 is  a Menu{}; type==1 is a MenuItem{}
			if v.Property("type").(int) == 2 && val == v.String("title") {
				targetMenu = menus[i]
				menus[i].List("items").Convert(&menus)
				found = true
				break
			}
		}
		if !found {
			*err = "AddMenuItem error: unknown MenuPath item:" + val
			return errors.New(*err)
		}
	}
	//	Then we load the QML for the MenuItem
	var newObj qml.Object
	var e error
	if mArgs.IsPath {
		fmt.Println(skel.LogPrefix(), "AddMenuItem isPath is true", mArgs.ObjPath)

		newObj, e = qmlEngine.LoadFile(mArgs.ObjPath)
		if e != nil {
			*err = "AddMenuItem error: erroneous ObjPath: " + mArgs.ObjPath + e.Error()
			return errors.New(*err)
		}
	} else {
		newObj, e = qmlEngine.LoadString("", mArgs.ObjPath)
		if e != nil {
			*err = "AddMenuItem error: erroneous ObjPath: " + mArgs.ObjPath + e.Error()
			return errors.New(*err)
		}
	}
	o := newObj.Create(nil)
	//	Eventually we add the MenuItem
	targetMenu.Call("insertItem", 0, o)
	o.Set("topwindow", qmlWindow.Root())

	for i, v := range mArgs.MenuPath {
		fmt.Println(skel.LogPrefix(), i, v)
	}
	return nil
}

//	Adds a qml SkTab{} into main window
//		args is a json object of the form:
//		{"IsPath": bool,					//true if ObjPath is an actual path, fasle if a string
//			"ObjPath":"/path/to/qml/for/SkTab/object"}
func (m *MainUI) AddSkTab(args *string, err *string) error {
	defer func() {
		if x := recover(); x != nil {
			fmt.Println(skel.LogPrefix(), "run time panic: ", x)
		}
	}()
	fmt.Println(skel.LogPrefix(), "Entering AddSkTab", *args)

	mArgs := &MenuArgs{}
	if e := json.Unmarshal([]byte(*args), mArgs); e != nil {
		fmt.Println(skel.LogPrefix(), "Error unmarshalling in AddSkTab", e)
	}
	fmt.Println(skel.LogPrefix(), "after AddSkTab args convertion", mArgs)

	var newObj qml.Object
	var loadfileerror error
	if mArgs.IsPath {
		fmt.Println(skel.LogPrefix(), "AddSkTab isPath is true", mArgs.ObjPath)
		fmt.Println(qmlWindow)
		fmt.Println(qmlWindow.String("title"))
		qmlWindow.Set("title", "addsktab")
		// newObj, loadfileerror = qmlEngine.LoadFile(mArgs.ObjPath)
		fmt.Println(skel.LogPrefix(), "right after newObj")

		if loadfileerror != nil {
			*err = "AddSkTab error: erroneous ObjPath: " + mArgs.ObjPath + loadfileerror.Error()
			return errors.New(*err)
		}
		fmt.Println(skel.LogPrefix(), "AddSkTab newObj should be created")
	} else {
		fmt.Println(skel.LogPrefix(), "AddSkTab isPath is false")

		newObj, loadfileerror = qmlEngine.LoadString("", mArgs.ObjPath)
		if loadfileerror != nil {
			*err = "AddSkTab error: erroneous ObjPath: " + mArgs.ObjPath + loadfileerror.Error()
			return errors.New(*err)
		}
	}
	fmt.Println(skel.LogPrefix(), "AddSkTab looking for tabdock")
	newObj = newObj.Create(nil)
	topdock := qmlWindow.ObjectByName("topdock")
	fmt.Println(skel.LogPrefix(), "Calling tabdock addTab()")

	topdock.Call("addTab", newObj)
	fmt.Println(skel.LogPrefix(), "new SkTab should have been added")

	return nil
}

//	Register a plugin offering a qml Tab{} to be displayed in the preferences window.
//		args is a json object of the form:
//		{MenuPath":["PluginName"],		//name of plugin to display it's preference Tab{}
//			"IsPath": bool,			//true if ObjPath is an actual path, fasle if a string
//			"ObjPath":"/path/to/qml/for/Tab/object"}
func (m *MainUI) RegisterPreferenceTab(args *string, err *string) error {
	defer func() {
		if x := recover(); x != nil {
			fmt.Println(skel.LogPrefix(), "run time panic: ", x)
		}
	}()
	fmt.Println(skel.LogPrefix(), "Entering RegisterPreferenceTab", *args)
	mArgs := &MenuArgs{}
	if e := json.Unmarshal([]byte(*args), mArgs); e != nil {
		fmt.Println(skel.LogPrefix(), "Error unmarshalling in AddSkTab", e)
	}
	prefwin := qmlWindow.Object("preferenceWindow")
	prefwin.Call("registerNewTab", mArgs.MenuPath[0], mArgs.IsPath, mArgs.ObjPath)
	return nil
}
