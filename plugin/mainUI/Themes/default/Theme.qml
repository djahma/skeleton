import QtQuick 2.2

pragma Singleton
 
QtObject {
    property SystemPalette activeColors: SystemPalette{
        colorGroup: SystemPalette.Active;
    }
    property SystemPalette inactiveColors: SystemPalette{
        colorGroup: SystemPalette.Inactive;
    }
    property SystemPalette disabledColors: SystemPalette{
        colorGroup: SystemPalette.Disabled;
    }

    /*
      Window Theme
    */
    property QtObject window: QtObject{
        property color backgroundActive: activeColors.window;
        property color backgroundInactive: inactiveColors.window;
        property color backgroundDisabled: disabledColors.window;
    }
    /*
      Borders Theme
    */
    property QtObject border: QtObject{
        property QtObject width: QtObject{
            property int thin: 1;
            property int normal: 2
            property int thick: 3;
        }
        property QtObject color: QtObject{
            property color light: activeColors.light;
//            property color colorDark: Qt.lighter(activeColors.shadow,0.5);
            property color dark: activeColors.alternateBase;
            property color normal: activeColors.dark;
            property color shadow: activeColors.shadow;
            property color focus: activeColors.highlight;
            property color disabled: disabledColors.highlight;
        }
    }
    /*
      Text Theme
    */
    property QtObject text: QtObject{
        property QtObject color: QtObject{
            property color active: activeColors.windowText;
            property color control : activeColors.buttonText
            property color controlDisabled : disabledColors.buttonText
            property color inactive: inactiveColors.windowText;
            property color disabled: disabledColors.windowText;
            property color selected: activeColors.highlightedText;
        }
        property QtObject title: QtObject{
            property int alignment: Text.AlignHCenter;
            property QtObject font: QtObject{
                property int  sizeH1: 16;
                property int  sizeH2: 12;
            }
        }
        property QtObject normal: QtObject{
            property int alignment: Text.AlignLeft;
            property QtObject font: QtObject{
                property int  sizeSmall: 8;
                property int  sizeNormal: 10;
                property int  sizeBig: 12;
            }
        }
    }

    /*
      Controls Theme
    */
    property QtObject control: QtObject{
        property QtObject color: QtObject{
            property color backgroundActive: activeColors.base;
            property color backgroundInactive: inactiveColors.window;
            property color backgroundDarkActive: activeColors.alternateBase;
            property color buttonBackgroundActive: activeColors.button;
            property color buttonBackgroundDisabled: disabledColors.button;
            property color selected: activeColors.highlight;
//            property color backgroundActive: Qt.lighter(activeColors.shadow,0.75);
//            property color backgroundInactive: Qt.lighter(inactiveColors.shadow,0.65);
        }
        property QtObject tab: QtObject{
            property int heightActive: 23;
            property int heightInactive: 20;
            property int widthMax:300;
        }
        property QtObject button: QtObject{
            property int radius: 3
        }
    }
}
