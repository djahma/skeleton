package main

import (
	"fmt"
	"io/ioutil"
	"net/rpc"
	"os"
	"skeleton/API/skeleton"
)

//	GoObject is published to the QML side.
//		This type serves as a bridge to execute go code from QML
type GoObj struct {
	//---------------------------Externals----------------------//
	Goargs RPCArgs
	//---------------------------Internals----------------------//
	sk           *skeleton.Skeleton
	rpcClientMap map[string]*rpc.Client
}

func (g *GoObj) OkToSkeleton() {
	g.sk.Ok()
}
func (g *GoObj) Quit() {
	g.sk.Quit()
}

type RPCArgs struct {
	Strs   []string
	Runes  []rune
	Ints   []int64
	Floats []float64
	Bools  []bool
}

func (g *GoObj) PWD() string {
	dir, _ := os.Getwd()
	return dir
}

func (g *GoObj) PluginDir(pluginName string) string {
	fmt.Println(skel.LogPrefix(), "Entering func (g *GoObj) PluginDir")

	var dir string
	if val, err := skel.GetPluginDir(pluginName); err != nil {
		fmt.Println(skel.LogPrefix(), err)
		return ""
	} else {
		dir = val
	}
	return dir
}

//Callrpc allow for calling an rpc function from the QML side when GoObj is imported.
//All data and parameters exchanges are done in the form of strings of json objects
//	pluginName is the plugin to be called via RPC
//	rpcFunc is the classic "Object.Function" to be called via RPC
//	args is a stringified JSON object that will be passed "as is" to the rpcFunc.
func (g *GoObj) Callrpc(pluginName string, rpcFunc string, args string) string {
	defer func() {
		if x := recover(); x != nil {
			fmt.Println(skel.LogPrefix(), "run time panic: ", x)
		}
	}()
	fmt.Println(g.sk.LogPrefix(), "Entering CallRPC:", pluginName, rpcFunc, args)

	//first get an RPC client to the plugin to be called
	if g.rpcClientMap == nil {
		g.rpcClientMap = make(map[string]*rpc.Client)
	}
	// fmt.Println(g.sk.LogPrefix(), "Callrpc: after test for g.rpcClientMap == nil")

	if _, ok := g.rpcClientMap[pluginName]; !ok {
		// fmt.Println(g.sk.LogPrefix(), "Callrpc: couldnt find g.rpcClientMap[", pluginName, "]")
		if client, e := g.sk.RpcClient(pluginName); e != nil {
			fmt.Println("Error in getting an rpc client:", e)
		} else {
			g.rpcClientMap[pluginName] = client
		}
	}
	// fmt.Println(g.sk.LogPrefix(), "Callrpc: g.rpcClientMap[pluginName] is not nil anymore")

	var itfce string
	g.rpcClientMap[pluginName].Call(rpcFunc, &args, &itfce)
	// fmt.Println(g.sk.LogPrefix(), "Callrpc: after g.rpcClientMap[pluginName].Call(rpcFunc, &args, &itfce)")

	// fmt.Println(g.sk.LogPrefix(), "response is", itfce)
	return itfce
}

func (g *GoObj) formatRPCArgs(args ...interface{}) *RPCArgs {
	rpcA := &RPCArgs{}
	for _, v := range args {
		switch val := v.(type) {
		case string:
			rpcA.Strs = append(rpcA.Strs, string(val))
		case rune:
			rpcA.Runes = append(rpcA.Runes, rune(val))
		case int:
			rpcA.Ints = append(rpcA.Ints, int64(val))
		case float64:
			rpcA.Floats = append(rpcA.Floats, float64(val))
		}
	}
	return rpcA
}

func (p *GoObj) Write(str string) {
	fmt.Println(skel.LogPrefix(), "un message depuis GoObj in QML:", str)

}

//FileContent allows to access content of files
//	mode		is any of "read" "write" "append"
//	filepath	is complete path to file
func (g *GoObj) FileContent(mode string, filepath string, content string) string {
	switch mode {
	case "read":
		if cont, err := ioutil.ReadFile(filepath); err != nil {
			fmt.Println(skel.LogPrefix(), "Error in FileContent(", filepath, "):", err.Error())
			return ""
		} else {
			return string(cont)
		}
	case "write":
		if err := ioutil.WriteFile(filepath, []byte(content), os.ModePerm); err != nil {
			fmt.Println(skel.LogPrefix(), "Error in FileContent(", filepath, ",", mode, "):", err.Error())
			return ""
		}
	case "append":
		if err := ioutil.WriteFile(filepath, []byte(content), os.ModeAppend); err != nil {
			fmt.Println(skel.LogPrefix(), "Error in FileContent(", filepath, "):", err.Error())
			return ""
		}
	}
	return ""
}
