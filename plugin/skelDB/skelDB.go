package main

import (
	"fmt"
	"os"
	"skeleton/API/skeleton"
	"skeleton/plugin"
	"skeleton/plugin/mainUI/API"
)

var (
	plug *plugin.Plugin
	skel *skeleton.Skeleton
)

func main() {
	defer func() {
		if x := recover(); x != nil {
			fmt.Println(skel.LogPrefix(), "run time panic: ", x)
		}
	}()
	skel = &skeleton.Skeleton{PluginName: "skelDB"}
	if err := skel.InitRPC(os.Args[1], os.Args[2]); err != nil {
		fmt.Println(skel.LogPrefix(), err)
		return
	}
	plug := &plugin.Plugin{}
	sdb := NewSkelDB()
	plug.Init(skel, Parse, sdb)
	defer func() {
		skel.Close()
		plug.Stop()
	}()
	mui := new(mainUI.MainUI)
	mui.Initialise(plug, skel)
	defer mui.Close()
	_ = mui.AddMenuItem([]string{"View", "New Tab"}, true,
		plug.Path+"/qml/skelDB_menuItem/menuItem.qml")
	_ = mui.RegisterPreferenceTab("skelDB", true, plug.Path+"/qml/skelDB_prefTab/prefTab.qml")
	skel.Ok()
	<-plug.QuitChan
	fmt.Println(skel.LogPrefix(), "after quitchan")
}
