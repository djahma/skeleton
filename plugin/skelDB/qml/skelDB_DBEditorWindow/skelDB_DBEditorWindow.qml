import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Window 2.0
import QtQuick.Layouts 1.1
import "../../../mainUI/qml"
import "../../../mainUI/Themes"
import GoExtensions 1.0

Window{
    id: editorwin
    title: "Database Viewer Editor"
    modality:Qt.WindowModal
    color: Theme.window.backgroundActive
    minimumWidth :400
    minimumHeight : 400
    property var treeobj
    FocusScope{
        anchors.fill: parent
        RowLayout{
            id:firstRow
            Button{
                text: "New"
                iconName: "add"
            }
            Button{
                text: "Save Changes"
                iconName: "stock_save"
                onClicked: {
                    var filepath=goobj.pluginDir("skelDB")+"/config/DBSortingConf.json"
                    goobj.fileContent("write",filepath,JSON.stringify(treeobj,null,"\t"))
                }
            }
        }

        ListView{
            id:listView
            height:Math.min(childrenRect.height,400)
            anchors.top:firstRow.bottom
            anchors.bottom:txtarea.top
            anchors.left: parent.left
            anchors.right: parent.right
            delegate: listViewDelegate
            spacing:3

            Component{
                id:listViewDelegate
                Rectangle{
                    id: dlgRect
                    height: 113
                    z: mouseTreeObj.drag.active? 10:0
                    anchors.left: parent.left
                    anchors.right: parent.right
                    anchors.margins: Theme.control.button.radius
                    radius: Theme.control.button.radius
                    color: Theme.window.backgroundActive
                    border.width: Theme.border.width.normal
                    border.color: Theme.border.color.dark
                    Drag.active: mouseTreeObj.drag.active
                    Drag.keys:["treeobj"]
                    Drag.hotSpot.x:mouseTreeObj.mouseX
                    Drag.hotSpot.y: mouseTreeObj.mouseY
                    Item{
                        anchors.fill:parent
                        anchors.margins: Theme.control.button.radius
                        RowLayout{
                            anchors.fill:parent
                            Column{
                                Layout.fillWidth: true
                                RowLayout{
                                    anchors.left: parent.left
                                    anchors.right: parent.right
                                    Label{text: "DataBase:"}
                                    ComboBox{
                                        id: comboDB
//                                        model: modelData.db
                                        editable: true
                                        Layout.fillWidth: true
                                        Component.onCompleted: {
                                            var dbList=goobj.callrpc("skelDB", "SkelDB.GetDBNames","")
                                            var dbListObj=JSON.parse(dbList)
                                            dbListObj.unshift(modelData.db)
                                            model=dbListObj
                                        }
                                        onCurrentTextChanged: {
                                            var colList=goobj.callrpc("skelDB", "SkelDB.GetCollectionNames",currentText)
                                            var colListObj=JSON.parse(colList)
                                            if(colListObj===null)colListObj=new Array()
                                            colListObj.unshift(modelData.collection)
                                            comboCollec.model=colListObj
                                        }
                                    }
                                }
                                RowLayout{
                                    anchors.left: parent.left
                                    anchors.right: parent.right
                                    Label{text: "Collection:"}
                                    ComboBox{
                                        id: comboCollec
                                        editable: true
                                        Layout.fillWidth: true
                                    }
                                }
                                RowLayout{
                                    anchors.left: parent.left
                                    anchors.right: parent.right
                                    Label{text:"Nodes:"}
                                    TextField{
                                        id:txtNodes
            //                            anchors.top:comboDB.bottom
                                        Layout.fillWidth: true
                                        text:modelData.nodes.toString()
                                    }
                                }
                                RowLayout{
                                    anchors.left: parent.left
                                    anchors.right: parent.right
                                    Label{text:"Entries:"}
                                    TextField{
                                        id: txtEntries
            //                            anchors.top:txtNodes.bottom
                                        Layout.fillWidth: true
                                        text: modelData.entries.toString()
                                    }
                                }

                            }
                            Column{
                                Button{
                                    iconName: "go-up"
                                    onClicked: {
                                        if(index===0)return
                                        var first=treeobj.dbSortingObjs[index-1]
                                        var second=treeobj.dbSortingObjs[index]
                                        treeobj.dbSortingObjs[index-1]=second
                                        treeobj.dbSortingObjs[index]=first
    //                                    goobj.fileContent("write",filepath,JSON.stringify(objs))
                                        txtarea.text=""
                                        for (var i=0;i<treeobj["dbSortingObjs"].length;i++){
                                            txtarea.text+=treeobj["dbSortingObjs"][i].db+" "+
                                                    treeobj["dbSortingObjs"][i].nodes+" "+
                                                    treeobj["dbSortingObjs"][i].entries.toString()+"\n"
                                        }
                                        listView.model=treeobj.dbSortingObjs
                                    }
                                }
                                Button{
                                    checkable: true
                                    iconName: checked? "stock_yes":"stock_no"
                                    tooltip: "Check to display in tree view"
                                    onCheckedChanged: {
                                        if(checked)treeobj.dbSortingObjs[index].show=true
                                        else treeobj.dbSortingObjs[index].show=false
                                    }
                                    Component.onCompleted: {
                                        if(treeobj.dbSortingObjs[index].show)checked=true
                                        else checked=false
                                    }
                                }

                                Button{
                                    iconName: "go-down"
                                    onClicked: {
                                        if(index===treeobj.dbSortingObjs.length-1) return
                                        var first=treeobj.dbSortingObjs[index+1]
                                        var second=treeobj.dbSortingObjs[index]
                                        treeobj.dbSortingObjs[index+1]=second
                                        treeobj.dbSortingObjs[index]=first
    //                                    goobj.fileContent("write",filepath,JSON.stringify(objs))
                                        txtarea.text=""
                                        for (var i=0;i<treeobj["dbSortingObjs"].length;i++){
                                            txtarea.text+=treeobj["dbSortingObjs"][i].db+" "+
                                                    treeobj["dbSortingObjs"][i].nodes+" "+
                                                    treeobj["dbSortingObjs"][i].entries.toString()+"\n"
                                        }
                                        listView.model=treeobj.dbSortingObjs
                                    }
                                }
                            }
                            Column{
                                Image {
                                    id: move
                                    source: "../../../mainUI/Themes/default/images/view-fullscreen.png"
                                    fillMode: Image.PreserveAspectFit
                                    anchors.left: parent.left
                                    anchors.right: parent.right
                                    MouseArea{
                                        id: mouseTreeObj
                                        anchors.fill: parent
                                        drag.axis: Drag.YAxis
                                        drag.target: dlgRect
                                        onReleased: {
                                            if(dlgRect.Drag.active && dlgRect.Drag.target){
                                                console.log("drag released, targetInd=", dlgRect.Drag.target.ind, "thisInd=",index)
                                                var targetInd=dlgRect.Drag.target.ind
                                                var thisInd=index
                                                if(targetInd!==thisInd){
                                                    var first=treeobj.dbSortingObjs[thisInd]
                                                    var second=treeobj.dbSortingObjs[targetInd]
                                                    treeobj.dbSortingObjs[thisInd]=second
                                                    treeobj.dbSortingObjs[targetInd]=first
                                                    listView.model=treeobj.dbSortingObjs
                                                }
                                            }
                                        }
                                    }
                                }

                                Button{
                                    iconName: "stock_delete"
                                }
                            }
                        }

                        DropArea{
                            id:dropTreeobj
                            anchors.fill: parent
                            keys: ["treeobj"]
                            property int ind:index
                            visible: mouseTreeObj.drag.active? false:true
                        }
                    }
                }
            }
        }

        TextArea{
            id: txtarea
            height:100
            anchors.left: parent.left
            anchors.right:parent.right
            anchors.bottom: parent.bottom
        }
    }
    GoObj{
        id: goobj
    }
    Component.onCompleted: {
        var filepath=goobj.pluginDir("skelDB")+"/config/DBSortingConf.json"
        var content=goobj.fileContent("read",filepath,"")
        var objs=JSON.parse(content)
        for (var i=0;i<objs["dbSortingObjs"].length;i++){
            txtarea.text+=objs["dbSortingObjs"][i].db+" "+
                    objs["dbSortingObjs"][i].nodes+" "+
                    objs["dbSortingObjs"][i].entries.toString()+"\n"
        }
        editorwin.treeobj=objs
        listView.model=editorwin.treeobj.dbSortingObjs
    }
}
