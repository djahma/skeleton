import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import "../../../mainUI/qml"
import "../../../mainUI/Themes"
import GoExtensions 1.0

//  The Component is loaded in a loader with a JSON object
//      called "tempO" that represents an importer definition.
Column{
    spacing: Theme.control.button.radius
    Rectangle{
        border.width: Theme.border.width.thin
        border.color: Theme.border.color.light
        anchors.left: parent.left
        anchors.right: parent.right
        height: 2
    }
    //  Importer name and save/cancel buttons
    RowLayout{
        anchors.left: parent.left
        anchors.right: parent.right
        Label{text: "ImporterDefinition Name:"}
        TextField{
            Layout.fillWidth: true
            onTextChanged: tempO.dataImporterName=text
            Component.onCompleted: if(tempO.dataImporterName)text=tempO.dataImporterName
        }
        Button{
            text: "Cancel"
            tooltip: "Scrap whatever you are doing with current data importer definition: Nothing Done!"
            onClicked: {tempO=undefined}
        }
        Button{
            text: "Save"
            tooltip: "Data importer definition will be saved to './config/def_importer.json'"
            onClicked: save=true
        }
    }
    RowLayout{
        Button{
            text: "Add importer from scratch"
            onClicked: {
                if(!tempO.importers)tempO.importers=new Array
                tempO.importers.push(new Object)
                repeaterFromScratch.model=tempO.importers
            }
        }
    }
    Flickable{
        id: flickFromScratch
        anchors.left: parent.left
        anchors.right: parent.right
        height: contentItem.childrenRect.height
        contentWidth: contentItem.childrenRect.width
        Column{
            id: flickFromScratchCol
            spacing: Theme.control.button.radius
            Repeater{
                id: repeaterFromScratch
                model: if(tempO && tempO.importers)tempO.importers
                delegate: compFromScratch
            }
        }
    }
    Component{
        id: compFromScratch
        RowLayout{
            Button{
                iconName: "gtk-remove"
                onClicked: {
                    parent.visible=false
                    tempO.importers.splice(index,1)
                    repeaterFromScratch.model=tempO.importers
                }
            }
            Label{text:"Source:"}
            ComboBox{
                id: comboSource
                implicitWidth: 200
                property bool hascompleted: false
                onCurrentTextChanged: {
                    if(hascompleted)tempO.importers[index].dataSourceName=currentText
                }
                Component.onCompleted:{
                    var filepath=goobj.pluginDir("skelDB")+"/config/def_datasource.json"
                    var content=goobj.fileContent("read",filepath,"")
                    var jsObj=JSON.parse(content)
                    var sourceArr=new Array
                    for(var i=0;i<jsObj.length;i++)
                        sourceArr.push(jsObj[i].dataSourceName)
                    sourceArr.sort()
                    comboSource.model=sourceArr
                    if(tempO.importers[index].dataSourceName){
                        for(i=0;i<comboSource.model.length;i++){
                            if(comboSource.model[i]===tempO.importers[index].dataSourceName)comboSource.currentIndex=i
                        }
                    }
                    else tempO.importers[index].dataSourceName=currentText
                    hascompleted = true
                }
            }
            Label{text: "Processing"}
            ComboBox{
                id: comboProcessing
                implicitWidth: 200
                property bool hascompleted: false
                onCurrentTextChanged: {
                    if(hascompleted)tempO.importers[index].dataProcessingName=currentText
                }
                Component.onCompleted:{
                    var filepath=goobj.pluginDir("skelDB")+"/config/def_processing.json"
                    var content=goobj.fileContent("read",filepath,"")
                    var jsObj=JSON.parse(content)
                    var processingArr=new Array
                    for(var i=0;i<jsObj.length;i++)
                        processingArr.push(jsObj[i].dataProcessingName)
                    processingArr.sort()
                    comboProcessing.model=processingArr
                    if(tempO.importers[index].dataProcessingName){
                        for(i=0;i<comboProcessing.model.length;i++){
                            if(comboProcessing.model[i]===tempO.importers[index].dataProcessingName)comboProcessing.currentIndex=i
                        }
                    }
                    else tempO.importers[index].dataProcessingName=currentText
                    hascompleted = true
                }
            }
            Label{text: "Operation"}
            ComboBox{
                id: comboOperation
                implicitWidth: 200
                property bool hascompleted: false
                onCurrentTextChanged: {
                    if(hascompleted)tempO.importers[index].dataOperationName=currentText
                }
                Component.onCompleted:{
                    var filepath=goobj.pluginDir("skelDB")+"/config/def_dboperation.json"
                    var content=goobj.fileContent("read",filepath,"")
                    var jsObj=JSON.parse(content)
                    var operationArr=new Array
                    for(var i=0;i<jsObj.length;i++)
                        operationArr.push(jsObj[i].dataOperationName)
                    operationArr.sort()
                    comboOperation.model=operationArr
                    if(tempO.importers[index].dataOperationName){
                        for(i=0;i<comboOperation.model.length;i++){
                            if(comboOperation.model[i]===tempO.importers[index].dataOperationName)comboOperation.currentIndex=i
                        }
                    }
                    else tempO.importers[index].dataOperationName=currentText
                    hascompleted = true
                }
            }
        }
    }
    RowLayout{
        Button{
            text: "Add existing importer"
            onClicked: {
                if(!tempO.subimporters)tempO.subimporters=new Array
                tempO.subimporters.push("")
                repeaterIncludeExisting.model=tempO.subimporters
            }
        }
    }
    Repeater{
        id: repeaterIncludeExisting
        model: if(tempO && tempO.subimporters)tempO.subimporters
        delegate: compIncludeExisting
    }
    Component{
        id: compIncludeExisting
        RowLayout{
            Button{
                iconName: "gtk-remove"
                onClicked: {
                    parent.visible=false
                    tempO.subimporters.splice(index,1)
                    repeaterIncludeExisting.model=tempO.subimporters
                }
            }
            ComboBox{
                id: comboOtherImporter
                implicitWidth: 200
                property bool hascompleted: false
                onCurrentTextChanged: {
                    if(hascompleted)tempO.subimporters[index]=currentText
                }
                Component.onCompleted:{
                    var filepath=goobj.pluginDir("skelDB")+"/config/def_importer.json"
                    var content=goobj.fileContent("read",filepath,"")
                    var jsObj=JSON.parse(content)
                    var importArr=new Array
                    for(var i=0;i<jsObj.length;i++)
                        importArr.push(jsObj[i].dataImporterName)
                    importArr.sort()
                    for(i=0;i<importArr.length;i++){
                        if(importArr[i]===tempO.dataImporterName)
                            importArr.splice(i,1)
                    }
                    if(importArr.length>0){
                        comboOtherImporter.model=importArr
                        if(tempO.subimporters[index]){
                            for(i=0;i<comboOtherImporter.model.length;i++){
                                if(comboOtherImporter.model[i]===tempO.subimporters[index])comboOtherImporter.currentIndex=i
                            }
                        }
                        else tempO.subimporters[index]=currentText
                    }
                    hascompleted = true
                }
            }
        }
    }   //End of compIncludeExisting
}
