import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import "../../../mainUI/Themes"

Rectangle{
    id: subfieldItem
    anchors.left: if(parent!==null)parent.left
    anchors.right: if(parent!==null)parent.right
    anchors.topMargin: Theme.control.button.radius
    anchors.leftMargin: Theme.control.button.radius
    anchors.bottomMargin: Theme.control.button.radius
    color:"transparent"
    border.color: Theme.text.color.active
    border.width: Theme.border.width.thin
    radius: Theme.control.button.radius
    height: childrenRect.height+Theme.control.button.radius*2
    property var parentObj
    property string selectedTxt
    property var colSubFields: colSubFields
    property var comboOnSubField: comboOnSubField
//    onTopFieldsChanged: comboOnSubField.model=topFields
    ColumnLayout{
        anchors.topMargin: Theme.control.button.radius
        anchors.leftMargin: Theme.control.button.radius
        anchors.left: if(parent!==null)parent.left
        anchors.right: if(parent!==null)parent.right
        anchors.top: if(parent!==null)parent.top
        RowLayout{
            anchors.left:parent.left
            anchors.right: parent.right
            Layout.maximumWidth: parent.width-Theme.control.button.radius
            Label{text:"SubField on:"}
            ComboBox{
                id: comboOnSubField
                Layout.fillWidth: true
                onModelChanged: {
                    for(var i=0; i<model.length; i++){
                        if(model[i]===selectedTxt){currentIndex=i; break}
                    }
                }

                onCurrentTextChanged: {
                    selectedTxt=currentText
                    parentObj.updateSubFieldObj()
                }
            }
            Button{
                text: "Remove"
                onClicked: {
                    var ind = subfieldItem.Positioner.index
                    parentObj.removeSubFieldAt(ind)
                }
            }
            Button{
                text: "Add Subfields"
                onClicked: {
                    parentObj.updateSubFieldObj()
                    var comp=Qt.createComponent("subfieldComponent.qml")
                    var obj=comp.createObject(colSubFields,
                                              {   "parentObj": subfieldItem,
                                                  "selectedTxt": ""})
                }
            }
        }
        RowLayout{
            Label{text: "Fields:"}
            TextField{
                id:subfieldTxt
                placeholderText: "comma separated list of fields"
                Layout.fillWidth: true
                onTextChanged: {
                    var fArr = text.split(",")
                    for(var i=0; i<colSubFields.children.length; i++){
                        colSubFields.children[i].updateComboOnSubField(fArr)
                    }
                }
            }
        }
        RowLayout{
            anchors.left:parent.left
            anchors.right: parent.right
            anchors.bottomMargin: Theme.control.button.radius
            Column{
                id: colSubFields
                Layout.fillWidth: true
                anchors.bottomMargin: Theme.control.button.radius
                spacing: Theme.control.button.radius
            }
        }
    }
    Component.onCompleted: {
        var f = parentObj.getFields()
        comboOnSubField.model=f
        for(var i=0; i<f.length; i++){
            if(f[i]===selectedTxt){
                comboOnSubField.currentIndex=i
                break
            }
        }
        var o = parentObj.getFieldObj(selectedTxt)
        var sf = new Array
        if(o!==null) {
            sf = Object.getOwnPropertyNames(o)
            subfieldTxt.text = sf.join(",")
        }
        else subfieldTxt.text = ""
        for(i=0; i<sf.length; i++){
            if(o[sf[i]]){
                var comp=Qt.createComponent("subfieldComponent.qml")
                var obj=comp.createObject(colSubFields,
                                          {   "parentObj": subfieldItem,
                                              "selectedTxt": sf[i]})
            }
        }
    }
    //-----FUNCTIONS-----//
    function getFields(){
        var fieldArr = Object.getOwnPropertyNames(parentObj.getFieldObj(selectedTxt))
        return fieldArr
    }
    function getFieldObj(name){
        var po = parentObj.getFieldObj(selectedTxt)
        if(po[name])return po[name]
        return null
    }
    function removeSubFieldAt(position){
        colSubFields.children[position].parent = null
        parentObj.updateSubFieldObj()
    }
    function builSubfieldObj(){
        var sf = subfieldTxt.text.split(",")
        var fObj = new Object
        for(var i=0; i<sf.length; i++){
            fObj[sf[i]]=null
        }
        for(i=0; i<colSubFields.children.length; i++){
            var curTxt = colSubFields.children[i].comboOnSubField.currentText
            fObj[curTxt]=colSubFields.children[i].builSubfieldObj()
        }
        return fObj
    }
    function updateSubFieldObj(){
        parentObj.updateSubFieldObj()
    }
    function updateComboOnSubField(modelArray){
        var lastTxt = selectedTxt
        comboOnSubField.model=modelArray
        for(var i=0; i<comboOnSubField.model.length; i++){
            if(comboOnSubField.model[i]===lastTxt){comboOnSubField.currentIndex=i; break}
        }
    }
}
