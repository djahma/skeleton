import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import "../../../mainUI/qml"
import "../../../mainUI/Themes"
import GoExtensions 1.0

//  The Component is loaded in a loader with a JSON object
//      called "tempO" that represents an import definition.
Column{
    spacing: Theme.control.button.radius
    Rectangle{
        border.width: Theme.border.width.thin
        border.color: Theme.border.color.light
        anchors.left: parent.left
        anchors.right: parent.right
        height: 2
    }
    //  DataOperation name and save/cancel buttons
    RowLayout{
        anchors.left: parent.left
        anchors.right: parent.right
        Label{text: "DataOperationDefinition Name:"}
        TextField{
            Layout.fillWidth: true
            onTextChanged: tempO.dataOperationName=text
            Component.onCompleted: if(tempO.dataOperationName)text=tempO.dataOperationName
        }
        Button{
            text: "Cancel"
            tooltip: "Scrap whatever you are doing with current data operation definition: Nothing Done!"
            onClicked: {tempO=undefined}
        }
        Button{
            text: "Save"
            tooltip: "Data operation definition will be saved to './config/def_dboperation.json'"
            onClicked: save=true
        }
    }
    RowLayout{
        anchors.left: parent.left
        anchors.right: parent.right
        ComboBox{
            id: comboOperation
            implicitWidth: 150
            property bool hascompleted: false
            model:["Export_to_file","db_Insert","db_Update_only","db_Update_then_Insert"]
            onCurrentTextChanged: if(hascompleted)tempO.operation = currentText
            Component.onCompleted: {
                if(tempO.operation){
                    for(var i=0; i<model.length; i++){
                        if(model[i]===tempO.operation){currentIndex=i;break;}
                    }
                }
                else tempO.operation=currentText
                hascompleted=true
            }
        }
        Label{
            visible: comboOperation.currentText==="Export_to_file"
            text:" File format:"
        }
        ComboBox{
            id: comboFileFormat
            visible: comboOperation.currentText==="Export_to_file"
            implicitWidth: 90
            property bool hascompleted: false
            model:["csv","json"]
            onCurrentTextChanged: if(hascompleted){tempO.fileFormat = currentText;labelExample.setText();}
            Component.onCompleted: {
                if(tempO.fileFormat){
                    for(var i=0; i<model.length; i++){
                        if(model[i]===tempO.fileFormat){currentIndex=i;break;}
                    }
                }
                else tempO.fileFormat=currentText
                hascompleted=true
                labelExample.setText()
            }
        }
        Label{
            visible: comboOperation.currentText!=="Export_to_file"
            text: "Where? : "
        }
        TextField{
            visible: comboOperation.currentText!=="Export_to_file"
            placeholderText: "db,collection,field,subfield..."
            Layout.fillWidth: true
            property bool hascompleted: false
            onTextChanged: if(hascompleted){tempO.dbPath=text}
            Component.onCompleted: {
                if(tempO.dbPath)text=tempO.dbPath
                else tempO.dbPath=""
                hascompleted=true
            }
        }
    }
    //-----File based Elements
    RowLayout{
        anchors.left: parent.left
        anchors.right: parent.right
        visible: comboOperation.currentText==="Export_to_file"
        Label{
            visible: comboOperation.currentText==="Export_to_file"
            text: "Name file after"
        }
        ComboBox{
            id: comboFileAfter
            visible: comboOperation.currentText==="Export_to_file"
            implicitWidth: 90
            property bool hascompleted: false
            model:["field","string"]
            onCurrentTextChanged: if(hascompleted)tempO.filenameAfter = currentText
            Component.onCompleted: {
                if(tempO.filenameAfter){
                    for(var i=0; i<model.length; i++){
                        if(model[i]===tempO.filenameAfter){currentIndex=i;break;}
                    }
                }
                else tempO.filenameAfter=currentText
                hascompleted=true
            }
        }
        TextField{
            visible: comboFileAfter.visible && comboFileAfter.currentText==="string"
            placeholderText: "file name"
            Layout.fillWidth: true
            property bool hascompleted: false
            onTextChanged: if(hascompleted){tempO.filename=text;labelExample.setText();}
            Component.onCompleted: {
                if(tempO.filename)text=tempO.filename
                else tempO.filename=""
                hascompleted=true
                labelExample.setText()
            }
        }
        Label{
            visible: comboFileAfter.visible && comboFileAfter.currentText==="string"
            text: "Number of digits for incremental name:"
        }
        SkSpinBox{
            visible: comboFileAfter.visible && comboFileAfter.currentText==="string"
            implicitWidth: 50
            minimumValue: 1
            property bool hascompleted: false
            onValueChanged: if(hascompleted){tempO.fileDigits=value;labelExample.setText();}
            Component.onCompleted: {
                if(tempO.fileDigits){
                    value=tempO.fileDigits
                }
                else tempO.fileDigits=1
                hascompleted=true
                labelExample.setText()
            }
        }
        TextField{
            visible: comboFileAfter.visible && comboFileAfter.currentText==="field"
            placeholderText: "path,to,key"
            implicitWidth: 150
            Layout.fillWidth: true
            property bool hascompleted: false
            onTextChanged:if(hascompleted)tempO.filenamePath=text
            Component.onCompleted: {
                if(tempO.filenamePath)text=tempO.filenamePath
                else tempO.filenamePath=""
                hascompleted=true
            }
        }
    }
    RowLayout{
        visible: comboFileAfter.visible && comboFileAfter.currentText==="string"
        Label{
            id: labelExample
            text: "Example name:"
            function setText(){
                var str=""
                str+=tempO.filename
                for(var i=1; i<tempO.fileDigits; i++)str+="0"
                str+="1."+tempO.fileFormat
                text="Example name: "+str
            }
        }
    }
    //-----Database Elements
    RowLayout{
        visible:comboOperation.currentText==="db_Update_only" || comboOperation.currentText==="db_Update_then_Insert"
        Label{
            text:"Update based on fields. How many?:"
        }
        SkSpinBox{
            implicitWidth: 50
            minimumValue: 1
            property bool hascompleted: false
            onValueChanged: if(hascompleted){
                                tempO.dbFields=value;
                                if(!tempO.fields)tempO.fields=new Array
                                while(tempO.fields.length>tempO.dbFields)tempO.fields.pop()
                                while(tempO.fields.length<tempO.dbFields)tempO.fields.push("")
                                flickFieldsRepeater.model=tempO.dbFields
                            }
            Component.onCompleted: {
                if(tempO.dbFields){
                    value=tempO.dbFields
                }
                else tempO.dbFields=1
                if(!tempO.fields)tempO.fields=new Array
                while(tempO.fields.length>tempO.dbFields)tempO.fields.pop()
                while(tempO.fields.length<tempO.dbFields)tempO.fields.push("")
                flickFieldsRepeater.model=tempO.dbFields
                hascompleted=true
            }
        }
    }
    Flickable{
        id: flickFields
        visible:comboOperation.currentText==="db_Update_only" || comboOperation.currentText==="db_Update_then_Insert"
        anchors.left: parent.left
        anchors.right: parent.right
        height: contentItem.childrenRect.height
        contentWidth: contentItem.childrenRect.width
        Row{
            id: flickFieldsRow
            spacing: Theme.control.button.radius
            Repeater{
                id: flickFieldsRepeater
                model: tempO.dbFields
                delegate: TextField{
                    placeholderText: "Field_"+index+",subfield_"+index+"..."
                    implicitWidth: 150
                    property bool hascompleted: false
                    onTextChanged: if(hascompleted)tempO.fields[index]=text
                    Component.onCompleted: {
                        if(tempO.fields[index])text=tempO.fields[index]
                        else tempO.fields[index]=""
                        hascompleted=true
                    }
                }
            }
            Component.onCompleted: {
                if(!tempO.fields){
                    tempO.fields=new Array
                    while(tempO.fields.length>tempO.dbFields)tempO.fields.pop()
                    while(tempO.fields.length<tempO.dbFields)tempO.fields.push("")
                }

            }
        }
        function resetFields(){
            flickFieldsRepeater.model=[]
            for(var c in flickFieldsRow.children) c.parent=undefined
            flickFieldsRepeater.model=tempO.dbFields
        }
    }
}
