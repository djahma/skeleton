import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import "../../../mainUI/qml"
import "../../../mainUI/Themes"
import GoExtensions 1.0

SkTab{
    title: "skelDB"
    TabView{
        id:tabview
        anchors.fill:parent
        property var confObj
        property var schemaObj
        property var importObj
        Tab{
            id: groupMongoDB
            title:"MongoDB configuration"
            clip: true
            Column{
                anchors.left: parent.left
                anchors.right: parent.right
                RowLayout{
                    id:row1
                    anchors.left: parent.left
                    anchors.right: parent.right
                    Label{text:"Connection string:"}
                    TextField{id:txtConn;text:tabview.confObj.connStr;Layout.fillWidth: true}
                    Button{
                        id: btnConn
                        property bool success
                        property string tooltipTxt: "Attempt to connect to MongoDB with given string\n"+
                                                    "New connection replaces old one on success"
                        text:"Connect"
                        tooltip: {
                            if(success)tooltipTxt+"\nCurrent Status: Connected"
                            else tooltipTxt+"\nCurrent Status: Disconnected"
                        }
                        iconName: {
                            if(success) "connect_established"
                            else "connect_no"
                        }
                        onClicked: {
                            goobj.callrpc("skelDB","SkelDB.Connect",txtConn.text)
                            btnConn.success=JSON.parse(goobj.callrpc("skelDB","SkelDB.IsConnected",""))
                        }
                    }
                    Button{
                        text:"Save"
                        iconName: "stock_save"
                        onClicked: {
                            var filepath=goobj.pluginDir("skelDB")+"/skelDBConf.json"
                            goobj.fileContent("write",filepath,JSON.stringify(tabview.confObj,null,"\t"))
                        }
                    }
                    Component.onCompleted: {
                        //  general skelDB config
                        var filepath=goobj.pluginDir("skelDB")+"/skelDBConf.json"
                        var content=goobj.fileContent("read",filepath,"")
                        tabview.confObj=JSON.parse(content)
                        //FIX: There is some kind of race with goobj.callrpc causing skelDB to crash
                        btnConn.success=JSON.parse(goobj.callrpc("skelDB","SkelDB.IsConnected",""))
                    }
                }
            }
        }
        Tab{
            id: groupSchema
            title:"DB Schemas"
            clip: true
            property var tempO
            Flickable{
                id:flick2
                anchors.fill:parent
                anchors.margins: Theme.control.button.radius
                contentHeight: contentItem.childrenRect.height
                property var schemaObj
                Column{
                    anchors.left: parent.left
                    anchors.right: parent.right
                    spacing: Theme.control.button.radius
                    RowLayout{
                        anchors.left: parent.left
                        anchors.right: parent.right
                        Label{text:"Use:"}
                        ComboBox{
                            id: comboSchema
                            Layout.fillWidth: true
                        }
                        Button{
                            iconName: "gtk-remove"
                            tooltip: "Delete selected schema"
                            onClicked: {
                                for(var i=0;i<flick2.schemaObj.length;i++){
                                    if(comboSchema.currentText===flick2.schemaObj[i].schemaName){
                                        flick2.schemaObj.splice(i,1)
                                        var filepath=goobj.pluginDir("skelDB")+"/config/DBSchemas.json"
                                        goobj.fileContent("write",filepath,JSON.stringify(flick2.schemaObj,null,"\t"))
                                        break
                                    }
                                }
                                var schemaArr=new Array
                                for(i=0;i<flick2.schemaObj.length;i++)
                                    schemaArr.push(flick2.schemaObj[i].schemaName)
                                comboSchema.model=schemaArr
                            }
                        }
                        Button{
                            iconName: "add"
                            tooltip: "Create new schema from scratch"
                            onClicked: {
                                groupSchema.tempO=new Object
                                loaderSchema.sourceComponent=compLoaderSchema
                                loaderSchema.visible=true
                            }
                        }
                        Button{
                            iconName: "gtk-edit"
                            tooltip: "Edit selected schema"
                            onClicked: {
                                var filepath=goobj.pluginDir("skelDB")+"/config/DBSchemas.json"
                                var content=goobj.fileContent("read",filepath,"")
                                flick2.schemaObj=JSON.parse(content)
                                for(var i=0;i<flick2.schemaObj.length;i++){
                                    if(comboSchema.currentText===flick2.schemaObj[i].schemaName){
                                        groupSchema.tempO=flick2.schemaObj[i]
                                        loaderSchema.sourceComponent=compLoaderSchema
                                        loaderSchema.visible=true
                                        break
                                    }
                                }
                            }
                        }
                    }
                    Loader{
                        id: loaderSchema
                        anchors.left: parent.left
                        anchors.right: parent.right
                        onChildrenRectChanged: {
                            console.log("ChildrenRect.height=", childrenRect.height)
                            console.log("item.height=", item.height)
                        }
                    }
                    onVisibleChanged: {
                        if(visible) displayDBSchemas()
                    }
                    Component.onCompleted: displayDBSchemas()

                    function displayDBSchemas(){
                        //  schema obj
                        var filepath=goobj.pluginDir("skelDB")+"/config/DBSchemas.json"
                        var content=goobj.fileContent("read",filepath,"")
                        flick2.schemaObj=JSON.parse(content)
                        var schemaArr=new Array
                        for(var i=0;i<flick2.schemaObj.length;i++)
                            schemaArr.push(flick2.schemaObj[i].schemaName)
                        comboSchema.model=schemaArr
                    }

                    Component{
                        id: compLoaderSchema
                        Item{
                            anchors.left: if(parent!==null)parent.left
                            anchors.right: if(parent!==null)parent.right
                            height: childrenRect.height
                        Column{
                            id: columnCompLoaderSchemas
                            anchors.left: parent.left
                            anchors.right: parent.right
                            height: childrenRect.height
                            spacing: Theme.control.button.radius*2
                            Rectangle{
                                border.width: Theme.border.width.thin
                                border.color: Theme.border.color.focus
                                anchors.left: parent.left
                                anchors.right: parent.right
                                height: 2
                            }
                            RowLayout{
                                anchors.left: parent.left
                                anchors.right: parent.right
                                Label{text:"Schema Name:"}
                                TextField{
                                    placeholderText: "enter schema name"
                                    onTextChanged: groupSchema.tempO.schemaName=text
                                    Layout.fillWidth: true
                                    Component.onCompleted: if(groupSchema.tempO.schemaName!==undefined)
                                                               text=groupSchema.tempO.schemaName
                                }
                                Button{
                                    text:"Cancel"
                                    tooltip: "Scrap whatever you are doing on schemas: Nothing done!"
                                    onClicked: {
                                        loaderSchema.visible=false
                                        loaderSchema.sourceComponent=undefined
//                                        groupSchema.tempO=undefined
                                    }
                                }
                                Button{
                                    text:"Save Schema"
                                    tooltip: "Schema will be saved to file './config/DBSchemas.json'"
                                    onClicked:{
                                        for(var i=0;i<repeatCollections.model.length;i++){
                                            repeatCollections.itemAt(i).updateSubFieldObj()
                                        }
                                        var replace=false
                                        for(i=0;i<flick2.schemaObj.length;i++){
                                            if(flick2.schemaObj[i].schemaName===groupSchema.tempO.schemaName){
                                                flick2.schemaObj[i]=groupSchema.tempO
                                                replace=true
                                                break
                                            }
                                        }
                                        if(!replace)flick2.schemaObj.push(groupSchema.tempO)
                                        var filepath=goobj.pluginDir("skelDB")+"/config/DBSchemas.json"
                                        goobj.fileContent("write",filepath,JSON.stringify(flick2.schemaObj,null,"\t"))
                                        loaderSchema.visible=false
                                        loaderSchema.sourceComponent=undefined
                                        var schemaArr=new Array
                                        for(i=0;i<flick2.schemaObj.length;i++)
                                            schemaArr.push(flick2.schemaObj[i].schemaName)
                                        comboSchema.model=schemaArr
                                    }
                                }
                            }
                            RowLayout{
                                anchors.left: parent.left
                                anchors.right: parent.right
                                Label{text:"Database Name:"}
                                TextField{
                                    placeholderText: "enter db name"
                                    onTextChanged: groupSchema.tempO.dbname=text
                                    Layout.fillWidth: true
                                    Component.onCompleted: if(groupSchema.tempO.dbname!==undefined)
                                                               text=groupSchema.tempO.dbname
                                }
                                Button{
                                    text:"Add Collection"
                                    tooltip: "Define a new collection in this database"
                                    onClicked: {
                                        if(groupSchema.tempO.collections===undefined)
                                            groupSchema.tempO.collections=new Array
                                        groupSchema.tempO.collections.push(new Object)
                                        repeatCollections.model=groupSchema.tempO.collections
                                    }
                                }
                            }
                            Repeater{
                                id:repeatCollections
                                Layout.fillWidth: true
                                model:groupSchema.tempO.collections
                                delegate: compLoaderSchemaCollection
                            }
                            Component{
                                id: compLoaderSchemaCollection
                                Rectangle{
                                    id: collectionItem
                                    border.color: Theme.border.color.light
                                    border.width: Theme.border.width.thin
                                    radius: Theme.control.button.radius
                                    color: Theme.control.color.backgroundActive
                                    anchors.left: if(parent!==null)parent.left
                                    anchors.right: if(parent!==null)parent.right
                                    height: childrenRect.height+Theme.control.button.radius*2
                                    property var colSubFields: colSubFields
                                    ColumnLayout{
                                        anchors.topMargin: Theme.control.button.radius
                                        anchors.leftMargin: Theme.control.button.radius
                                        anchors.bottomMargin: Theme.control.button.radius
                                        anchors.left: if(parent!==null)parent.left
                                        anchors.right: if(parent!==null)parent.right
                                        anchors.top: if(parent!==null)parent.top
                                        RowLayout{
                                            anchors.left:parent.left
                                            anchors.right: parent.right
                                            Layout.maximumWidth: parent.width-Theme.control.button.radius
                                            Label{text:"Collection Name:"}
                                            TextField{
                                                onTextChanged: groupSchema.tempO.collections[index].collname=text
                                                Layout.fillWidth: true
                                                Component.onCompleted: if(modelData.collname!==undefined)text=modelData.collname
                                            }
                                            Button{
                                                text:"Remove"
                                                tooltip: "Scrap that collection from database"
                                                onClicked: {
                                                    groupSchema.tempO.collections.splice(index,1)
                                                    repeatCollections.model=groupSchema.tempO.collections
                                                }
                                            }
                                        }
                                        RowLayout{
                                            anchors.left:parent.left
                                            anchors.right: parent.right
                                            Layout.maximumWidth: parent.width-Theme.control.button.radius
                                            Label{text:"Fields:"}
                                            TextField{
                                                id: txtFields
                                                placeholderText: "comma separated list of fields"
                                                Layout.fillWidth: true
                                                onTextChanged: {
                                                    for(var i=0;i<colSubFields.children.length;i++){
                                                        colSubFields.children[i].updateComboOnSubField(text.split(","))
                                                    }
//                                                    updateSubFieldObj()
                                                }
                                                Component.onCompleted: {
                                                    if(groupSchema.tempO.collections[index].fields!==undefined){
                                                       var fieldArr=Object.getOwnPropertyNames(groupSchema.tempO.collections[index].fields)
                                                       text=fieldArr.join(",")
                                                        for(var i=0;i<fieldArr.length;i++){
                                                            if(groupSchema.tempO.collections[index].fields[fieldArr[i]]){
                                                                var comp=Qt.createComponent("subfieldComponent.qml")
                                                                var obj=comp.createObject(colSubFields,
                                                                                          {   "parentObj": collectionItem,
                                                                                              "selectedTxt": fieldArr[i]})
                                                            }
                                                        }
                                                    }
                                                }   //End of TextField Component.onCompleted
                                            }
                                            Button{
                                                text: "Add Subfield"
                                                tooltip: "Define a subobject on a given field"
                                                onClicked: {
                                                    updateSubFieldObj()
                                                    var comp=Qt.createComponent("subfieldComponent.qml")
                                                    var obj=comp.createObject(colSubFields,
                                                                              {   "parentObj": collectionItem,
                                                                                  "selectedTxt": ""})
                                                }
                                            }
                                        }
                                        RowLayout{
                                            anchors.left:parent.left
                                            anchors.right: parent.right
                                            Column{
                                                id: colSubFields
                                                Layout.fillWidth: true
                                                spacing: Theme.control.button.radius
                                            }
                                        }
                                    }
                                    function getFields(){
                                        var fieldArr = Object.getOwnPropertyNames(groupSchema.tempO.collections[index].fields)
                                        return fieldArr
                                    }
                                    function getFieldObj(name){
                                        if(groupSchema.tempO.collections[index].fields[name]){
                                            return groupSchema.tempO.collections[index].fields[name]
                                        }
                                        return null
                                    }
                                    function removeSubFieldAt(position){
                                        colSubFields.children[position].parent = null
                                        builSubfieldObj()
                                    }
                                    function builSubfieldObj(){
                                        var sf = txtFields.text.split(",")
                                        var fObj = new Object
                                        for(var i=0; i<sf.length; i++){
                                            fObj[sf[i]]=null
                                        }
                                        for(i=0; i<colSubFields.children.length; i++){
                                            var curTxt = colSubFields.children[i].comboOnSubField.currentText
                                            fObj[curTxt]=colSubFields.children[i].builSubfieldObj()
                                        }
                                        groupSchema.tempO.collections[index].fields=fObj
                                        return fObj
                                    }
                                    function updateSubFieldObj(){
                                        builSubfieldObj()
                                    }
                                }
                            }   //End of Component compLoaderSchemaCollection
                        }
                        }   //End of Item{}
                    }   //End of Component compLoaderSchema
                }
            }
        }
        Tab{
            id:groupImport
            title:"Import"
            clip: true
            Flickable{
                id: flickImport
                anchors.fill: parent
                anchors.margins: Theme.control.button.radius
                contentHeight: contentItem.childrenRect.height
                Column{
                    anchors.left: parent.left
                    anchors.right: parent.right
                    spacing: Theme.control.button.radius
                    //---Importer UserInterface using file ./config/def_importer.json
                    RowLayout{
                        anchors.left: parent.left
                        anchors.right: parent.right
                        ColumnLayout{
                            Layout.fillWidth: true
                            RowLayout{
                                id:g3row1
                                Label{text:"Import data:"}
                                TextField{
                                    id: txtImportsrc
                                    Layout.fillWidth: true
                                    placeholderText: "select files | comma separated data"
                                }
                                Button{
                                    text: "Select..."
                                    iconName: "stock_open"
                                    onClicked: {
                                        fileDialog.open()
                                    }
                                }
                                FileDialog{
                                    id: fileDialog
                                    selectMultiple: true
                                    title: "Please choose a file"
                                    onAccepted: {
                                        console.log("You chose: " + fileDialog.fileUrls.length+" files")
                                        txtImportsrc.text=""
                                        for(var i=0;i<fileDialog.fileUrls.length;i++){
                                            if(i>0)txtImportsrc.text+=","
                                            txtImportsrc.text+=fileDialog.fileUrls[i]
                                        }
                                        fileDialog.close()
                                    }
                                    onRejected: {
                                        console.log("Canceled")
                                        fileDialog.close()
                                    }
                                }
                            }
                            RowLayout{
                                id:g3row2
                                Label{text:"Use:"}
                                ComboBox{
                                    id: comboImport
                                    Layout.fillWidth: true
                                }
                            }
                        }
                        Button{
                            text: "Import"
                            anchors.top:parent.top
                            anchors.bottom: parent.bottom
                        }
                    }
                    Rectangle{
                        border.width: Theme.border.width.thin
                        border.color: Theme.border.color.focus
                        anchors.left: parent.left
                        anchors.right: parent.right
                        height: 2
                    }
                    //---Data Source definition, UserInterface using file ./config/def_datasource.json
                    Column{
                        id: colDataSource
                        anchors.left: parent.left
                        anchors.right: parent.right
                        property var datasourceObj
                        RowLayout{
                            Label{text:"Data Source definition:"}
                        }
                        RowLayout{
                            anchors.left: parent.left
                            anchors.right: parent.right
                            Label{text:"Use:"}
                            ComboBox{
                                id: combodatasource
                                Layout.fillWidth: true
                            }
                            Button{
                                iconName: "gtk-remove"
                                tooltip: "Delete selected data source definition"
                                onClicked: {
                                    for(var i=0;i<colDataSource.datasourceObj.length;i++){
                                        if(combodatasource.currentText===colDataSource.datasourceObj[i].dataSourceName){
                                            colDataSource.datasourceObj.splice(i,1)
                                            var filepath=goobj.pluginDir("skelDB")+"/config/def_datasource.json"
                                            goobj.fileContent("write",filepath,JSON.stringify(colDataSource.datasourceObj,null,"\t"))
                                            break
                                        }
                                    }
                                    var sourceArr=new Array
                                    for(i=0;i<colDataSource.datasourceObj.length;i++)
                                        sourceArr.push(colDataSource.datasourceObj[i].dataSourceName)
                                    combodatasource.model=sourceArr
                                }
                            }
                            Button{
                                iconName: "add"
                                tooltip: "Create new data source definition from scratch"
                                onClicked: {
                                    loaderDataSource.tempO=new Object
                                    loaderDataSource.sourceComponent=Qt.createComponent("./datasourceComponent.qml")
                                    loaderDataSource.visible=true
                                }
                            }
                            Button{
                                iconName: "gtk-edit"
                                tooltip: "Edit selected data source definition"
                                onClicked: {
                                    var found=false
                                    colDataSource.loadDataSourceObj()
                                    for(var i=0; i<colDataSource.datasourceObj.length; i++){
                                        if(colDataSource.datasourceObj[i].dataSourceName===combodatasource.currentText){
                                            loaderDataSource.tempO=colDataSource.datasourceObj[i]
                                            found=true
                                            break
                                        }
                                    }
                                    if(!found)loaderDataSource.tempO=new Object
                                    loaderDataSource.sourceComponent=Qt.createComponent("./datasourceComponent.qml")
                                    loaderDataSource.visible=true
                                }
                            }
                        }
                        Loader{
                            id: loaderDataSource
                            anchors.left: parent.left
                            anchors.right: parent.right
                            property var tempO
                            property bool save: false
                            onTempOChanged: {
                                if(typeof(tempO)==="undefined"){
                                    loaderDataSource.sourceComponent=undefined
                                    loaderDataSource.visible=false
                                }
                            }
                            onSaveChanged: {
                                console.log("onSaveChanged=",save)
                                if(save===true){
                                    var replace = false
                                    for(var i=0; i<colDataSource.datasourceObj.length; i++){
                                        if(colDataSource.datasourceObj[i].dataSourceName===tempO.dataSourceName){
                                            replace = true
                                            console.log("Saving tempO to colDataSource.datasourceObj[",i,"]")
                                            colDataSource.datasourceObj[i] = tempO
                                        }
                                    }
                                    if(!replace)colDataSource.datasourceObj.push(tempO)
                                    var filepath=goobj.pluginDir("skelDB")+"/config/def_datasource.json"
                                    goobj.fileContent("write",filepath,JSON.stringify(colDataSource.datasourceObj,null,"\t"))
                                    console.log("onSavedChanged, just after 'write' and before loaderDataSource undefined")
                                    loaderDataSource.sourceComponent=undefined
                                    loaderDataSource.visible=false
                                    save=false
                                    colDataSource.loadDataSourceObj()
                                    var schemaArr=new Array
                                    for(i=0;i<colDataSource.datasourceObj.length;i++)
                                        schemaArr.push(colDataSource.datasourceObj[i].dataSourceName)
                                    schemaArr.sort()
                                    combodatasource.model=schemaArr
                                }
                            }
                        }
                        Component.onCompleted: {
                            loadDataSourceObj()
                            var schemaArr=new Array
                            for(var i=0;i<datasourceObj.length;i++)
                                schemaArr.push(datasourceObj[i].dataSourceName)
                            schemaArr.sort()
                            combodatasource.model=schemaArr
                        }
                        function loadDataSourceObj(){
                            var filepath=goobj.pluginDir("skelDB")+"/config/def_datasource.json"
                            var content=goobj.fileContent("read",filepath,"")
                            datasourceObj=JSON.parse(content)
                        }
                    }
                    Rectangle{
                        border.width: Theme.border.width.thin
                        border.color: Theme.border.color.light
                        anchors.left: parent.left
                        anchors.right: parent.right
                        height: 2
                    }
                    //---Data Processing definition, UserInterface using file ./config/def_processing.json
                    ColumnLayout{
                        id: colDataProcessing
                        anchors.left: parent.left
                        anchors.right: parent.right
                        property var dataProcessingObj
                        RowLayout{
                            Label{text:"Data Processing definition:"}
                        }
                        RowLayout{
                            anchors.left: parent.left
                            anchors.right: parent.right
                            Label{text:"Use:"}
                            ComboBox{
                                id: comboprocessing
                                Layout.fillWidth: true
                            }
                            Button{
                                iconName: "gtk-remove"
                                tooltip: "Delete selected processing definition"
                                onClicked: {
                                    for(var i=0;i<colDataProcessing.dataProcessingObj.length;i++){
                                        if(comboprocessing.currentText===colDataProcessing.dataProcessingObj[i].dataProcessingName){
                                            colDataProcessing.dataProcessingObj.splice(i,1)
                                            var filepath=goobj.pluginDir("skelDB")+"/config/def_processing.json"
                                            goobj.fileContent("write",filepath,JSON.stringify(colDataProcessing.dataProcessingObj,null,"\t"))
                                            break
                                        }
                                    }
                                    var processingArr=new Array
                                    for(i=0;i<colDataProcessing.dataProcessingObj.length;i++)
                                        processingArr.push(colDataProcessing.dataProcessingObj[i].dataProcessingName)
                                    comboprocessing.model=processingArr
                                }
                            }
                            Button{
                                iconName: "add"
                                tooltip: "Create new processing definition from scratch"
                                onClicked: {
                                    loaderDataProcessing.tempO=new Object
                                    loaderDataProcessing.sourceComponent=Qt.createComponent("./dataprocessingComponent.qml")
                                    loaderDataProcessing.visible=true
                                }
                            }
                            Button{
                                iconName: "gtk-edit"
                                tooltip: "Edit selected processing definition"
                                onClicked: {
                                    var found=false
                                    colDataProcessing.loaderDataProcessingObj()
                                    for(var i=0; i<colDataProcessing.dataProcessingObj.length; i++){
                                        if(colDataProcessing.dataProcessingObj[i].dataProcessingName===comboprocessing.currentText){
                                            loaderDataProcessing.tempO=colDataProcessing.dataProcessingObj[i]
                                            found=true
                                            break
                                        }
                                    }
                                    if(!found)loaderDataProcessing.tempO=new Object
                                    loaderDataProcessing.sourceComponent=Qt.createComponent("./dataprocessingComponent.qml")
                                    loaderDataProcessing.visible=true
                                }
                            }
                        }
                        Loader{
                            id: loaderDataProcessing
                            anchors.left: parent.left
                            anchors.right: parent.right
                            property var tempO
                            property bool save: false
                            onTempOChanged: {
                                if(typeof(tempO)==="undefined"){
                                    loaderDataProcessing.sourceComponent=undefined
                                    loaderDataProcessing.visible=false
                                }
                            }
                            onSaveChanged: {
                                console.log("onSaveChanged=",save)
                                if(save===true){
                                    var replace = false
                                    for(var i=0; i<colDataProcessing.dataProcessingObj.length; i++){
                                        if(colDataProcessing.dataProcessingObj[i].dataProcessingName===tempO.dataProcessingName){
                                            replace = true
                                            colDataProcessing.dataProcessingObj[i] = tempO
                                        }
                                    }
                                    if(!replace)colDataProcessing.dataProcessingObj.push(tempO)
                                    var filepath=goobj.pluginDir("skelDB")+"/config/def_processing.json"
                                    goobj.fileContent("write",filepath,JSON.stringify(colDataProcessing.dataProcessingObj,null,"\t"))
                                    loaderDataProcessing.sourceComponent=undefined
                                    loaderDataProcessing.visible=false
                                    save=false
                                    colDataProcessing.loaderDataProcessingObj()
                                    var schemaArr=new Array
                                    for(i=0;i<colDataProcessing.dataProcessingObj.length;i++)
                                        schemaArr.push(colDataProcessing.dataProcessingObj[i].dataProcessingName)
                                    schemaArr.sort()
                                    comboprocessing.model=schemaArr
                                }
                            }
                        }
                        Component.onCompleted: {
                            loaderDataProcessingObj()
                            var schemaArr=new Array
                            for(var i=0;i<dataProcessingObj.length;i++)
                                schemaArr.push(dataProcessingObj[i].dataProcessingName)
                            schemaArr.sort()
                            comboprocessing.model=schemaArr
                        }
                        function loaderDataProcessingObj(){
                            var filepath=goobj.pluginDir("skelDB")+"/config/def_processing.json"
                            var content=goobj.fileContent("read",filepath,"")
                            dataProcessingObj=JSON.parse(content)
                        }
                    }
                    Rectangle{
                        border.width: Theme.border.width.thin
                        border.color: Theme.border.color.light
                        anchors.left: parent.left
                        anchors.right: parent.right
                        height: 2
                    }
                    //---Database Operation definition, UserInterface using file ./config/def_dboperation.json
                    ColumnLayout{
                        id: colDataOperation
                        anchors.left: parent.left
                        anchors.right: parent.right
                        property var dataOperationObj
                        RowLayout{
                            Label{text:"Data Operation definition:"}
                            }
                        RowLayout{
                            anchors.left: parent.left
                            anchors.right: parent.right
                            Label{text:"Use:"}
                            ComboBox{
                                id: combodboperation
                                Layout.fillWidth: true
                            }
                            Button{
                                iconName: "gtk-remove"
                                tooltip: "Delete selected data operation definition"
                                onClicked: {
                                    for(var i=0;i<colDataOperation.dataOperationObj.length;i++){
                                        if(combodboperation.currentText===colDataOperation.dataOperationObj[i].dataOperationName){
                                            colDataOperation.dataOperationObj.splice(i,1)
                                            var filepath=goobj.pluginDir("skelDB")+"/config/def_dboperation.json"
                                            goobj.fileContent("write",filepath,JSON.stringify(colDataOperation.dataOperationObj,null,"\t"))
                                            break
                                        }
                                    }
                                    var operationArr=new Array
                                    for(i=0;i<colDataOperation.dataOperationObj.length;i++)
                                        operationArr.push(colDataOperation.dataOperationObj[i].dataOperationName)
                                    combodboperation.model=operationArr
                                }
                            }
                            Button{
                                iconName: "add"
                                tooltip: "Create new data operation definition from scratch"
                                onClicked: {
                                    loaderDataOperation.tempO=new Object
                                    loaderDataOperation.sourceComponent=Qt.createComponent("./dataoperationComponent.qml")
                                    loaderDataOperation.visible=true
                                }
                            }
                            Button{
                                iconName: "gtk-edit"
                                tooltip: "Edit selected data operation definition"
                                onClicked: {
                                    var found=false
                                    colDataOperation.loadDataOperationObj()
                                    for(var i=0; i<colDataOperation.dataOperationObj.length; i++){
                                        if(colDataOperation.dataOperationObj[i].dataOperationName===combodboperation.currentText){
                                            loaderDataOperation.tempO=colDataOperation.dataOperationObj[i]
                                            found=true
                                            break
                                        }
                                    }
                                    if(!found)loaderDataOperation.tempO=new Object
                                    loaderDataOperation.sourceComponent=Qt.createComponent("./dataoperationComponent.qml")
                                    loaderDataOperation.visible=true
                                }
                            }
                        }
                        Loader{
                            id: loaderDataOperation
                            anchors.left: parent.left
                            anchors.right: parent.right
                            property var tempO
                            property bool save: false
                            onTempOChanged: {
                                if(typeof(tempO)==="undefined"){
                                    loaderDataOperation.sourceComponent=undefined
                                    loaderDataOperation.visible=false
                                }
                            }
                            onSaveChanged: {
                                console.log("loaderDataOperation onSaveChanged=",save)
                                if(save===true){
                                    var replace = false
                                    for(var i=0; i<colDataOperation.dataOperationObj.length; i++){
                                        if(colDataOperation.dataOperationObj[i].dataOperationName===tempO.dataOperationName){
                                            replace = true
                                            colDataOperation.dataOperationObj[i] = tempO
                                        }
                                    }
                                    if(!replace)colDataOperation.dataOperationObj.push(tempO)
                                    var filepath=goobj.pluginDir("skelDB")+"/config/def_dboperation.json"
                                    goobj.fileContent("write",filepath,JSON.stringify(colDataOperation.dataOperationObj,null,"\t"))
                                    loaderDataOperation.sourceComponent=undefined
                                    loaderDataOperation.visible=false
                                    save=false
                                    colDataOperation.loadDataOperationObj()
                                    var operationArr=new Array
                                    for(i=0;i<colDataOperation.dataOperationObj.length;i++)
                                        operationArr.push(colDataOperation.dataOperationObj[i].dataOperationName)
                                    operationArr.sort()
                                    combodboperation.model=operationArr
                                }
                            }
                        }
                        Component.onCompleted: {
                            loadDataOperationObj()
                            var operationArr=new Array
                            for(var i=0;i<dataOperationObj.length;i++)
                                operationArr.push(dataOperationObj[i].dataOperationName)
                            operationArr.sort()
                            combodboperation.model=operationArr
                        }
                        function loadDataOperationObj(){
                            var filepath=goobj.pluginDir("skelDB")+"/config/def_dboperation.json"
                            var content=goobj.fileContent("read",filepath,"")
                            dataOperationObj=JSON.parse(content)
                        }
                    }
                    Rectangle{
                        border.width: Theme.border.width.thin
                        border.color: Theme.border.color.focus
                        anchors.left: parent.left
                        anchors.right: parent.right
                        height: 2
                    }
                    //---Importer definition, UserInterface using file ./config/def_importer.json
                    //---Importers show up in the import dialog at top of UserInterface
                    ColumnLayout{
                        id: colImporter
                        anchors.left: parent.left
                        anchors.right: parent.right
                        property var dataImporterObj
                        RowLayout{
                            Label{text:"Importer definition:"}
                        }
                        RowLayout{
                            anchors.left: parent.left
                            anchors.right: parent.right
                            Label{text:"Use:"}
                            ComboBox{
                                id: comboimporter
                                Layout.fillWidth: true
                            }
                            Button{
                                iconName: "gtk-remove"
                                tooltip: "Delete selected importer definition"
                                onClicked: {
                                    for(var i=0;i<colImporter.dataImporterObj.length;i++){
                                        if(comboimporter.currentText===colImporter.dataImporterObj[i].dataImporterName){
                                            colImporter.dataImporterObj.splice(i,1)
                                            var filepath=goobj.pluginDir("skelDB")+"/config/def_importer.json"
                                            goobj.fileContent("write",filepath,JSON.stringify(colImporter.dataImporterObj,null,"\t"))
                                            break
                                        }
                                    }
                                    var importerArr=new Array
                                    for(i=0;i<colImporter.dataImporterObj.length;i++)
                                        importerArr.push(colImporter.dataImporterObj[i].dataImporterName)
                                    comboimporter.model=importerArr
                                }
                            }
                            Button{
                                iconName: "add"
                                tooltip: "Create new importer definition from scratch"
                                onClicked: {
                                    loaderImporter.tempO=new Object
                                    loaderImporter.sourceComponent=Qt.createComponent("./dataimporterComponent.qml")
                                    loaderImporter.visible=true
                                }
                            }
                            Button{
                                iconName: "gtk-edit"
                                tooltip: "Edit selected importer definition"
                                onClicked: {
                                    var found=false
                                    colImporter.loadDataImporterObj()
                                    for(var i=0; i<colImporter.dataImporterObj.length; i++){
                                        if(colImporter.dataImporterObj[i].dataImporterName===comboimporter.currentText){
                                            loaderImporter.tempO=colImporter.dataImporterObj[i]
                                            found=true
                                            break
                                        }
                                    }
                                    if(!found)loaderImporter.tempO=new Object
                                    loaderImporter.sourceComponent=Qt.createComponent("./dataimporterComponent.qml")
                                    loaderImporter.visible=true
                                }
                            }
                        }
                        Loader{
                            id: loaderImporter
                            anchors.left: parent.left
                            anchors.right: parent.right
                            property var tempO
                            property bool save: false
                            onTempOChanged: {
                                if(typeof(tempO)==="undefined"){
                                    loaderImporter.sourceComponent=undefined
                                    loaderImporter.visible=false
                                }
                            }
                            onSaveChanged: {
                                console.log("loaderImporter onSaveChanged=",save)
                                if(save===true){
                                    var replace = false
                                    for(var i=0; i<colImporter.dataImporterObj.length; i++){
                                        if(colImporter.dataImporterObj[i].dataImporterName===tempO.dataImporterName){
                                            replace = true
                                            colImporter.dataImporterObj[i] = tempO
                                        }
                                    }
                                    if(!replace)colImporter.dataImporterObj.push(tempO)
                                    var filepath=goobj.pluginDir("skelDB")+"/config/def_importer.json"
                                    goobj.fileContent("write",filepath,JSON.stringify(colImporter.dataImporterObj,null,"\t"))
                                    loaderImporter.sourceComponent=undefined
                                    loaderImporter.visible=false
                                    save=false
                                    colImporter.loadDataImporterObj()
                                    var importerArr=new Array
                                    for(i=0;i<colImporter.dataImporterObj.length;i++)
                                        importerArr.push(colImporter.dataImporterObj[i].dataImporterName)
                                    importerArr.sort()
                                    comboimporter.model=importerArr
                                }
                            }
                        }
                        Component.onCompleted: {
                            loadDataImporterObj()
                            var importerArr=new Array
                            for(var i=0;i<dataImporterObj.length;i++)
                                importerArr.push(dataImporterObj[i].dataImporterName)
                            importerArr.sort()
                            comboimporter.model=importerArr
                        }
                        function loadDataImporterObj(){
                            var filepath=goobj.pluginDir("skelDB")+"/config/def_importer.json"
                            var content=goobj.fileContent("read",filepath,"")
                            dataImporterObj=JSON.parse(content)
                        }
                    }                    
                }

                Component.onCompleted: {
                    updateComboImport()
                }
                function updateComboImport(){
                    var modelArr=loadImportModel("textbox")
                    comboImport.model=modelArr
                }

                function loadImportModel(source){
                    var i,j,k,importersArr,sourceName,sourceObj
                    var filepath=goobj.pluginDir("skelDB")+"/config/def_importer.json"
                    var content=goobj.fileContent("read",filepath,"")
                    tabview.importObj=JSON.parse(content)
                    var importArr=new Array
                    filepath=goobj.pluginDir("skelDB")+"/config/def_datasource.json"
                    content=goobj.fileContent("read",filepath,"")
                    sourceObj=JSON.parse(content)

                    if(source==="all"||""){
                        for(i=0;i<tabview.importObj.length;i++)
                            importArr.push(tabview.importObj[i].dataImporterName)
                    }
                    else {
                        for(i=0;i<tabview.importObj.length;i++){
                            importersArr=tabview.importObj[i].importers
                            for(j=0;j<importersArr.length;j++){
                                sourceName=importersArr[j].dataSourceName
                                for(k=0;k<sourceObj.length;k++){
                                    if(sourceObj[k].dataSourceName===sourceName){
                                        if(source==="textbox" && sourceObj[k].datasource==="Textbox")
                                            importArr.push(tabview.importObj[i].dataImporterName)
                                        else if(source==="db" && sourceObj[k].datasource==="Database")
                                            importArr.push(tabview.importObj[i].dataImporterName)
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    return importArr
                }
            }   // End of Flickable
//            onVisibleChanged:{
//                console.log("Tab is visible?=",visible)
//            }


        }   //End of Import Tab
        Component.onCompleted: currentIndex=2
        GoObj{id:goobj}
    }
}
