import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import "../../../mainUI/qml"
import "../../../mainUI/Themes"
import GoExtensions 1.0

//  The Component is loaded in a loader with a JSON object
//      called "tempO" that represents an import definition.
    ColumnLayout{
        Rectangle{
            border.width: Theme.border.width.thin
            border.color: Theme.border.color.light
            anchors.left: parent.left
            anchors.right: parent.right
            height: 2
        }
        RowLayout{
            Label{text: "Importer Name:"}
            TextField{
                id: txtImportName
                Layout.fillWidth: true
                onTextChanged: tempO.importName=text
            }
            Button{
                id: importCancel
                text: "Cancel"
                tooltip: "Scrap whatever you are doing with importers: Nothing Done!"
                onClicked: {tempO=undefined}
            }
            Button{
                id: importSave
                text: "Save Importer"
                tooltip: "Importer config will be saved to './config/DBImportFormat.json'"
                onClicked: save=true
            }
        }
        RowLayout{
            Label{text: "Source type:"}
            ComboBox{
                id: comboSrcType
                model: ["file","inline","url"]
                onCurrentTextChanged: {
                    if(currentText==="file"||"url")
                        comboDataFormat.model=["text(csv)","inline","JSON"]
                    if(currentText==="inline")
                        comboDataFormat.model=["inline"]
                    tempO.sourceType=comboSrcType.currentText
                }
                Component.onCompleted: tempO.sourceType=""
            }
            Label{text:"Data format:"}
            ComboBox{
                id: comboDataFormat
                onCurrentTextChanged: {
                    if(currentText==="inline"){
                        tempO.skipLines=0
                        tempO.separator=","
                        loaderDesc.sourceComponent=descInline
                        loaderDesc.visible=true
                    }
                    else if(currentText==="text(csv)"){
                        loaderDesc.sourceComponent=descCsv
                        loaderDesc.visible=true
                    }
                    else if(currentText==="JSON"){
                        loaderDesc.sourceComponent=descJSON
                        loaderDesc.visible=true
                    }
                    else loaderDesc.visible=false
                    if(currentText!=="0") {
                        tempO.sourceFormat=currentText
                        }
                    else tempO.sourceFormat=""
                }
            }
        }
        Loader{
            id: loaderDesc
            anchors.left: parent.left
            anchors.right: parent.right
        }
        Component{
            id: descInline
            ColumnLayout{
                RowLayout{
                    Label{text:"Lines to skip:"}
                    SpinBox{
                        decimals: 0
                        minimumValue: 0
                    }
                    Label{text:"Separator:"}
                    TextField{
                        text:","
                        onTextChanged: tempO.separator=text
                    }
                }
                RowLayout{
                    anchors.left: parent.left
                    anchors.right: parent.right
                    Label{text:"Source Column names:"}
                    ListView{
                        id:descColumnsListView
                        implicitHeight: contentItem.childrenRect.height
                        Layout.fillWidth: true
                        clip:true
                        orientation: Qt.Horizontal
                        layoutDirection: Qt.LeftToRight
                        spacing: 5
                        model: 1
                        delegate: descColumnsNames
                        Component.onCompleted: {
                            console.log("Inline Listview complete")
                            tempO.numCols=1
                        }
                    }
                }
            }
        }
        Component{
            id: descCsv
            ColumnLayout{
                RowLayout{
                    Label{text:"Lines to skip:"}
                    SpinBox{
                        decimals: 0
                        minimumValue: 0
                        onValueChanged: tempO.skipLines=value
                    }
                    Label{text:"Separator:"}
                    TextField{
                        text:","
                        onTextChanged: tempO.separator=text
                    }
                    Label{text:"Columns:"}
                    SpinBox{
                        id:spindescColumns
                        minimumValue: 0
                        onValueChanged: {
                            descColumnsListView.model=spindescColumns.value
                            console.log("Spin value=",value)
                            tempO.numCols=value
                        }
                    }
                }
                RowLayout{
                    anchors.left: parent.left
                    anchors.right: parent.right
                    Label{text:"Source Column names:"}
                    ListView{
                        id:descColumnsListView
                        implicitHeight: contentItem.childrenRect.height
                        Layout.fillWidth: true
                        clip:true
                        orientation: Qt.Horizontal
                        layoutDirection: Qt.LeftToRight
                        spacing: 5
                        model: spindescColumns.value
                        delegate: descColumnsNames
                    }
                }
                RowLayout{
                    anchors.left: parent.left
                    anchors.right: parent.right
                    Label{text:"Columns Transformation:"}
                    ListView{
                        id: transformListView
                        implicitHeight: contentItem.childrenRect.height
                        Layout.fillWidth: true
                        clip:true
                        orientation: Qt.Horizontal
                        layoutDirection: Qt.LeftToRight
                        spacing: 5
                        model: spindescColumns.value
                        delegate: transformComp
                    }
                }
            }
        }
        Component{
            id: descJSON
            ColumnLayout{
                RowLayout{
                    Label{text:"Data path:"}
                    TextField{
                        placeholderText: "path,to,data"
                        Layout.fillWidth: true
                    }
                    Label{text:"Data is in:"}
                    ComboBox{
                        id:comboDataisin
                        model:["Object","Array"]
                        onCurrentTextChanged: {
                            if(currentText==="Object")
                                labelDataisin.text="Number of keys:"
                            else labelDataisin.text="Array length:"
                        }
                    }
                    Label{text:"under key:"}
                    TextField{
                        placeholderText: "name of key"
                    }
                }
                RowLayout{
                    Label{id:labelDataisin}
                    SpinBox{
                        id:spindescColumns
                        minimumValue: 0
                        onValueChanged: {
                            descColumnsListView.model=spindescColumns.value
                            console.log("Spin value=",value)
                        }
                    }
                }
                RowLayout{
                    anchors.left: parent.left
                    anchors.right: parent.right
                    Label{text:"Source Column names:"}
                    ListView{
                        id:descColumnsListView
                        implicitHeight: contentItem.childrenRect.height
                        Layout.fillWidth: true
                        clip:true
                        orientation: Qt.Horizontal
                        layoutDirection: Qt.LeftToRight
                        spacing: 5
                        model: spindescColumns.value
                        delegate: descColumnsNames
                    }
                }
                RowLayout{
                    anchors.left: parent.left
                    anchors.right: parent.right
                    Label{text:"Columns Transformation:"}
                    ListView{
                        id: transformListView
                        implicitHeight: contentItem.childrenRect.height
                        Layout.fillWidth: true
                        clip:true
                        orientation: Qt.Horizontal
                        layoutDirection: Qt.LeftToRight
                        spacing: 5
                        model: spindescColumns.value
                        delegate: transformComp
                    }
                }
            }
        }
        Component{
            id: descColumnsNames
            TextField{
                objectName: "textfield_"+index
                placeholderText: "Column"+index+" name."
                onTextChanged: {
                    tempO.colNames=new Array
                    for(var i=0;i<parent.children.length;i++){
                        console.log("Col",i,"=",parent.children[i].objectName.split("_")[0])
                        if(parent.children[i].objectName.split("_")[0]==="textfield"){
                            tempO.colNames.push(parent.children[i].text)
                            //TODO: initialize this array somewhere above
                            tempO.dataTransforms=new Array
                            tempO.dataTransforms.push(new Object)
                        }
                    }
                }
            }
        }
        Component{
            id: transformComp
            ColumnLayout{
                Button{
                    text:"Add Transform"+index
                    onClicked: {
                        console.log(typeof(tempO))
                        //tempO.dataTransform
                    }
                }
                Repeater{
                    model: tempO.dataTransforms
                    delegate: dataTransformsDelegate
                }
                Component{
                    id: dataTransformsDelegate
                    ComboBox{
                        model:["name_transform","string_transform",
                            "num_transform","date_transform","skip_column"]
                    }
                }
            }
        }
    }
