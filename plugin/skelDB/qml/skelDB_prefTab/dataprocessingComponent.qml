import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.3
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import "../../../mainUI/qml"
import "../../../mainUI/Themes"
import GoExtensions 1.0

//  The Component is loaded in a loader with a JSON object
//      called "tempO" that represents a data-processing definition.
Column{
    spacing: Theme.control.button.radius
    Rectangle{
        border.width: Theme.border.width.thin
        border.color: Theme.border.color.light
        anchors.left: parent.left
        anchors.right: parent.right
        height: 2
    }
    //  DataProcessing name and save/cancel buttons
    RowLayout{
        anchors.left: parent.left
        anchors.right: parent.right
        Label{text: "DataProcessingDefinition Name:"}
        TextField{
            Layout.fillWidth: true
            onTextChanged: tempO.dataProcessingName=text
            Component.onCompleted: if(tempO.dataProcessingName)text=tempO.dataProcessingName
        }
        Button{
            text: "Cancel"
            tooltip: "Scrap whatever you are doing with current data processing definition: Nothing Done!"
            onClicked: {tempO=undefined}
        }
        Button{
            text: "Save"
            tooltip: "DataProcessing definition will be saved to './config/def_processing.json'"
            onClicked: save=true
        }
    }
    RowLayout{
        anchors.left: parent.left
        Label{text: "Process incoming data:"}
        ExclusiveGroup { id: useCollectorGroup }
        RadioButton{
            id: radioAsIs
            text: "as is"
            exclusiveGroup: useCollectorGroup
            onCheckedChanged: {
                if(checked){
                    tempO.useCollector=false
                    var arr = Object.getOwnPropertyNames(tempO)
                    for(var p in arr){
                        if(arr[p]!=="useCollector" && arr[p]!=="dataProcessingName")
                            tempO[arr[p]]=undefined
                    }
                }
                else tempO.useCollector=true
            }
        }
        RadioButton{
            text: "through collector"
            exclusiveGroup: useCollectorGroup
            Component.onCompleted: {
                if(tempO.useCollector)checked=true
                else {
                    checked=false
                    radioAsIs.checked=true
                }
            }
        }
    }
    RowLayout{
        visible: radioAsIs.checked===false
        Label{text: "Collector"}
        ComboBox{
            property bool hascompleted: false
            model:["collector 1","collector 2"]
            onCurrentTextChanged: {
                if(hascompleted)tempO.collectorName=currentText
            }
            onVisibleChanged: {
                if(hascompleted){
                    if(visible)tempO.collectorName=currentText
                }
            }
            Component.onCompleted: {
                if(tempO.collectorName){
                    for(var i=0;i<model.length;i++)
                        if(model[i]===tempO.collectorName){currentIndex=i;break;}
                }
                else{
                    tempO.collectorName=model[0]
                }
                hascompleted = true
            }
        }
        Label{text: "Function"}
        ComboBox{
            property bool hascompleted: false
            model: ["function 1","function 2"]
            onCurrentTextChanged: {
                if(hascompleted)tempO.collectorFunction=currentText
            }
            onVisibleChanged: {
                if(hascompleted){
                    if(visible)tempO.collectorFunction=currentText
                }
            }
            Component.onCompleted: {
                if(tempO.collectorFunction){
                    for(var i=0;i<model.length;i++)
                        if(model[i]===tempO.collectorFunction){currentIndex=i;break;}
                }
                else{
                    tempO.collectorFunction=model[0]
                }
                hascompleted = true
            }
        }
    }
    RowLayout{
        visible: radioAsIs.checked===false
        CheckBox{
            id: checkModifyOutput
            property bool hascompleted: false
            text: "Modify output"
            onCheckedChanged: if(hascompleted)tempO.modifyOutput = checked
            onVisibleChanged: {
                if(hascompleted){
                    if(visible)tempO.modifyOutput=checked
                }
            }
            Component.onCompleted: {
                if(tempO.modifyOutput)checked=tempO.modifyOutput
                else tempO.modifyOutput=false
                hascompleted=true
            }
        }
        Label{
            visible: checkModifyOutput.checked
            text: "Number of fields:"
        }
        SkSpinBox{
            id: numFields
            visible: checkModifyOutput.checked
            property bool hascompleted: false
            implicitWidth: 50
            minimumValue: 0
            onValueChanged: {
                if(hascompleted){
                    tempO.numFields=value
                    if(!tempO.numExtraFields)tempO.numExtraFields=0
                    var total = tempO.numExtraFields + tempO.numFields
                    if(!tempO.fieldsOps)tempO.fieldsOps=new Array
                    while(tempO.fieldsOps.length>total)tempO.fieldsOps.pop()
                    while(tempO.fieldsOps.length<total)tempO.fieldsOps.push(new Object)
                    flickOpsRepeater.model=tempO.fieldsOps
                }
            }
            Component.onCompleted: {
                if(tempO.numFields){
                    value=tempO.numFields
                    if(!tempO.numExtraFields)tempO.numExtraFields=0
                    var total = tempO.numExtraFields + tempO.numFields
                    if(!tempO.fieldsOps)tempO.fieldsOps=new Array
                    while(tempO.fieldsOps.length>total)tempO.fieldsOps.pop()
                    while(tempO.fieldsOps.length<total)tempO.fieldsOps.push(new Object)
                    flickOpsRepeater.model=tempO.fieldsOps
                }
                else tempO.numFields=0
                hascompleted = true
            }
        }
    }
    RowLayout{
        visible: checkModifyOutput.checked && radioAsIs.checked===false
        Label{text:"Data path:"}
        TextField{
            placeholderText: "path,to,data,array"
            Layout.fillWidth: true
            property bool hascompleted: false
            onTextChanged: if(hascompleted) tempO.pathToDataArray=text
            onVisibleChanged: {
                if(!checkModifyOutput.hascompleted)return
                if(visible){ if(tempO.pathToDataArray)text=tempO.pathToDataArray}
                else tempO.pathToDataArray=undefined
            }
            Component.onCompleted: {
                if(tempO.pathToDataArray) text=tempO.pathToDataArray
                hascompleted = true
            }
        }
        Label{text:"Data is in:"}
        ComboBox{
            implicitWidth: 80
            property bool hascompleted: false
            model:["Object","Array"]
            onCurrentTextChanged: {
                if(hascompleted)tempO.dataIsIn=currentText
            }
            onVisibleChanged: {
                if(!checkModifyOutput.hascompleted)return
                if(visible){
                    if(tempO.dataIsIn==="Array")currentIndex=1
                    else {currentIndex=0; tempO.dataIsIn=currentText}
                }
                else tempO.dataIsIn=undefined
            }
            Component.onCompleted: {
                if(tempO.dataIsIn==="Array") currentIndex=1
                else currentIndex=0
                hascompleted = true
            }
        }
        Label{
            text:"Add Extra Fields:"
        }
        SkSpinBox{
            id: numExtraFields
            implicitWidth: 50
            property bool hascompleted: false
            minimumValue: 0
            onValueChanged: {
                if(hascompleted){
                    tempO.numExtraFields=value
                    if(!tempO.numFields)tempO.numFields=0
                    var total = tempO.numExtraFields + tempO.numFields
                    if(!tempO.fieldsOps)tempO.fieldsOps=new Array
                    while(tempO.fieldsOps.length>total)tempO.fieldsOps.pop()
                    while(tempO.fieldsOps.length<total)tempO.fieldsOps.push(new Object)
                    flickOpsRepeater.model=tempO.fieldsOps
                }
            }
            onVisibleChanged: {
                if(!checkModifyOutput.hascompleted)return
                var total = tempO.numExtraFields + tempO.numFields
                if(visible){
                    if(tempO.numExtraFields){
                        value=tempO.numExtraFields
                        while(tempO.fieldsOps.length>total)tempO.fieldsOps.pop()
                        while(tempO.fieldsOps.length<total)tempO.fieldsOps.push(new Object)
                        flickOpsRepeater.model=tempO.fieldsOps
                    }
                }
                else {
                    tempO.numExtraFields=0
                    value=0
                    total = tempO.numExtraFields + tempO.numFields
                    while(tempO.fieldsOps.length>total)tempO.fieldsOps.pop()
                    while(tempO.fieldsOps.length<total)tempO.fieldsOps.push(new Object)
                    flickOpsRepeater.model=tempO.fieldsOps
                }
            }
            Component.onCompleted: {
                if(tempO.numExtraFields){
                    value=tempO.numExtraFields
                    if(!tempO.numFields)tempO.numFields=0
                    var total = tempO.numExtraFields + tempO.numFields
                    while(tempO.fieldsOps.length>total)tempO.fieldsOps.pop()
                    while(tempO.fieldsOps.length<total)tempO.fieldsOps.push(new Object)
                    flickOpsRepeater.model=tempO.fieldsOps
                }
                else tempO.numExtraFields=0
                hascompleted = true
            }
        }
    }

    Flickable{
        id: flickOps
        visible: checkModifyOutput.checked && radioAsIs.checked===false
        anchors.left: parent.left
        anchors.right: parent.right
        height: contentItem.childrenRect.height
        contentWidth: contentItem.childrenRect.width
        Row{
            id: flickOpsRow
            spacing: Theme.control.button.radius
            Repeater{
                id: flickOpsRepeater
                model: if(tempO.fieldsOps)tempO.fieldsOps
                delegate: compOps
            }
            Component.onCompleted: {
                if(!tempO.fieldsOps){
                    tempO.fieldsOps=new Array
                    while(tempO.fieldsOps.length>tempO.numFields)tempO.fieldsOps.pop()
                    while(tempO.fieldsOps.length<tempO.numFields)tempO.fieldsOps.push(new Object)
                }

            }
        }
        function resetFields(){
            flickOpsRepeater.model=[]
            for(var c in flickOpsRow.children) c.parent=undefined
            flickOpsRepeater.model=tempO.fieldsOps
        }
    }

    Component{
        id: compOps
        Rectangle{
            id: rectOps
            height: colOps.childrenRect.height+Theme.control.button.radius*2
            width: colOps.childrenRect.width+Theme.control.button.radius*2
            color: "transparent"
            border.width: Theme.border.width.thin
            border.color: Theme.border.color.light
            radius: Theme.control.button.radius
            property int fieldIndex:index
            Column{
                id: colOps
                anchors.fill: parent
                anchors.margins: Theme.control.button.radius
                RowLayout{
                    id: rowOps
                    objectName: "rowOps"
                    Layout.minimumWidth: 233
                    TextField{
                        Layout.fillWidth: true
                        placeholderText: "Field_"+index+"_name"
                        onTextChanged: tempO.fieldsOps[index].name=text
                        Component.onCompleted: {
                            if(tempO.fieldsOps[index].name)text=tempO.fieldsOps[index].name
                            if(!tempO.fieldsOps[index].ops)tempO.fieldsOps[index].ops=new Array
                        }
                    }
                    ComboBox{
                        implicitWidth: 85
                        model:["number","string","date"]
                        onCurrentTextChanged: tempO.fieldsOps[index].type=currentText
                        Component.onCompleted: {
                            if(rectOps.fieldIndex===0)model.push("Object")
                            if(tempO.fieldsOps[index].type){
                                switch(tempO.fieldsOps[index].type){
                                case "number": currentIndex=0; break;
                                case "string": currentIndex=1; break;
                                case "Object": currentIndex=3; break;
                                default: currentIndex=2;
                                }
                            }
                        }
                    }
                    Button{
                        iconName: "add"
                        tooltip: "Add an operation on field"
                        onClicked: {
                            if(!tempO.fieldsOps[index].ops)tempO.fieldsOps[index].ops=new Array
                            repeatOps.model = tempO.fieldsOps[index].ops.push(new Object)
                        }
                    }
                }
                Repeater{
                    id: repeatOps
                    model: {
                        if(tempO)tempO.fieldsOps[index].ops
                        else null
                    }
                    delegate: textboxOpsDelegate
                }
                Component{
                    id: textboxOpsDelegate
                    RowLayout{
                        onWidthChanged: parent.resetWidth()
                        property int opIndex:index
                        Button{
                            anchors.top: parent.top
                            iconName: "remove"
                            tooltip: "Remove this operation on field"
                            onClicked: {
                                colOps.parent.removeOp(index)
                            }
                        }
                        ColumnLayout{
                            anchors.top: parent.top
                            ComboBox{
                                id: comboOpChoice
                                implicitWidth: 150
                                property bool hascompleted: false
                                model:["skip_field","date_description","name_transform"
                                    ,"string_transform","number_transform",
                                "find_in_collection","reduce_dataset"]
                                onCurrentTextChanged: {
                                    if(hascompleted){
                                        tempO.fieldsOps[rectOps.fieldIndex].ops[index].name=currentText
                                        tempO.fieldsOps[rectOps.fieldIndex].ops[index].opVars=new Object
                                        loadOpComp(currentText)
                                    }
                                }
                                Component.onCompleted: {
                                    var op =tempO.fieldsOps[rectOps.fieldIndex].ops[index]
                                    if(op.name){
                                        for(var i=0; i<model.length; i++){
                                            if(model[i]===op.name){ currentIndex=i; break; }
                                        }
                                        if(!op.opVars)op.opVars=new Object
                                    }
                                    else {
                                        op.name=currentText
                                        op.opVars=new Object
                                    }
                                    loadOpComp(op.name)
                                    hascompleted=true
                                }
                                function loadOpComp(opname){
                                    var op =tempO.fieldsOps[rectOps.fieldIndex].ops[index]
                                    if(!op.opVars)op.opVars=new Object
                                    opLoader.visible = true
                                    switch(opname){
                                    case "skip_field":
                                        opLoader.sourceComponent = undefined
                                        opLoader.visible = false
                                        break;
                                    case "date_description":
                                        opLoader.sourceComponent = compDateOp
                                        break;
                                    case "name_transform":
                                        opLoader.sourceComponent = compNewNameOp
                                        break;
                                    case "string_transform":
                                        opLoader.sourceComponent = compStringOp
                                        break;
                                    case "number_transform":
                                        opLoader.sourceComponent = compNumberOp
                                        break;
                                    case "find_in_collection":
                                        opLoader.sourceComponent = compFindInCol
                                        break;
                                    case "reduce_dataset":
                                        opLoader.sourceComponent = compReduce
                                    }
                                }
                            }
                            Label{
                                text: {
                                    switch(comboOpChoice.currentText){
                                    case "skip_field":
                                        "Field will be discarded"
                                        break;
                                    case "date_description":
                                        "Use placeholder letters to\ndescribe your date field"
                                        break;
                                    case "name_transform":
                                        "Change the field name"
                                        break;
                                    case "string_transform":
                                        "Modify string value"
                                        break;
                                    case "number_transform":
                                        "Write equation using 'a' as\nplaceholder for field value"
                                        break;
                                    case "find_in_collection":
                                        "Retrieve data from database"
                                        break;
                                    case "reduce_dataset":
                                        "Discard some retrieved data\nTo be used after find_in_collection"
                                    }
                                }
                                color: Theme.text.color.disabled
                            }
                        }
                        Loader{
                            id: opLoader
                            anchors.baseline: parent.top
                        }
                        Component{
                            id: compNewNameOp
                            TextField{
                                placeholderText: "new field name"
                                onTextChanged: tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.opIndex].opVars.newName=text
                                Component.onCompleted: {
                                    if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.opIndex].opVars.newName)text=tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.opIndex].opVars.newName
                                }
                            }
                        }
                        Component{
                            id: compNumberOp
                            //  Backend replaces char 'a' with field value.toString() and then
                            // runs eval(resultingStr)
                            TextField{
                                placeholderText: "ex: (2*a+8)/100"
                                onTextChanged: tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.opIndex].opVars.numberOp=text
                                Component.onCompleted: {
                                    if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.opIndex].opVars.numberOp)text=tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.opIndex].opVars.numberOp
                                }
                            }
                        }
                        Component{
                            id: compDateOp
                            TextField{
                                implicitWidth: 150
                                placeholderText: "YYYY.MM.DD hh:mm:ss"
                                onTextChanged: tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.opIndex].opVars.dateDescriptor=text
                                Component.onCompleted: {
                                    if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.opIndex].opVars.dateDescriptor)text=tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.opIndex].opVars.dateDescriptor
                                }
                            }
                        }
                        Component{
                            id: compStringOp
                            RowLayout{
                                ComboBox{
                                    id: comboStrMode
                                    property bool hascompleted: false
                                    model:["trim","insert","remove","subString",
                                    "split","replace","toLowerCase","toUpperCase"]
                                    anchors.top: parent.top
                                    onCurrentTextChanged: {
                                        if(hascompleted){
                                            tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.opIndex].opVars.stringMode = currentText
                                            loadStringOp(currentText)
                                        }
                                    }
                                    Component.onCompleted: {
                                        if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.opIndex].opVars.stringMode){
                                            for(var i=0; i<model.length; i++){
                                                if(model[i]===tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.opIndex].opVars.stringMode){
                                                    currentIndex=i; break;
                                                }
                                            }
                                        }
                                        else tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.opIndex].opVars.stringMode = currentText
                                        loadStringOp(currentText)
                                        hascompleted=true
                                    }
                                    function loadStringOp(stringOpMode){
                                        var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.opIndex].opVars
                                        loaderStrMode.visible = true
                                        switch(stringOpMode){
                                        case "trim":
                                            loaderStrMode.sourceComponent = compStringMode_trim
                                            break;
                                        case "insert":
                                            loaderStrMode.sourceComponent = compStringMode_insert
                                            break;
                                        case "remove":
                                            loaderStrMode.sourceComponent = compStringMode_remove
                                            break;
                                        case "subString":
                                            loaderStrMode.sourceComponent = compStringMode_substring
                                            break;
                                        case "split":
                                            loaderStrMode.sourceComponent = compStringMode_split
                                            break;
                                        case "replace":
                                            loaderStrMode.sourceComponent = compStringMode_replace
                                            break;
                                        case "toLowerCase":
                                        case "toUpperCase":
                                            loaderStrMode.sourceComponent = undefined
                                            loaderStrMode.visible = false
                                        }
                                    }
                                }
                                Loader{
                                    id: loaderStrMode
                                }
                                Component{
                                    id: compStringMode_trim
                                    ComboBox{
                                        property bool hascompleted: false
                                        model:["left","right","both"]
                                        onCurrentTextChanged: if(hascompleted)
                                                                  tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.direction = currentText
                                        Component.onCompleted: {
                                            var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars
                                            if(op.direction){
                                                for(var i=0; i<model.length; i++){
                                                    if(model[i]===op.direction){currentIndex=i;break;}
                                                }
                                            }
                                            else op.direction=currentText
                                            hascompleted=true
                                        }
                                    }
                                }
                                Component{
                                    id: compStringMode_insert
                                    ColumnLayout{
                                        RowLayout{
                                            anchors.baseline: parent.top
                                            ComboBox{
                                                id: comboStringModeInsert
                                                implicitWidth: 100
                                                property bool hascompleted: false
                                                model: ["beginning","end","at"]
                                                onCurrentTextChanged: if(hascompleted)
                                                                          tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars.insert = currentText
                                                Component.onCompleted: {
                                                    var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars
                                                    if(op.insert){
                                                        for(var i=0; i<model.length; i++){
                                                            if(model[i]===op.insert){currentIndex=i;break;}
                                                        }
                                                    }
                                                    else op.insert = currentText
                                                    hascompleted = true
                                                }
                                            }
                                            SkSpinBox{
                                                id: spinStringModeInsert
                                                property bool hascompleted: false
                                                visible: comboStringModeInsert.currentText==="at"
                                                implicitWidth: 50
                                                minimumValue: 0
                                                onValueChanged: if(hascompleted)
                                                                    tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars.at = value
                                                Component.onCompleted: {
                                                    var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars
                                                    if(op.at) value = op.at
                                                    else op.at = value
                                                    hascompleted = true
                                                }
                                            }
                                        }
                                        TextField{
                                            property bool hascompleted: false
                                            implicitWidth: comboStringModeInsert.width + (spinStringModeInsert.visible? spinStringModeInsert.width:0)
                                            placeholderText: "String to insert"
                                            onTextChanged: {
                                                if(hascompleted)
                                                    tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars.text = text
                                            }
                                            Component.onCompleted: {
                                                var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars
                                                if(op.text) text = op.text
                                                else op.text = text
                                                hascompleted = true
                                            }
                                        }
                                    }
                                }
                                Component{
                                    id: compStringMode_remove
                                    ColumnLayout{
                                        ComboBox{
                                            id: comboStringModeRemove
                                            model:["RegExp","at"]
                                            property bool hascompleted: false
                                            onCurrentTextChanged: if(hascompleted)
                                                                      tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars.remove = currentText
                                            Component.onCompleted: {
                                                var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars
                                                if(op.remove){
                                                    if(op.remove==="at")currentIndex=1
                                                    else currentIndex=0
                                                }
                                                else op.remove = currentText
                                                hascompleted = true
                                            }
                                        }
                                        TextField{
                                            visible: comboStringModeRemove.currentText==="RegExp"
                                            placeholderText: "Regular Expression"
                                            implicitWidth: 125
                                            property bool hascompleted: false
                                            onTextChanged: {
                                                if(hascompleted)
                                                    tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars.regexp = text
                                            }
                                            Component.onCompleted: {
                                                var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars
                                                if(op.regexp) text = op.regexp
                                                else op.regexp = text
                                                hascompleted = true
                                            }
                                        }
                                        RowLayout{
                                            visible: comboStringModeRemove.currentText==="at"
                                            Label{text: "start"}
                                            SkSpinBox{
                                                minimumValue: 0
                                                implicitWidth: 50
                                                property bool hascompleted: false
                                                onValueChanged: if(hascompleted)
                                                                    tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars.start = value
                                                Component.onCompleted: {
                                                    var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars
                                                    if(op.start) value = op.start
                                                    else op.start = value
                                                    hascompleted = true
                                                }
                                            }
                                            Label{text: "chars"}
                                            SkSpinBox{
                                                minimumValue: 0
                                                implicitWidth: 50
                                                property bool hascompleted: false
                                                onValueChanged: if(hascompleted)
                                                                    tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars.nchars = value
                                                Component.onCompleted: {
                                                    var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars
                                                    if(op.nchars) value = op.nchars
                                                    else op.nchars = value
                                                    hascompleted = true
                                                }
                                            }
                                        }
                                    }
                                }
                                Component{
                                    id: compStringMode_substring
                                    ColumnLayout{
                                        RowLayout{
                                            Label{text: "start"}
                                            SkSpinBox{
                                                minimumValue: 0
                                                implicitWidth: 50
                                                property bool hascompleted: false
                                                onValueChanged: if(hascompleted)
                                                                    tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars.start = value
                                                Component.onCompleted: {
                                                    var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars
                                                    if(op.start) value = op.start
                                                    else op.start = value
                                                    hascompleted = true
                                                }
                                            }
                                        }
                                        RowLayout{
                                            Label{text: "# of chars"}
                                            SkSpinBox{
                                                minimumValue: 0
                                                implicitWidth: 50
                                                property bool hascompleted: false
                                                onValueChanged: if(hascompleted)
                                                                    tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars.nchars = value
                                                Component.onCompleted: {
                                                    var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars
                                                    if(op.nchars) value = op.nchars
                                                    else op.nchars = value
                                                    hascompleted = true
                                                }
                                            }
                                        }
                                    }
                                }
                                Component{
                                    id: compStringMode_split
                                    ColumnLayout{
                                        RowLayout{
                                            Label{text: "delimiter"}
                                            TextField{
                                                placeholderText: "e.g. ,"
                                                implicitWidth: 50
                                                property bool hascompleted: false
                                                onTextChanged: {
                                                    if(hascompleted)
                                                        tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars.delimiter = text
                                                }
                                                Component.onCompleted: {
                                                    var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars
                                                    if(op.delimiter) text = op.delimiter
                                                    else op.delimiter = text
                                                    hascompleted = true
                                                }
                                            }
                                        }
                                        RowLayout{
                                            Label{text: "get index"}
                                            SkSpinBox{
                                                minimumValue: 0
                                                implicitWidth: 50
                                                property bool hascompleted: false
                                                onValueChanged: if(hascompleted)
                                                                    tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars.index = value
                                                Component.onCompleted: {
                                                    var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.parent.opIndex].opVars
                                                    if(op.index) value = op.index
                                                    else op.index = value
                                                    hascompleted = true
                                                }
                                            }
                                        }
                                    }
                                }
                                Component{
                                    id: compStringMode_replace
                                    ColumnLayout{
                                        TextField{
                                            placeholderText: "Regular Expression"
                                            implicitWidth: 125
                                            property bool hascompleted: false
                                            onTextChanged: {
                                                if(hascompleted)
                                                    tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars.regexp = text
                                            }
                                            Component.onCompleted: {
                                                var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars
                                                if(op.regexp) text = op.regexp
                                                else op.regexp = text
                                                hascompleted = true
                                            }
                                        }
                                        TextField{
                                            placeholderText: "Replacing string"
                                            implicitWidth: 125
                                            property bool hascompleted: false
                                            onTextChanged: {
                                                if(hascompleted)
                                                    tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars.replacingStr = text
                                            }
                                            Component.onCompleted: {
                                                var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars
                                                if(op.replacingStr) text = op.replacingStr
                                                else op.replacingStr = text
                                                hascompleted = true
                                            }
                                        }
                                    }
                                }   //End of compStringMode_replace
                            } // End of compStringOp/RowLayout
                        } // End of compStringOp
                        Component{
                            id: compFindInCol
                            ColumnLayout{
                                RowLayout{
                                    id:rowFindInCol
                                    Label{text:"Initial value from:"}
                                    ComboBox{
                                        id: comboFindInCol
                                        model:["string","thisValue"]
                                        property bool hascompleted: false
                                        onCurrentTextChanged: {
                                            if(hascompleted){
                                                tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.findInColMode = currentText
//                                                loadStringOp(currentText)
                                            }
                                        }
                                        Component.onCompleted: {
                                            if(tempO.sourceType==="file")model.push("file")
                                            model=model
                                            if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.findInColMode){
                                                for(var i=0; i<model.length; i++){
                                                    if(model[i]===tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.findInColMode){
                                                        currentIndex=i; break;
                                                    }
                                                }
                                            }
                                            else tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.findInColMode = currentText
//                                            loadStringOp(currentText)
                                            hascompleted=true
                                        }
                                    }
                                }
                                TextField{
                                    visible: comboFindInCol.currentText==="string"
                                    implicitWidth: rowFindInCol.width
                                    placeholderText: "Initial value"
                                    onTextChanged: tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.opIndex].opVars.initVal=text
                                    Component.onCompleted: {
                                        if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.opIndex].opVars.initVal)text=tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.opIndex].opVars.initVal
                                    }
                                }
                                RowLayout{
                                    visible: {
                                        if(comboFindInCol.currentText==="file" && comboSrcFormat.currentText==="CSV")true
                                        else false
                                    }
                                    ComboBox{
                                        id: comboFindInCol_file
                                        implicitWidth: 150
                                        model:["ValueFromFileName","ValueFromFileLine"]
                                        property bool hascompleted: false
                                        onCurrentTextChanged: if(hascompleted)
                                                                  tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.valueFromFile = currentText
                                        Component.onCompleted: {
                                            if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.valueFromFile){
                                                if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.valueFromFile==="ValueFromFileLine")
                                                    currentIndex=1
                                                else currentIndex = 0
                                            }
                                            else tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.valueFromFile = currentText
                                            hascompleted=true
                                        }
                                    }
                                    SkSpinBox{
                                        visible:comboFindInCol_file.currentText==="ValueFromFileLine"
                                        minimumValue: 0
                                        implicitWidth: 50
                                        property bool hascompleted: false
                                        onValueChanged: if(hascompleted)
                                                            tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.lineNum = value
                                        Component.onCompleted: {
                                            var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars
                                            if(op.lineNum) value = op.lineNum
                                            else op.lineNum = value
                                            hascompleted = true
                                        }
                                    }
                                }
                                TextField{
                                    visible: {
                                        if(comboFindInCol.currentText==="file" && comboSrcFormat.currentText==="JSON")true
                                        else false
                                    }
                                    implicitWidth: rowFindInCol.width
                                    placeholderText: "path to value key: key1,key2,..."
                                    onTextChanged: tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.opIndex].opVars.filePath=text
                                    Component.onCompleted: {
                                        if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.opIndex].opVars.filePath)text=tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.opIndex].opVars.filePath
                                    }
                                }
                                Label{text: "Seek value in db under:"}
                                TextField{
                                    id: textSeekVal
                                    implicitWidth: rowFindInCol.width
                                    placeholderText: "db,collection,field,subfield..."
                                    onTextChanged: tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.opIndex].opVars.initSeekPath=text
                                    Component.onCompleted: {
                                        if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.opIndex].opVars.initSeekPath)text=tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.opIndex].opVars.initSeekPath
                                    }
                                }
                                ColumnLayout{
                                    implicitWidth: rowFindInCol.width
                                    CheckBox{
                                        id: checkUseInterm
                                        text: "Use intermediate value"
                                        onCheckedChanged: tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.useIntermediateVal = checked
                                        Component.onCompleted: {
                                            if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.useIntermediateVal) checked = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.useIntermediateVal
                                            else checked = false
                                        }
                                    }
                                    RowLayout{
                                        visible: checkUseInterm.checked
                                        implicitWidth: rowFindInCol.width
                                        Label{
                                            text:{
                                                var p = textSeekVal.text.split(",")
                                                "Use " + p[0]+","+p[1]+","
                                            }
                                        }
                                        TextField{
                                            Layout.fillWidth: true
                                            placeholderText: "field,subfield,..."
                                            onTextChanged: tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars.intermSeekKeys=text
                                            Component.onCompleted: {
                                                if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars.intermSeekKeys)text=tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars.intermSeekKeys
                                            }
                                        }
                                    }
                                    RowLayout{
                                        implicitWidth: rowFindInCol.width
                                        visible: checkUseInterm.checked
                                        Label{text:"Find in:"}
                                        TextField{
                                            id: textSeekIntermVal
                                            Layout.fillWidth: true
                                            placeholderText: "db,collection,field,subfield,..."
                                            onTextChanged: tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars.intermSeekPath=text
                                            Component.onCompleted: {
                                                if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars.intermSeekPath)text=tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars.intermSeekPath
                                            }
                                        }
                                    }
                                }
                                ColumnLayout{
                                    implicitWidth: rowFindInCol.width
                                    CheckBox{
                                        id: checkUseFinal
                                        text: "Seek final value"
                                        onCheckedChanged: tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.useFinalVal = checked
                                        Component.onCompleted: {
                                            if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.useFinalVal) checked = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.useFinalVal
                                            else checked = false
                                        }
                                    }
                                    RowLayout{
                                        visible: checkUseFinal.checked
                                        implicitWidth: rowFindInCol.width
                                        Label{
                                            text:{
                                                if(checkUseInterm.checked){
                                                    var p = textSeekIntermVal.text.split(",")
                                                    "Use " + p[0]+","+p[1]+","
                                                }
                                                else{
                                                    var p = textSeekVal.text.split(",")
                                                    "Use " + p[0]+","+p[1]+","
                                                }
                                            }
                                        }
                                        TextField{
                                            Layout.fillWidth: true
                                            placeholderText: "field,subfield,..."
                                            onTextChanged: tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars.finalSeekKeys=text
                                            Component.onCompleted: {
                                                if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars.finalSeekKeys)text=tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.parent.opIndex].opVars.finalSeekKeys
                                            }
                                        }
                                    }
                                }
                            }
                        } //End of compSameColFind
                        Component{
                            id: compReduce
                            ColumnLayout{
                                RowLayout{
                                    ComboBox{
                                        id: comboReduceField
                                        implicitWidth: 100
                                        model: ["thisField_is","field_x_is"]
                                        property bool hascompleted: false
                                        onCurrentTextChanged: if(hascompleted)
                                                                  tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceField = currentText
                                        Component.onCompleted: {
                                            if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceField){
                                                for(var i=0;i<model.length;i++){
                                                    if(model[i]===tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceField){
                                                        currentIndex=i
                                                        break;
                                                    }
                                                }
                                            }
                                            else tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceField = currentText
                                            hascompleted=true
                                        }
                                    }
                                    TextField{
                                        visible: (comboReduceField.currentText==="field_x_is")
                                        Layout.fillWidth: true
                                        implicitWidth: 150
                                        placeholderText: "db,collection,field_x,..."
                                        onTextChanged: tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceFieldX=text
                                        Component.onCompleted: {
                                            if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceFieldX)text=tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceFieldX
                                        }
                                    }
                                    ComboBox{
                                        id: comboReduce
                                        implicitWidth: 90
                                        model: ["notNull","equalTo","higherThan","lowerThan",
                                        "highest","lowest","dateNow"]
                                        property bool hascompleted: false
                                        onCurrentTextChanged: if(hascompleted)
                                                                  tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceMode = currentText
                                        Component.onCompleted: {
                                            if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceMode){
                                                for(var i=0;i<model.length;i++){
                                                    if(model[i]===tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceMode){
                                                        currentIndex=i
                                                        break;
                                                    }
                                                }
                                            }
                                            else tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceMode = currentText
                                            hascompleted=true
                                        }
                                    }
                                }
                                RowLayout{
                                    ComboBox{
                                        id: comboReduceValOrField
                                        visible: (comboReduce.currentText==="equalTo" || comboReduce.currentText==="higherThan" || comboReduce.currentText==="lowerThan")
                                        implicitWidth: 80
                                        model: ["value","field"]
                                        property bool hascompleted: false
                                        onCurrentTextChanged: if(hascompleted)
                                                                  tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceValOrField = currentText
                                        Component.onCompleted: {
                                            if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceValOrField){
                                                for(var i=0;i<model.length;i++){
                                                    if(model[i]===tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceValOrField){
                                                        currentIndex=i
                                                        break;
                                                    }
                                                }
                                            }
                                            else tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceValOrField = currentText
                                            hascompleted=true
                                        }
                                    }
                                    TextField{
                                        Layout.fillWidth: true
                                        visible: comboReduceValOrField.visible && comboReduceValOrField.currentText==="value"
                                        onTextChanged: tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceVal=text
                                        Component.onCompleted: {
                                            if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceVal)text=tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceVal
                                        }
                                    }
                                    SkSpinBox{
                                        visible: comboReduceValOrField.visible && comboReduceValOrField.currentText==="field"
                                        minimumValue: 0
                                        implicitWidth: 50
                                        property bool hascompleted: false
                                        onValueChanged: if(hascompleted)
                                                            tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceFieldNum = value
                                        Component.onCompleted: {
                                            var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars
                                            if(op.reduceFieldNum) value = op.reduceFieldNum
                                            else op.reduceFieldNum = value
                                            hascompleted = true
                                        }
                                    }
                                    ComboBox{
                                        id: comboReduceDate
                                        visible: (comboReduce.currentText==="dateNow")
                                        implicitWidth: 80
                                        model: ["plus","minus"]
                                        property bool hascompleted: false
                                        onCurrentTextChanged: if(hascompleted)
                                                                  tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceDateMode = currentText
                                        Component.onCompleted: {
                                            if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceDateMode){
                                                for(var i=0;i<model.length;i++){
                                                    if(model[i]===tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceDateMode){
                                                        currentIndex=i
                                                        break;
                                                    }
                                                }
                                            }
                                            else tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceDateMode = currentText
                                            hascompleted=true
                                        }
                                    }
                                    SkSpinBox{
                                        visible: (comboReduce.currentText==="dateNow")
                                        minimumValue: 0
                                        implicitWidth: 50
                                        property bool hascompleted: false
                                        onValueChanged: if(hascompleted)
                                                            tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceDateNum = value
                                        Component.onCompleted: {
                                            var op = tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars
                                            if(op.reduceDateNum) value = op.reduceDateNum
                                            else op.reduceDateNum = value
                                            hascompleted = true
                                        }
                                    }
                                    ComboBox{
                                        id: comboReduceDateTimeframe
                                        visible: (comboReduce.currentText==="dateNow")
                                        implicitWidth: 90
                                        model: ["second","minute","hour","day","week","month","year"]
                                        property bool hascompleted: false
                                        onCurrentTextChanged: if(hascompleted)
                                                                  tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceDateTimeframe = currentText
                                        Component.onCompleted: {
                                            if(tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceDateTimeframe){
                                                for(var i=0;i<model.length;i++){
                                                    if(model[i]===tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceDateTimeframe){
                                                        currentIndex=i
                                                        break;
                                                    }
                                                }
                                            }
                                            else tempO.fieldsOps[rectOps.fieldIndex].ops[parent.parent.parent.parent.opIndex].opVars.reduceDateTimeframe = currentText
                                            hascompleted=true
                                        }
                                    }
                                }
                            }
                        }   //End of compReduce
                    }
                }
                function resetWidth(){
                    var maxWidth=0
                    for(var i=0; i<children.length; i++){
                        if(children[i].objectName!=="rowOps" && children[i].width>maxWidth)
                            maxWidth=children[i].width
                    }
                    rowOps.width=Math.max(233,maxWidth)
                }
            }
            function removeOp(atIndex){
                tempO.fieldsOps[index].ops.splice(atIndex,1)
                repeatOps.model=tempO.fieldsOps[index].ops
            }
        }
    }   //End of compOps
}
