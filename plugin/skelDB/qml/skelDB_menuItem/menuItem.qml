import QtQuick 2.0
import QtQuick.Controls 1.3


MenuItem{
    text: qsTr("DataBase")
    property var topwindow
    onTriggered: {
        var goobj= Qt.createQmlObject("import GoExtensions 1.0; GoObj{id:goobj;}",
                                     topwindow,null)
        goobj.write("Click on Database menu"+goobj.pWD())
        var comp=Qt.createComponent(goobj.pWD()+"/plugin/skelDB/qml/skelDB_SkTab/skelDB_SkTab.qml")
        var obj=comp.createObject(null)
        topwindow.dock.addTab(obj)
//        goobj.callrpc("mainUI", "MainUI.AddSkTab",
//                      JSON.stringify({"IsPath":true,"ObjPath":goobj.pWD()+"/plugin/skelDB/qml/skelDB_SkTab/skelDB_SkTab.qml"}))
    }
}
