import QtQuick 2.0
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.2
import QtQuick.Layouts 1.1
import "../../../mainUI/qml"

SkTab{
    title: "Database"
    FocusScope {
        id: contentHead
        x: 0
        width: parent.width
        height: comborefreshDB.height
        TextField{
            id:search
            width: parent.width-comborefreshDB.width-btnEditTree.width-btnPreference.width

            placeholderText: "Search the tree view"
//            onHoveredChanged: {
//                if(hovered)focus=true
//                else focus=false
//            }
            onActiveFocusChanged: console.log("search onActiveFocusChanged=", activeFocus)
            activeFocusOnTab: true

//            Layout.fillWidth:true
        }
        Item{
            id: comborefreshDB
            anchors.left:search.right
            height: search.height+2
            width:comboDBModel.width+10
            ComboBox {

                id: comboDBModel
                z:0
                height: parent.height
                width:comboDBModel.currentText.length*10

                model:["UpdateQuote","InsertQuot", "UpdateProducts", "InsertEconomics"]
                onPressedChanged: {
                    console.log("onPressedChanged", pressed)
    //                if(pressed===true){comboBox1.z=1; bm.z=0}
    //                else {comboBox1.z=0; bm.z=1}
                }
                onActivated: {
                    console.log("onActivated", index)
    //                    bm.text=textAt(index)
                }
            }
            Button{
                id:btnrefreshDB
                z:1
                height: parent.height
                tooltip: "Launch selected DataBase operation"
                anchors.topMargin: 5
                iconName: "view-refresh"
//                anchors.left:comboDBModel.right
                anchors.right: comborefreshDB.right
            }
        }

        Button {
            id: btnEditTree
            z:1
            height: parent.height
            width: height
            anchors.left: comborefreshDB.right
            iconSource: "./tree.png"
            tooltip: "Edit tree view"
            onCheckedChanged:console.log("oncheckedChanged", checked)
            onPressedChanged: console.log("onPressedChanged",pressed)
            onClicked: {
                console.log("onClicked")
                var goobj= Qt.createQmlObject("import GoExtensions 1.0; GoObj{id:goobj;}",
                                             btnEditTree,null)
                console.log(goobj.pluginDir("skelDB"))
                var comp=Qt.createComponent(goobj.pluginDir("skelDB")+"/qml/skelDB_DBEditorWindow/skelDB_DBEditorWindow.qml")
                var win=comp.createObject(btnEditTree)
                win.visible=true
            }
        }
        Button {
            id: btnPreference
            z:1
            height: parent.height
            width: height
            anchors.left: btnEditTree.right
            iconSource: "./preference.png"
            tooltip: "DataBase preferences"
            onCheckedChanged:console.log("oncheckedChanged", checked)
            onPressedChanged: console.log("onPressedChanged",pressed)
            onClicked: {
                console.log("onClicked")
                var prefW=tabdock.dock.topwindow.preferenceWindow
                prefW.visible=true
                prefW.tabview.selectTabByTitle("skelDB")
            }
        }
        Item {
            id: root
            width: 600 // change to parent.width
            height: 500// change to parent.height
            anchors.top:contentHead.bottom
            ListView {
                id: rootView
                anchors.fill: parent
                delegate: groupsDelegate
                model: listModel
                highlight: Rectangle {color: "lightsteelblue"; radius: 6}
                // focus: true
                Component {
                    id: groupsDelegate
                    Item {
                        id: container
                        width: 200
                        height: childrenRect.height
                        Column {
                            x: 14
                            id: mainColumn
                            Row {
                                id: mainRow
                                spacing: 3
                                property bool expanded: false
                                Image {
                                    id: expander
                                    source: "expander.png"
                                    rotation: mainRow.expanded ? 90 : 0
                                    opacity: elements.count === 0 ? 0 : 1
                                    Behavior on rotation {
                                        NumberAnimation {duration: 110}
                                    }
                                    MouseArea {
                                        visible: expander.opacity === 1 ? true : false
                                        id: expanderMouseArea
                                        anchors.fill: parent
                                        onClicked: {
                                            mainRow.expanded = !mainRow.expanded
                                            console.log(container.height)
                                        }
                                    }
                                }
                                Text {
                                    id: name
                                    text: group
                                }
                            }
                            Item {
                                width: 200
                                height: childView.contentHeight
                                visible: mainRow.expanded
                                ListView {
                                    id: childView
                                    anchors.fill: parent
                                    // visible: mainRow.expanded
                                    model: elements
                                    delegate: groupsDelegate
                                    highlight: Rectangle {color: "lightsteelblue"; radius: 5}
                                    focus: true
                                }
                            }
                        }
                        // ListView {
                        // anchors.right: parent.right
                        // visible: mainRow.expanded
                        // model: elements
                        // delegate: groupsDelegate
                        // }
                    }
                }
                ListModel {
                    id:listModel
                    ListElement {group: "first"; elements: []}
                    ListElement {
                        group: "second"
                        elements: [
                            ListElement {
                                group: "second2"
                                elements: [
                                ListElement {
                                    group: "second2.2"
                                    elements: []
                                }
                                ]
                            },
                            ListElement {group: "second3"; elements: []},
                            ListElement {group: "second4"; elements: []}
                        ]
                    }
                    ListElement {group: "third"; elements: []}
                }
            }
        }
    }
}
