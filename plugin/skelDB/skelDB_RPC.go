package main

import (
	"encoding/json"
	"fmt"
	"gopkg.in/mgo.v2"
	"io/ioutil"
)

type SkelDB struct {
	Session *mgo.Session
	connStr string `json:"connStr"`
}

//	Initialise a new SkelDB with a session to mongoDB
//		returns nil if connection to mongo fails
func NewSkelDB() *SkelDB {
	s := &SkelDB{}
	if filepath, err := skel.GetPluginDir("skelDB"); err != nil {
		fmt.Println(skel.LogPrefix(), "Error in NewSkelDB:", err.Error())
		return s
	} else {
		if content, errr := ioutil.ReadFile(filepath + "/skelDBConf.json"); errr != nil {
			fmt.Println(skel.LogPrefix(), "Error in NewSkelDB:", errr.Error())
			return s
		} else {
			if errrr := json.Unmarshal(content, s); errrr != nil {
				fmt.Println(skel.LogPrefix(), "Error in NewSkelDB:", errr.Error())
				return s
			}
		}
	}
	var err error
	if s.Session, err = mgo.Dial(s.connStr); err != nil {
		fmt.Println(skel.LogPrefix(), "Error in NewSkelDB:", err.Error())
	}
	return s
}

//	Initialise a session to mongoDB
//		returns nil if connection to mongo fails
func (s *SkelDB) Connect(initStr *string, resp *string) error {
	cStr := s.connStr
	if *initStr != "" {
		cStr = *initStr
	}
	var err error
	if s.Session, err = mgo.Dial(cStr); err != nil {
		fmt.Println(skel.LogPrefix(), "Error while connecting:", err.Error())
		s.Session = nil
		*resp = err.Error()
		return err
	}
	*resp = ""
	return nil
}

//	Test if Session object if non-nil
func (s *SkelDB) IsConnected(args *string, resp *string) error {
	if s.Session != nil {
		b, _ := json.Marshal(true)
		*resp = string(b)
	} else {
		b, _ := json.Marshal(false)
		*resp = string(b)
	}
	return nil
}

//	Retreive the list of DataBase
//		resp is a stringified JSON array of databases
func (s *SkelDB) GetDBNames(args *string, resp *string) error {
	if dbs, err := s.Session.DatabaseNames(); err != nil {
		fmt.Println(skel.LogPrefix(), "Error in GetDBList:", err.Error())
		*resp = err.Error()
		return err
	} else {
		strs, _ := json.Marshal(dbs)
		*resp = string(strs)
	}
	return nil
}

//	Retreive the list of Collections for named database in args
//		args is the name of the database for which to retrieve the collections
//		resp is a stringified JSON array of collections
func (s *SkelDB) GetCollectionNames(args *string, resp *string) error {
	if cols, err := s.Session.DB(*args).CollectionNames(); err != nil {
		fmt.Println(skel.LogPrefix(), "Error in GetCollections:", err.Error())
		*resp = err.Error()
		return err
	} else {
		strs, _ := json.Marshal(cols)
		*resp = string(strs)
	}
	return nil
}
