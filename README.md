# skeleton
This was a first project to build a multi panel gui system using go and qml (go-qml).

You could create your own plugins: add menu entries, create your own windows and own tabs.
The system would allow you to drag and drop these tabs, resize them, create new windows by dragging tabs outside current window
![gifTour](gifTour.gif)

## To get it working
* You must get [gopkg.in/qml.v1](https://github.com/go-qml/qml)
* Compile every plugin, including all go files in root of plugin
  * e.g. with MainUI plugin `cd plugin/mainUI` then `go build mainUI.go mainUI_GoObj.go mainUI_RPC.go mainUI_parser.go`

## How it works
* The top `skeleton` program builds a list of plugins from folders contained in the 'plugin' folder
* It then sorts these plugins by reading the `requiredPkg` key in the `<pluginName>Conf.json` file
* `skeleton` creates an RPC server to relay messages across plugins.
* Then launch plugins' process and pass RPC address and port as arguments. Every plugin creates its own RPC server easily thanks to the `plugin/plugin.go` package.
* `skeleton` also pipes stdin and stdout to control execution of plugins
* it then blocks execution until it receives a 'quit' message from any plugin.

The mainUI plugin is the most important one because it provides the GUI. It contains an 'API' package other plugins can import to easily use its functions: it basically wraps the rpc calls into nice go functions.

If you look into plugin/skelDB/skelDB.go, you will quickly see the process through which skelDB publishes its name to 'skeleton' program through the plugin package, how it creates a skeleton object to make use of the main program features, how it publishes its own functions (written in skelDB_RPC.go) through the use of `plugin.Init()` at line 29, and how it consumes mainUI plugin features (from line 34 in skelDB.go).

## Why I'm giving up on this project
I ambition on redoing the project in HTML. 'qml' is nice, but I find it too heavy a framework + HTML and web techs seem to be the way forward.

When [Atom 1.0](https://atom.io) got released, I discovered [electron](http://electron.atom.io) with it, and the [fireball project](https://github.com/fireball-x/editor-framework). This last project has achieved the same outcome as I was struggling to reach with qml, they are using html5, it's apparently easy to pack an app cross-platform, it doesn't take 1GB of crap to run, and I could actually focus on developping plugins with a go backend instead of worrying about stuff I am not interested in. Just Superbe!

## Future ambition
I know this is getting unrelated to this repository, but the final goal is to create a technical analysis of financial markets software: a mongoDB to pull data, a kool charting visualisation, a paper trading extension, a screening feature, and why not even connect it to an actual broker!?

The way I am now planning to do that is through packages for the [fireball project](https://github.com/fireball-x/editor-framework). So if you think linux is missing such software, don't hesitate to contact me :chart:
