//	This sub part of main skeleton program deals with the management of
//	plugins that are loaded as subprocesses from skeleton binary.
package main

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
)

type Plugin struct {
	Name        string
	Path        string
	RequiredPkg []string `json:"requiredPkg"`
	RpcAddress  string
	RpcPort     string
	Proc        *exec.Cmd
	Stdin       io.WriteCloser
	Stdout      io.ReadCloser
}

func (p *Plugin) Start(arg ...string) error {
	logger.Printf("skeleton: Starting plugin %s\n", p.Name)
	p.Proc = exec.Command(p.Path+string(os.PathSeparator)+p.Name, arg...)
	//	Stdin/out will be used to communicate strings with sub processes
	stdout, _ := p.Proc.StdoutPipe()
	stdin, _ := p.Proc.StdinPipe()

	if err := p.Proc.Start(); err != nil {
		//	if it fails to launch, we return an error. Caller function should handle the error
		//	and remove that plugin from the list. This could crash others who depend on it
		return errors.New("Error starting plugin '" + p.Name + "':" + err.Error() + " \n..." +
			p.Name + " will be removed. This may affect other plugins.")

	} else {
		p.Stdout = stdout
		p.Stdin = stdin
	}
	return nil
}

func (p *Plugin) Stop() {
	logger.Println("skeleton: Shutting Down", p.Name)

	fmt.Fprint(p.Stdin, "Push quit\n")
	//	Close file descriptors and kill plugin process
	if p.Stdin != nil {
		p.Stdin.Close()
	}
	if p.Stdout != nil {
		p.Stdout.Close()
	}

	if p.Proc != nil && p.Proc.Process != nil {
		p.Proc.Process.Kill()
	}
	i := pluginsMap[p.Name]
	logger.Println("skeleton # of plugins=", len(plugins), "i=", i)
	plugins = append(plugins[:i], plugins[i+1:]...)
	pluginsMap = make(map[string]int)
	for i, v := range plugins {
		pluginsMap[v.Name] = i
	}
}

func (p *Plugin) ListenOnStdOut() {
	r := bufio.NewReader(p.Stdout)
	for {
		data, _, err := r.ReadLine()
		if err != nil {
			logger.Println("skeleton: Error reading from plugin ", p.Name, ".Stdout :", err)
			if p.Name == "mainUI" {
				quitChan <- 1
			}
			p.Stop()

			break
		} else {
			// fmt.Println("New line read from", p.Name, ":", string(data))
			if e := Parse(string(data)); e != nil {
				logger.Println("skeleton: ", e)
			}
		}
	}
}
