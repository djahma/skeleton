package main

import (
	"errors"
	"fmt"
	"strings"
)

func Parse(str string) error {
	strs := strings.Split(str, " ")
	if len(strs) <= 1 {
		e := fmt.Sprintln("Parsing error in skeleton, not enough elements in given string",
			strs)
		return errors.New(e)
	}
	if strs[0] == "Get" {
		switch strs[1] {
		case "pluginRpcAddress":
			adr := plugins[pluginsMap[strs[2]]].RpcAddress
			port := plugins[pluginsMap[strs[2]]].RpcPort
			fmt.Fprintf(plugins[pluginsMap[strs[3]]].Stdin, "%s:%s\n", adr, port)
		default:
			fmt.Println("skeleton: Unrecognised Get request", strs)
		}
	} else if strs[0] == "Push" {
		switch strs[1] {
		case "quit":
			quitChan <- 1
		case "ok":
			logger.Println("skeleton: Ok!")
			okChan <- 1
		case "log":
			logger.Println(strings.Join(strs[2:], " "))
			// fmt.Println(strings.Join(strs[2:], " "))
		default:
			logger.Println("skeleton: Unrecognised Push request", strs)
		}
	} else {
		e := fmt.Sprint("Parsing error in skeleton, no match for string sequence: ",
			strs)
		return errors.New(e)
	}
	return nil
}

type Skeleton struct {
}

func (s *Skeleton) Println(str *string, err *string) error {
	logger.Println("skeleton: printing the string", *str)
	*err = ""
	return nil
}

type RpcCoords struct {
	Address string
	Port    string
}

func (s *Skeleton) GetRpcCoords(pluginStr *string, coords *RpcCoords) error {
	p := plugins[pluginsMap[*pluginStr]]
	coords.Address = p.RpcAddress
	coords.Port = p.RpcPort
	return nil
}

func (s *Skeleton) GetPluginDir(pluginName *string, dir *string) error {
	_, ok := pluginsMap[*pluginName]
	if !ok {
		return errors.New("Error getting plugin directory for" + *pluginName)
	}
	*dir = plugins[pluginsMap[*pluginName]].Path
	return nil
}
