package main

import (
	"log"
	"os"
)

const (
	pluginCap int = 1000 // Max number of plugins skeleton binary will handle
)

var (
	plugins    = make([]*Plugin, 0, pluginCap)
	pluginsMap = make(map[string]int)
	quitChan   = make(chan int)
	okChan     = make(chan int)
	Conf       = new(Config)
	logger     *log.Logger
)

type Config struct {
	RpcAddress string `json:"rpcAddress"`
	RpcPort    string `json:"rpcPort"`
}

func main() {
	defer func() {
		if x := recover(); x != nil {
			logger.Println("run time panic: ", x)
		}
	}()
	logger = log.New(os.Stdout, "", log.Ldate+log.Ltime)
	logger.Println("Skeleton: parsing conf file")
	parseSkeletonConf()
	if err := initialiseSkeletonRPC(); err != nil {
		logger.Println("skeleton: Failed to start rpc:", err)
		return
	}
	logger.Println("skeleton: func main: rpcaddress=", Conf.RpcAddress, "RpcPort=", Conf.RpcPort)
	initialisePlugins()
	logger.Println("skeleton: After initialise")
	for i, v := range pluginsMap {
		logger.Println("skeleton:", i, "=", v)
	}
	// time.Sleep(1e9 * 1)
	// fmt.Fprint(plugins[1].Stdin, "Push quit\n")

	//	Block program execution until a plugin sends a quit message
	<-quitChan
	logger.Println("skeleton: right after <-quitChan")
	//	Shutting dow plugins
	for len(plugins) > 0 {
		plugins[0].Stop()
	}
}
