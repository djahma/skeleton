package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
	"os"
	"path/filepath"
	"strconv"
)

//	Parse the skeleton.conf file.
func parseSkeletonConf() {
	absPath, _ := filepath.Abs(os.Args[0])
	absPath += "Conf.json"
	logger.Println("skeleton: Skeleton path=", absPath)
	file, _ := os.Open(absPath)
	defer file.Close()
	dec := json.NewDecoder(file)
	dec.Decode(Conf)
	return
}

func initialiseSkeletonRPC() error {
	server := rpc.NewServer()
	server.HandleHTTP(rpc.DefaultRPCPath, rpc.DefaultDebugPath)
	server.Register(&Skeleton{})
	listener, e := net.Listen("tcp", fmt.Sprintf(":%s", Conf.RpcPort))
	if e != nil {
		return errors.New(fmt.Sprintln("listen error on skeleton RPC:", e))
	}
	go func(l net.Listener) {
		for {
			if conn, err := l.Accept(); err != nil {
				logger.Println("skeleton: Error serving:", err)
			} else {
				go server.ServeCodec(jsonrpc.NewServerCodec(conn))
			}
		}
	}(listener)
	return nil
}

func initialisePlugins() {
	//	First fill in the list of all plugins under ./plugin folder
	wd, _ := os.Getwd()
	allPlugins := pluginListAll(wd + string(os.PathSeparator) + "plugin")
	for i, v := range allPlugins {
		logger.Println("skeleton: allPlugins[", i, "]", v.Name)
		parsePluginConf(v)
	}
	//	Second, put plugins in the global loading-ordered list "plugins[]"
	orderPluginsByRequiredPkg(allPlugins)
	logger.Println("skeleton: plugins[] len=", len(plugins))
	//	Third, start plugin processes and connect over rpc and stdin/stdout
	for i, v := range plugins {
		port, _ := strconv.Atoi(Conf.RpcPort)
		v.RpcAddress = "localhost"
		port += i + 1
		v.RpcPort = strconv.Itoa(port)

		if err := v.Start(Conf.RpcAddress, Conf.RpcPort); err != nil {
			logger.Println(err)
			v.Stop()
			plugins = append(plugins[:i], plugins[i+1:]...)
			continue
		}
		pluginsMap[v.Name] = i
		go v.ListenOnStdOut()
		//	Block until plugin has finished launching its RPC server
		<-okChan
	}
}

//	Returns the list of all potential plugins that could be loaded.
func pluginListAll(path string) []*Plugin {
	logger.Println("skeleton: package main, func pluginListAll")
	file, err := os.Open(path)
	if err != nil {
		logger.Println(err.Error())
	}
	plugList := make([]*Plugin, 0, pluginCap)
	pl, _ := file.Readdirnames(-1)
	for i, s := range pl {
		plgPath := path + string(os.PathSeparator) + s
		logger.Println("skeleton: PluginList: indexing #", i, plgPath)
		fi, _ := os.Stat(path + string(os.PathSeparator) + s)
		if fi.IsDir() {
			logger.Println("skeleton: Base name=", fi.Name())
			plg := &Plugin{Name: s, Path: plgPath}
			plugList = append(plugList, plg)
		}
	}
	return plugList
}

//	Parse the plugin.conf file and fill the Plugin parameter with values.
func parsePluginConf(plg *Plugin) {
	logger.Println("skeleton: package main, func parsePluginConf: plg.Name=",
		plg.Name, "path=",
		plg.Path+string(os.PathSeparator)+plg.Name+"Conf.json")
	// Parse conf file for given plg plugin parameter
	file, _ := os.Open(plg.Path + string(os.PathSeparator) + plg.Name + "Conf.json")
	dec := json.NewDecoder(file)
	dec.Decode(plg)
	return
}

//	Fill the "plugins[]" global variabe from the pl *Plugin parameter, using
//	the order dictated by the RequiredPkg config variable. Plugins whose requiredPkg
//	cant be found are discarded with a warning.
func orderPluginsByRequiredPkg(pl []*Plugin) {
	logger.Println("skeleton: package main, func orderPluginsByRequiredPkg")
	//	We copy the slice pl into a map for ease of manipulation afterwards
	var m = make(map[int]*Plugin)
	for i := 0; i < len(pl); i++ {
		m[i] = pl[i]
	}
	lastVisited := -1
	for len(m) > 0 {
		for ind, v := range m {
			if lastVisited == -1 {
				lastVisited = ind
			}
			//	Case plugin doesnt require any package to run: we add it outright
			if len(v.RequiredPkg) == 0 {
				plugins = append(plugins, v)
				delete(m, ind)
				lastVisited = -1
				break //	break and not continue because we popped one item from pl[]
			}
			//	Case plugin requires other plugin to run
			for j, n := 0, len(v.RequiredPkg); j < len(v.RequiredPkg); j++ {
				for i := 0; i < len(plugins); i++ {
					if plugins[i].Name == v.RequiredPkg[j] {
						n--         //	we found one required pkg
						if n == 0 { //	if all plugins have been found, we add it to plugins[]
							plugins = append(plugins, v)
							delete(m, ind)
							lastVisited = -1
						}
						break
					}
				}
				//	If one required plugin is missing, there is no point continuing this loop
				if n+j >= len(v.RequiredPkg) {
					break
				}
			}
			//	If we added a new plugin to plugins[],we restart the loop from beginning
			if lastVisited == -1 {
				break
			}
		}
		if lastVisited != -1 {
			logger.Println("skeleton: Warning: Plugin", m[lastVisited].Name,
				"is missing a required plugin to work and wont be loaded.")
			delete(m, lastVisited)
		}
	}
	//	At this point, the map is empty and all plugins have been either added to plugins[]
	//	or discarded as not loadable (because their required plugins are missing)
}
