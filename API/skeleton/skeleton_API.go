package skeleton

import (
	"errors"
	"fmt"
	"net/rpc"
	"net/rpc/jsonrpc"
)

var (
	Get  string = "Get"
	Push string = "Push"
)

type Skeleton struct {
	PluginName string
	// skeleton binary RPC details
	Address string
	Port    string
	// RPC internals
	client *rpc.Client
}

func NewSkeleton(yourPluginName string, skeletonRPCAddr string, skeletonRPCPort string) (*Skeleton, error) {
	sk := &Skeleton{PluginName: yourPluginName}
	sk.Address = skeletonRPCAddr
	sk.Port = skeletonRPCPort
	// sk.InitRPC(sk.Address, sk.Port)
	if err := sk.InitRPC(sk.Address, sk.Port); err != nil {
		return nil, err
	}
	return sk, nil
}

//	Initialise RPC details to communicate with skeleton process
//	This func should be called before any other func
func (s *Skeleton) InitRPC(adr string, port string) error {
	if adr != "" {
		s.Address = adr
	}
	if port != "" {
		s.Port = port
	}
	// client, err := rpc.DialHTTP("tcp", s.Address+":"+s.Port)
	client, err := jsonrpc.Dial("tcp", s.Address+":"+s.Port)
	if err != nil {
		return errors.New(err.Error())
	} else {
		s.client = client
	}
	return nil
}

func (s *Skeleton) Close() {
	s.client.Close()
}

//	Quit main program execution
func (s *Skeleton) Quit() {
	fmt.Printf("%s %s\n", Push, "quit")
}

//	To confirmreception. Ex: upon starting plugins
func (s *Skeleton) Ok() {
	fmt.Printf("%s %s\n", Push, "ok")
}

//	Prefix string to avoid messages throwing an error.
func (s *Skeleton) LogPrefix() string {
	return fmt.Sprintf("%s %s %s: ", Push, "log", s.PluginName)
}

//	Allows to retrieve address:port for a given plugin name. ex.: 'localhost:10503'
func (s *Skeleton) GetPluginRpcAddress(pluginName string, sender string) string {
	fmt.Println("about to ask plugin")
	fmt.Printf("%s %s %s %s\n", Get, "pluginRpcAddress", pluginName, sender)
	adr := ""
	fmt.Scan(&adr)
	return adr
}

func (s *Skeleton) Println(str string) error {
	answer := ""
	err := s.client.Call("Skeleton.Println", &str, &answer)
	if err != nil {
		return errors.New(err.Error())
	}
	return nil
}

type RpcCoords struct {
	Address string
	Port    string
}

func (s *Skeleton) GetRpcCoords(pluginName string) (address string, port string, err error) {
	coo := &RpcCoords{}
	er := s.client.Call("Skeleton.GetRpcCoords", &pluginName, &coo)
	if er != nil {
		return "", "", errors.New(er.Error())
	}
	return coo.Address, coo.Port, nil
}

//	Returns an *rpc.Client to contact plugin
func (s *Skeleton) RpcClient(pluginName string) (*rpc.Client, error) {
	coo := &RpcCoords{}
	er := s.client.Call("Skeleton.GetRpcCoords", &pluginName, &coo)
	if er != nil {
		return nil, errors.New(er.Error())
	}
	client, err := jsonrpc.Dial("tcp", coo.Address+":"+coo.Port)

	// client, err := rpc.DialHTTP("tcp", coo.Address+":"+coo.Port)
	if err != nil {
		fmt.Println("Skeleton.RpcClient error:", err)
		return nil, errors.New(err.Error())
	}
	return client, nil
}

func (s *Skeleton) GetPluginDir(pluginName string) (string, error) {
	var resp string
	if er := s.client.Call("Skeleton.GetPluginDir", &pluginName, &resp); er != nil {
		return "", er
	}
	return resp, nil

}
